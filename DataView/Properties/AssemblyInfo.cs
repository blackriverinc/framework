﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Framwork DataView")]
[assembly: AssemblyDescription("Programmatic use of web controls to build simple data presentations.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Black River Systems, Inc. of ILlinois")]
[assembly: AssemblyProduct("Framework.DataStore")]
[assembly: AssemblyCopyright("Copyright © Black River Systems, Inc. of Illinois 2011 - 2017 ")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("088cf756-14e6-4f32-a5a3-8385b0670ed0")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
