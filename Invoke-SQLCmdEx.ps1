﻿[CmdletBinding()]
param
	(
	[Parameter(Mandatory=$true)]
	[string]$ScriptPath,
	[string]$Server	= 'DMISQL8',
	[string]$Username = $null,
	[string]$Password = $null,
	[string]$Database = $null
	)
	
Set-StrictMode -Version Latest
#! Powershell Invoke-SQLCmd.ps1 -Script 'Database\Test\00-Schema\OnlineReporting.DB.sqlcls

function Execute-Main()
	{
	Push-Location
	Display-JobMessage $("+++++++++++++++++++++++++ " + [System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.ScriptName) + " +++++++++++++++++++++++++")
	Display-JobMessage ""
	try
		{
		$cwd = [System.IO.Path]::GetDirectoryName($MyInvocation.ScriptName)
		Set-Location $cwd
		
		$scriptLoc = Get-Location 
		#Resolve Script Path
		$ScriptPath = verifyPath -jobRootPath '.\' -pathName $ScriptPath
		
		Display-JobMessage $ScriptPath
		foreach($item in (gci -recurse $ScriptPath) | ?{$_.Extension -eq ".sql"} | Sort-Object -Property Directory, BaseName)
			{
			Display-JobMessage $item.Directory, $item.BaseName
			}
		}
	catch 
		{
		Display-JobExceptions $_.Exception
		}
	finally
		{
		Pop-Location
		Display-JobMessage ""
		Display-JobMessage $("------------------------- " + [System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.ScriptName) + " -------------------------")
		}
	}
	
#---------------------------------------------------------------------
#---------------------------------------------------------------------
function Display-JobMessage($msg)
	{
	Write-Host $([DateTime]::Now.ToString("MM/dd/yyyy hh:mm:ss") + " " + $msg)
	}
	
#---------------------------------------------------------------------
#---------------------------------------------------------------------
function Display-JobExceptions($exp)
	{
	Display-JobMessage "*$exp.GetType()*"
	Display-JobMessage "    $exp"
	if ($exp.InnerException)
	{
		Display-JobMessage "------------------------------------"
		Display-JobExceptions $exp.InnerException
		Display-JobMessage "------------------------------------"
		}
	}
		
#---------------------------------------------------------------------
# Invoke-SqlCommand
#---------------------------------------------------------------------
function Invoke-SqlCommand
	{
	param(
		[string] $dataSource = $Database,
		[string] $database = $Database,
		[Parameter(Mandatory = $true)]
		[string[]] $sqlCommands,
		[int] $timeout = 60
	)
	
	Set-StrictMode -Version Latest
	
	$authentication = "Integrated Security=SSPI;"
	$connectionString = "Provider=sqloledb; " +
							  "Data Source=$datasource; " +
							  "Initial Catalog=$database; " +
							  "$authentication;"
		
	$private:connection = New-Object System.Data.OleDb.OleDbConnection $connectionString
	try
		{
		$connection.Open()
		
		foreach($commandString in $sqlCommands)
			{
			$private:command = New-Object System.Data.OleDb.OleDbCommand $commandString, $connection
			$command.CommandTimeout = $Timeout
			$adapter = New-Object System.Data.oledb.OleDbDataAdapter $command
			$dataSet = New-Object System.Data.DataSet
			[void]$adapter.Fill($dataSet)
			$dataSet.Tables | Select-Object -Expand Rows
			}
		}
	finally
		{
		$connection.Close()
		}
	
	}

#---------------------------------------------------------------------
# verifyPath (private)
#	Verify object exists at location '$pathName', or failing that, 
#	at ($rootPath + $jobStreamID + $pathName). 
#
#	On failure to find an object, if $create flag is set, create a new
#	location at the ($rootPath + $jobStreamID + $pathName)
#---------------------------------------------------------------------
function local:verifyPath(
		[string]$pathName, 
		[bool]$create = $false,
		[bool]$explicit = $false,
		[string]$jobRootPath = $PWD,
		[bool]$silent = $false)
	{
	$item = Get-Item $pathName -ErrorAction SilentlyContinue
	if ($explicit -or $item -eq $null)
		{
		#Search a join to the $jobRootPath
		$joinedPath = $(Join-Path $jobRootPath $pathName)
		$item = Get-Item $joinedPath -ErrorAction SilentlyContinue
		if ($item -eq $null -and -not $create)
			{
			if (-not $silent)
				{
				Display-JobMessage "    !'$joinedPath' does not exist."
				}
			return $null;
			}
		if ($item -eq $null)
			{
			$extractedReports = New-Item -Path $jobRootPath -Name $pathName -type directory
			if (-not $silent)
				{
				Display-JobMessage "    !Created directory '$(Join-Path $jobRootPath $pathName)'"
				}
			}
		$pathName = $joinedPath
		}
	return $pathName
	}
	

#---------------------------------------------------------------------
#---------------------------------------------------------------------
Execute-Main @args
