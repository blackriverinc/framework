﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;

using PAWorkIntakeUI.Constants;
using PAWorkIntakeUI.DataTypes;
using PAWorkIntakeUI.Utility;

namespace PAWorkIntakeUI.Web.Extensions
{
    public class FormEntryModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {
            string fieldName = string.Format("{0}.{1}", bindingContext.ModelName, propertyDescriptor.Name);

            // Invoke custom property binders.
            var binderAttribute = propertyDescriptor.Attributes
                                            .OfType<IPropertyBinder>()
                                            .FirstOrDefault();

            /// The Validation Attributes will need a DisplayName; we will
            /// use the [Display] attribute if we can find it, otherwise we
            /// will take a stab at it by un-camel-casing the property name.
            var displayAttribute = propertyDescriptor.Attributes
                                      .OfType<DisplayAttribute>()
                                      .FirstOrDefault();
            string displayName = (displayAttribute != null)
                                        ? displayAttribute.Name
                                        : propertyDescriptor.Name.CamelCaseToDelimitted();

            if (binderAttribute != null)
            {
                if (binderAttribute.TryBindProperty(controllerContext, bindingContext, propertyDescriptor))
                {
                    string fieldValue = bindingContext.ValueProvider.GetValue(fieldName).AttemptedValue;

                    /// Custom binder had no complaints, but we still need to give the
                    /// validation attributes a crack at it.
                    var validationAttributes = propertyDescriptor.Attributes
                                                         .OfType<ValidationAttribute>();
                    List<ValidationResult> validationResults = new List<ValidationResult>();
                    var validationContext = new ValidationContext(bindingContext.Model, null, null)
                                                                    { MemberName = propertyDescriptor.Name,
                                                                     DisplayName = displayName};
                    var result = Validator.TryValidateValue(fieldValue,
                                                            validationContext,
                                                            validationResults,
                                                            validationAttributes);
 
                    foreach (var validationResult in validationResults)
                    {
                        bindingContext.ModelState.AddModelError(fieldName, validationResult.ErrorMessage);
                        bindingContext.ModelState[fieldName].Value = bindingContext.ValueProvider.GetValue(fieldName);
                    }

                }
            }
            else
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);

        }
    }
}
