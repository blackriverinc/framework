﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Mvc;

using PAWorkIntakeUI.Constants;
using PAWorkIntakeUI.Utility;

namespace PAWorkIntakeUI.Web.Extensions
{
    /// <summary>
    /// Allow specification of simple integer number binding. Default values for
    /// [AllowNegative], and [AllowThousands] is 'true'.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class IntegerBinderAttribute : Attribute, IPropertyBinder
    {
        public bool AllowThousands { get; set; }
        public bool AllowNegative { get; set; }

        public IntegerBinderAttribute()
        {
            AllowNegative = true;
            AllowThousands = true;
        }

        #region IPropertyBinderAttribute Members

        public bool TryBindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {
            string fieldName = string.Format("{0}.{1}", bindingContext.ModelName, propertyDescriptor.Name);
            string fieldValue = bindingContext.ValueProvider.GetValue(fieldName).AttemptedValue;
            if (string.IsNullOrWhiteSpace(fieldValue))
                fieldValue = "0";

            int parsedValue = default(int);

            // Use built in (int) parser to validate input.
            NumberStyles numberStyles = default(NumberStyles);
            numberStyles |= (AllowNegative)
                                ? NumberStyles.AllowLeadingSign
                                : default(NumberStyles);
            numberStyles |= (AllowThousands)
                                ? NumberStyles.AllowThousands
                                : default(NumberStyles);
            numberStyles |= NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite;
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            if (!int.TryParse(fieldValue, numberStyles, currentCulture, out parsedValue))
            {
                var displayAttribute = propertyDescriptor.Attributes
                                                              .OfType<DisplayAttribute>()
                                                              .FirstOrDefault();
                string displayName = (displayAttribute != null)
                    ? displayAttribute.Name : propertyDescriptor.Name.CamelCaseToDelimitted();
                string valMsg = string.Format(ValidationMessages.ValidInput, displayName);
                bindingContext.ModelState.AddModelError(fieldName, valMsg);
                bindingContext.ModelState[fieldName].Value = bindingContext.ValueProvider.GetValue(fieldName);
                return false;
            }

            if (propertyDescriptor.PropertyType == parsedValue.GetType()
            || propertyDescriptor.Converter.CanConvertFrom(parsedValue.GetType()))
            {
                propertyDescriptor.SetValue(bindingContext.Model, parsedValue);
            }
            else
            /// Cannot directly convert from the parsedValue's type (decimal) to 
            /// the property's type; output as a string and then reparse the value.
            {
                fieldValue = parsedValue.ToString("#");
                propertyDescriptor.SetValue(bindingContext.Model,
                                            propertyDescriptor.Converter.ConvertFromString(fieldValue));

            }
            return true;
        }

        #endregion
    }
}