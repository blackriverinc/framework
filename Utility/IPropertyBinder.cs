﻿using System;
using System.ComponentModel;
using System.Web.Mvc;

namespace PAWorkIntakeUI.Web.Extensions
{
    interface IPropertyBinder 
    {
        bool TryBindProperty(ControllerContext controllerContext,
                           ModelBindingContext bindingContext,
                            PropertyDescriptor propertyDescriptor);
    }
}
