﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Blackriverinc.Framework.Utility.Reflection;
using Blackriverinc.Framework.Utility.Time;

namespace Blackriverinc.Framework.Utility
   {
   /// <summary>
   /// Resolves tokens in a string to an underlying keyed-storage.
   /// </summary>
   public class TextTemplate
      {
      private Regex _tokenParser;


      string _template = null;

      private const string _defaultTokenPattern = @"\{ (?<token> =?(\w)+ ) (::(?<method> [\w]+))* (:(?<format> [\w\.\-\/]+))?  \}";
      private string _tokenPattern;
      public string TokenPattern
         {
         get
            {
            if (_tokenPattern == null || _tokenPattern.Length == 0)
               _tokenPattern = _defaultTokenPattern;
            return _tokenPattern;
            }
         set
            {
            // Use a fixed Parser to verify the new Pattern
            if (!_patternParser.Match(value).Success)
               throw new ApplicationException(string.Format("Token Pattern '{0}' must contain named group (?:<token> )",
                                                            value));

            _tokenPattern = value;
            _tokenParser = new Regex(_tokenPattern,
               RegexOptions.IgnoreCase 
               | RegexOptions.IgnorePatternWhitespace
               | RegexOptions.ExplicitCapture);
            }
         }

      // Verifies that any new pattern is valid
      private Regex _patternParser;
      private const string _patternVerifier = @"\<token\>";

      protected IDictionary<string, object> _dictionary;

      private void initializeDictionary(IEnumerable<KeyValuePair<string, object>> dictionary)
         {
         _dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
         foreach (var entry in dictionary)
            {
                if (!_dictionary.ContainsKey(entry.Key))
            _dictionary.Add(entry);
                else
                    _dictionary[entry.Key] = entry.Value;
         }
        }


        private void initializeDictionary(IEnumerable<DictionaryEntry> dictionary)
        {
            _dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            foreach (var entry in dictionary)
                if (!_dictionary.ContainsKey(entry.Key.ToString()))
                    _dictionary.Add(entry.Key.ToString(), entry.Value);
                else
                    _dictionary[entry.Key.ToString()] = entry.Value;
        }

      private void initializeDictionary(object values)
         {
         _dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

         Type type = values.GetType();
         PropertyInfo[] properties = type.GetProperties();
         foreach (var property in properties)
            {
            var value = property.GetValue(values, null);
            if (value != null && value.GetType() == typeof(string))
               value = value.ToString().Trim();
                if (!_dictionary.ContainsKey(property.Name))
            _dictionary.Add(property.Name, value);
                else
                    _dictionary[property.Name] = value;
            }
         }

      private void initializeTokenizer()
         {
         _patternParser = new Regex(_patternVerifier);
         TokenPattern = _defaultTokenPattern;
         }

      private void initializeTemplate(string template)
         {
         _template = template;
         }

      public TextTemplate(IEnumerable<KeyValuePair<string, object>> dictionary)
         {
         initializeTokenizer();
            initializeDictionary(dictionary);
        }

        public TextTemplate(IEnumerable<DictionaryEntry> dictionary)
        {
            initializeTokenizer();
         initializeDictionary(dictionary);
         }

      public TextTemplate(IDictionary<string, object> dictionary)
         {
         initializeTokenizer();
         _dictionary = dictionary;
         }

      public TextTemplate(object values)
         {
         initializeDictionary(values);
         initializeTokenizer();
         }

      public TextTemplate(string template)
         {
         initializeTemplate(template);
         initializeTokenizer();
         }

      public string Render(object values)
         {
         if (_template == null)
            throw new Exception("TextTemplate : No template given or defined. Cannot render the data source");

         initializeDictionary(values);

         return ResolveToString(_template);
         }
      public string Render(IDictionary<string, object> values)
         {
         if (_template == null)
            throw new Exception("TextTemplate : No template given or defined. Cannot render the data source");

         initializeDictionary(values);

         return ResolveToString(_template);
         }

      public string Render(IEnumerable<KeyValuePair<string, object>> values)
         {
         if (_template == null)
            throw new Exception("TextTemplate : No template given or defined. Cannot render the data source");

         initializeDictionary(values);

         return ResolveToString(_template);
         }

        public string Render(IEnumerable<DictionaryEntry> values)
        {
            if (_template == null)
                throw new Exception("TextTemplate : No template given or defined. Cannot render the data source");

            initializeDictionary(values);

            return ResolveToString(_template);
        }

      public string ResolveToString(string text, int recursionDepth = 8)
         {
            if (recursionDepth <= 0)
                return text;
         if (text != null)
            {
            MatchEvaluator substitute = new MatchEvaluator(txt =>
            {
               string value = "";
               string key = txt.Groups["token"].Value;
               string format = txt.Groups["format"].Value;
               if (key.SafeSubstring(0, 1) == "=")
                  // Token is tagged with "Stop Parse Now"
                  return string.Format("{0}{1}{2}", key.SafeSubstring(1),
                                                    (!string.IsNullOrEmpty(format))?":":"",
                                                    format??"");
               object obj = null;
               if (_dictionary.ContainsKey(key) && (obj = _dictionary[key]) != null)
                  {
                  if (!txt.Groups["method"].Success)
                     value = (string.IsNullOrEmpty(format))
                              ? obj.ToString()
                              : string.Format(string.Concat("{0:", format, "}"), obj);
                  else
                     {
                     // {key::method} Look for either 
                     //   a) A parameter-less instance method, or
                     //   b) an extension method, with no aditional parameters.
                     Type objType = obj.GetType();
                     string methodName = txt.Groups["method"].Value;

                     MethodInfo method = objType.GetMethod(methodName, 
                                                           BindingFlags.Instance, 
                                                           null,
                                                           Type.EmptyTypes,
                                                           null);
                     if (method != default(MethodInfo))
                        {
                        value = method.Invoke(obj, null).ToString();
                        }
                     else
                        {
                        method = objType.GetExtensionMethod(methodName);
                        if (method == default(MethodInfo))
                           {
                           Trace.WriteLine("WARN: Object referenced by token '{0}' does not implement a '{1}' method.".Bind(key, methodName));
                           value = obj.ToString();
                           }
                        else
                           {
                           value = method.Invoke(objType, new object[] {obj}).ToString();
                           }
                        }
                     }
                  }
               return (--recursionDepth > 0) ? ResolveToString(value, recursionDepth) : value;
            });

            text = _tokenParser.Replace(text, substitute);
            }
         return text;
         }
      }
   }
