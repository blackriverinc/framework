﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;

namespace Framework.Utility
   {
   public static class SerializationExtensions
      {
      #region Serializers

      /// <summary>
      /// Serialize the public, instance properties of an object
      /// to a C#-like initializer.
      /// </summary>
      /// <param name="obj">Object to serialize</param>
      /// <param name="fieldList">If non-empty, limits the seriualization
      /// to names in the list.</param>
      /// <returns></returns>
      public static string ToObjectString<T>(this T obj, string fieldList = "")
         {
         return obj.propertySerializer<T>(objectStringFormatter, fieldList);
         }

      /// <summary>
      /// Serialize the public, instance properties of an object
      /// to a Comma-separated value list; strings are enclosed in quotes..
      /// </summary>
      /// <param name="obj">Object to serialize</param>
      /// <param name="fieldList">If non-empty, limits the seriualization
      /// to names in the list.</param>
      /// <returns></returns>
      public static string ToCSVString<T>(this T obj, string fieldList = "")
         {
         return obj.propertySerializer<T>(commaDelimittedStringFormatter, fieldList);
         }

      public static string ToPipeDelimittedString<T>(this T obj, string fieldList = "")
         {
         return obj.propertySerializer<T>(pipeDelimittedStringFormatter, fieldList);
         }

      public static string ToPropertyNameList<T>(this T obj, string fieldList = "")
         {
         return obj.propertySerializer<T>(delimittedPropertyNameFormatter, fieldList);
         }

      public static string ToHtmlAttributes<T>(this T obj)
         {
         StringBuilder sb = new StringBuilder();
         Type type = typeof(T);

         PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance
                                                      | BindingFlags.DeclaredOnly);
         foreach (var p in properties)
            {
            string name = p.Name;
            var value = p.GetValue(obj, null);
            sb.AppendFormat(" {0} = '{1}'", name, value);
            }
         return sb.ToString();
         }

      /// <summary>
      /// Create a JSON string by using the JavaScriptSerializer with the
      /// ability to use custom JavaScriptConverter created just for this Type.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="obj"></param>
      /// <param name="fieldList"></param>
      /// <returns></returns>
      public static string ToJSONString<T>(this T obj, string fieldList = "")
         {
         StringBuilder sb = new StringBuilder();
         Type type = typeof(T);

         JavaScriptSerializer serializer = new JavaScriptSerializer();
         serializer.RegisterConverters(new List<JavaScriptConverter>() { new PublicPropertyConverter<T>(fieldList) });

         serializer.Serialize(obj, sb);

         return sb.ToString();
         }

      #endregion

      #region Helpers
      /// <summary>
      /// 
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="obj"></param>
      /// <param name="formatter"></param>
      /// <param name="fieldList"></param>
      /// <returns></returns>
      private static string propertySerializer<T>(this T obj, Func<T, StringBuilder, IEnumerable<PropertyInfo>, IEnumerable<string>, string> formatter, string fieldList = "")
         {
         StringBuilder sb = new StringBuilder();
         Type type = typeof(T);

         IEnumerable<PropertyInfo> properties = type.GetProperties(BindingFlags.Public
                                                      | BindingFlags.Instance
                                                      | BindingFlags.DeclaredOnly)
                                         .OrderBy(p => {
                                            var orderAttr = (DisplayAttribute)p.GetCustomAttributes(typeof(DisplayAttribute), true)
                                                               .SingleOrDefault();
                                            return (orderAttr != null ? orderAttr.Order : p.MetadataToken);
                                         });
         IEnumerable<string> allowedFields = fieldList.Split(new char[] { ',', ';', ' ' },
                                                             StringSplitOptions.RemoveEmptyEntries);
         formatter(obj, sb, properties, allowedFields);
         return sb.ToString();
         }

      private static string objectStringFormatter<T>(T obj, StringBuilder sb, IEnumerable<PropertyInfo> properties, IEnumerable<string> allowedFields)
         {
         foreach (var p in properties)
            {
            if (allowedFields.Count() == 0
            || allowedFields.Contains(p.Name))
               {
               if (sb.Length == 0)
                  {
                  sb.Append("(");
                  }
               else
                  {
                  sb.Append(",");
                  }
               string name = p.Name;
               var value = p.GetValue(obj, null);
               if (value != null && value.GetType() == typeof(string))
                  value = value.ToString().Trim();
               sb.AppendFormat("{0}={1}", name, value);
               }
            }
         if (sb.Length > 0) sb.Append(")");
         return sb.ToString();
         }

      private static string delimittedStringFormatter<T>(T obj, StringBuilder sb, IEnumerable<PropertyInfo> properties, IEnumerable<string> allowedFields, string delimitter = ",")
         {
         foreach (var p in properties)
            {
            if (allowedFields.Count() == 0
            || allowedFields.Contains(p.Name))
               {
               if (sb.Length > 0)
                  {
                  sb.Append(delimitter);
                  }
               var value = p.GetValue(obj, null);
               if (value != null && value.GetType() == typeof(string) && value.ToString().Contains(delimitter))
                  {
                  sb.AppendFormat("\"{0}\"", value.ToString().Trim());
                  }
               else
                  {
                  sb.AppendFormat("{0}", value);
                  }
               }
            }
         return sb.ToString();
         }

      private static string commaDelimittedStringFormatter<T>(T obj, StringBuilder sb, IEnumerable<PropertyInfo> properties, IEnumerable<string> allowedFields)
         {
         return delimittedStringFormatter(obj, sb, properties, allowedFields, ",");
         }

      private static string pipeDelimittedStringFormatter<T>(T obj, StringBuilder sb, IEnumerable<PropertyInfo> properties, IEnumerable<string> allowedFields)
         {
         return delimittedStringFormatter(obj, sb, properties, allowedFields, "|");
         }

      private static string delimittedPropertyNameFormatter<T>(T obj, StringBuilder sb, IEnumerable<PropertyInfo> properties, IEnumerable<string> allowedFields)
         {
         foreach (var p in properties)
            {
            if (allowedFields.Count() == 0
            || allowedFields.Contains(p.Name))
               {
               if (sb.Length > 0)
                  {
                  sb.Append(",");
                  }
               sb.Append(p.Name);
               }
            }
         return sb.ToString();
         }
      private class PublicPropertyConverter<T> : JavaScriptConverter
         {
         IEnumerable<string> _allowedFields = null;
         public PublicPropertyConverter(string fieldList = "")
            {
            _allowedFields = fieldList.Split(new char[] { ',', ';', ' ' },
                                                                StringSplitOptions.RemoveEmptyEntries);
            }

         public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
            {
            throw new NotImplementedException();
            }

         public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
            {
            IDictionary<string, object> results = new Dictionary<string, object>();

            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public
                                                         | BindingFlags.Instance);
            foreach (var p in properties)
               {
               if (_allowedFields.Count() == 0 || _allowedFields.Contains(p.Name))
                  results.Add(p.Name, p.GetValue(obj, null));
               }

            return results;
            }

         public override IEnumerable<Type> SupportedTypes
            {
            get { return new List<Type>() { typeof(T) }; }
            }
         }

      #endregion

      }
   }
