﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Blackriverinc.Framework.Utility
	{
	public static class TypeHandler
		{
		/// <summary>
		/// Get a type defined by either the simple Type Name, Namespace, and Assembly
		/// or some subset.
		/// <param name="typeName">
		/// Return a type specified by the simple name, perhaps qualified by the <paramref name="nameSpaceName"/>.
		/// </param>
		/// <param name="nameSpaceName">
		/// <list>
		///	<item>(null) => Return a type specified by the simple name only.</item>
		///	<item>(!null) => Return a type specified by the FQN.</item>
		/// </list>
		/// </param>
		/// <param name="assemblyName">
		/// <list>
		///	<item>(null) => Return a specified type in currently loaded assemblies.</item>
		///	<item>(!null) => Return a specified type in a specified, referenced assembly</item>
		/// </list>
		/// </param>
		/// </summary>
		/// <param name="typeName"></param>
		/// <param name="nameSpaceName"></param>
		/// <param name="assemblyName"></param>
		/// <returns></returns>
		public static Type GetReferencedTypeByName(string typeName, string nameSpaceName = null, string assemblyName = null)
			{
			Type type = null;

			Assembly assembly = null;
			if (assemblyName != null)
				{
				assembly = AppDomain.CurrentDomain.Load(assemblyName);
				if (assembly == null)
					throw new Exception(string.Format("Could not load assembly [{0}]", assemblyName));

				type = assembly.GetTypes()
						.Where(t => (nameSpaceName == null)
											? t.Name == typeName
											: t.FullName == string.Format("{0}.{1}", nameSpaceName, typeName))
							.FirstOrDefault();
				}
			else
				{
				type = AppDomain.CurrentDomain.GetAssemblies()
							.SelectMany(a => a.GetTypes())
							.Where(t => (nameSpaceName == null)
											? t.Name == typeName
											: t.FullName == string.Format("{0}.{1}", nameSpaceName, typeName))
							.FirstOrDefault();
				}
			return type;
			}
		}
	}
