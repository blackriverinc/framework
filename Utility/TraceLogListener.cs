﻿using System;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Text;


namespace Blackriverinc.Framework.Utility
{

   
   //---------------------------------------------------------------------------------------
   //
   //   Class:       AppTraceListener
   //   Author:      Black River Systems, Inc.
   //   Copyright:   Black River Systems, Inc 2015
   //                Granted full license to use and modify.

   //   Purpose:     Logs trace output to distinct files named according to the day
   //                month and year.  The log file captures all Trace and Debug output
   //                in addition to all exceptions published via the ExceptionManager.
   //
   //   Notes:       The path defaults to "c:\", but is read from the initializeData
   //                attribute that can be set in the application configuration file. if (
   //                the path is set in the configuration file, it must include the ending
   //                backslash (\).
   //
   //                Each write operation open a new streamwriter and) closes it
   //                after writing the content.  This is necessary to prevent the file
   //                from locking, we want a developer to be able to open the log file
   //                at any time to assist with troubleshooting.
   //
   //   07/09/2010 Mmohrbacher
   //
   //              + Timestamp at start of line
   //              + "Opening Log" "Closing Log" messages
   //              + No param c-tor opens log at location of Main Module/Logs
   //---------------------------------------------------------------------------------------
   public class AppTraceListener : TextWriterTraceListener
   {
      [Flags]
      enum TraceMask : uint
         {
         NULL = 0,
         LOG = 1,
         TRACE = 3,
         INFO = 5,
         WARN = 0x0801,
         FAIL = 0x8001
         }

      TraceMask _traceMask;

      //  Member Variables
      public string LogFilePath {get; private set; }

      private DateTime m_openDate = DateTime.Now;

      // true; when the last call was to WriteLine.
      private bool m_lineState = true;

      private bool m_logState = false;

      public int? _logMaxAgeDays = null;
      public int LogMaxAgeDays
         {
         get
            {
            if (!_logMaxAgeDays.HasValue)
               {
               _logMaxAgeDays = 10;
               string text = ConfigurationManager.AppSettings["LogMaxAgeDays"];
               int value = 0;
               if (Int32.TryParse(text, out value))
                  _logMaxAgeDays = value;
               }
            return _logMaxAgeDays.Value;
            }
         }

      #region "DisplayTaskBoolean"
      // Display TaskID inline in Message Line Header
      public bool DisplayTaskID
      {
         get;
         set;
      }
      #endregion

      #region "Constructors"

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Default Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use.
      //
      //---------------------------------------------------------------------------------------
      public AppTraceListener()
      {
         DisplayTaskID = false; ;
         initialize("Trace");
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use with a text writer and a name.
      //
      //---------------------------------------------------------------------------------------
      public AppTraceListener(TextWriter writer, string name) :
         base(writer, name)
      {
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use with a text writer and a name.
      //
      //---------------------------------------------------------------------------------------
      public AppTraceListener(TextWriter writer) :
         base(writer)
      {
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use with a text writer and a name.
      //
      //---------------------------------------------------------------------------------------
      public AppTraceListener(Stream stream) :
         base(stream)
      {
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use with a text writer and a name.
      //
      //---------------------------------------------------------------------------------------
      public AppTraceListener(Stream stream, string name) :
         base(stream, name)
      {
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use with a text writer and a name.
      //
      //   Notes:       The fileName is assumed to contain a directory location
      //                including the ending \.  The fileName is passed from the config file
      //                using the form:  initializeData="c:\temp\test\".
      //
      //                The AppTraceListener.log file is used to initialize the writer
      //                but all the actual content is logged to files mm-dd-yyyy.log
      //
      //---------------------------------------------------------------------------------------
      public AppTraceListener(string fileName)
      {
         initialize(fileName);
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Constructor
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Prepares the object for use with a text writer and a name.
      //
      //   Notes:       The fileName is assumed to contain a directory location
      //                including the ending \.  The fileName is passed from the config file
      //                using the form:  initializeData="c:\temp\test\".
      //
      //                The AppTraceListener.log file is used to initialize the writer
      //                but all the actual content is logged to files mm-dd-yyyy.log
      //        
      //---------------------------------------------------------------------------------------
      public AppTraceListener(string logPath, string name)
      {
         initialize(Path.Combine(logPath??"", name));
      }

      #endregion

      #region "Public Methods"

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Write
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the Write method to initialize the correct writer.
      //
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void Write(string message)
      {
      if (canWrite(message))
         {
         //  Verify the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.Write(format(message));

         //  Close the writer
         Writer.Close();
         }
      }
      //---------------------------------------------------------------------------------------
      //
      //   Method:      Write
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the Write method to initialize the correct writer.
      //
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void Write(object o)
      {
         //  Verify the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.Write(format("{0}", o));

         //  Close the writer
         this.Writer.Close();

      }


      //---------------------------------------------------------------------------------------
      //
      //   Method:      Write
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the Write method to initialize the correct writer.
      //
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void Write(object o, string category)
      {
         //  Verify the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.Write(format("{0}", o));

         //  Close the writer
         this.Writer.Close();
      }
      //---------------------------------------------------------------------------------------
      //
      //   Method:      Write
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the Write method to initialize the correct writer.
      //
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void Write(string message, string category)
      {
         //  Verif (y the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.Write(format("{0}", message), category);

         //  Close the writer
         Writer.Close();
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      WriteLine
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the WriteLine method to initialize the correct writer.
      //
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void WriteLine(string message)
      {
      if (canWrite(message))
         {
         //  Verify the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.WriteLine(format("{0}", message));
         m_lineState = true;

         //  Close the writer
         Writer.Close();
         }
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      WriteLine
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the WriteLine method to initialize the correct writer.
      //        
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void WriteLine(object o)
      {
         //  Verif (y the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.WriteLine(format("{0}", o));
         m_lineState = true;

         //  Close the writer
         Writer.Close();

      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      WriteLine
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the WriteLine method to initialize the correct writer.
      //        
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void WriteLine(object o, string category)
      {
         //  Verif (y the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.WriteLine(format("{0}", o), category);
         m_lineState = true;

         //  Close the writer
         Writer.Close();
      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      WriteLine
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the WriteLine method to initialize the correct writer.
      //        
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void WriteLine(string message, string category)
      {
         //  Verif (y the writer is accessing the correct file
         prepareWriter();

         //  Call the base method to write the output
         base.WriteLine(format("{0}", message), category);
         m_lineState = true;

         //  Close the writer
         Writer.Close();

      }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      Flush
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Overrides the Flush method to initialize hit the correct writer.
      //        
      //   Notes:       The writer must be opened and) closed for each operation
      //                to prevent the file from being locked (at any time a developer
      //                can manually open the log file to troubleshoot a problem).
      //
      //---------------------------------------------------------------------------------------
      override public void Flush()
      {
         //  Close the writer
         Writer.Close();
      }

      override public void Close()
      {
		if (m_logState)
			{
			//  Verify the writer is accessing the correct file
			prepareWriter();

			string logPath = String.Format("{1}-{0:yyyyMMdd}.log", m_openDate, LogFilePath);
			base.WriteLine(format(String.Format("--- Closing Log : {0} ---",
																  Path.GetFileName(logPath.ToString()))));
			m_lineState = true;

			System.Diagnostics.Trace.Listeners.Remove(this);

			base.Close();

			deleteOldLogFiles();
			}
      }

      #endregion
 
      #region "Private Methods"

      private void deleteOldLogFiles()
      {
         // Clean up old logs
         string baseDir = Path.GetDirectoryName(LogFilePath);
         string filePattern = string.Format("{0}*.log", Path.GetFileNameWithoutExtension(LogFilePath));
         foreach (var logName in Directory.EnumerateFiles(baseDir, filePattern))
         {
            // Remove all files older than 10 days
            if (File.GetCreationTime(logName).AddDays(LogMaxAgeDays) < DateTime.Now)
            {
               File.Delete(logName);
               Debug.WriteLine(string.Format("   Removed Log: {0}", logName));
            }
         }
      }
      
      private string format(string fmt, params object[] arguments)
      {
         if (m_lineState)
         {
            m_lineState = false;
            StringBuilder msgBuilder = new StringBuilder();
            msgBuilder.AppendFormat("{0:HH:mm:ss} ", DateTime.Now);
            if (DisplayTaskID)
               msgBuilder.AppendFormat("[{0}]",
                  System.Threading.Thread.CurrentThread.ManagedThreadId);
            msgBuilder.Append(" : ");
            return msgBuilder.AppendFormat(fmt, arguments).ToString();
         }
         else
            return string.Format(fmt, arguments);
      }


      private void initialize(string name)
         {
         try
            {
            string traceMask = ConfigurationManager.AppSettings["TraceMask"]??"";
            Enum.TryParse(traceMask.Replace('|', ',')
                                   .Replace(' ', ','), true, out _traceMask);

            if (_traceMask.HasFlag(TraceMask.LOG))
               {
               string baseDir = string.Empty;

               if (!Path.IsPathRooted(name))
                  {
                  Process process = Process.GetCurrentProcess();
                  ProcessModule mainModule = process.MainModule;
                  baseDir = System.IO.Path.GetDirectoryName(mainModule.FileName);
                  LogFilePath = string.Format(@"{0}\Logs\{1}", baseDir, name);
                  }
               else
                  LogFilePath = name;

               baseDir = System.IO.Path.GetDirectoryName(LogFilePath);
               if (!Directory.Exists(baseDir))
                  Directory.CreateDirectory(baseDir);

               Trace.WriteLine(string.Format("Opening TraceListener Log at '{0}'",
                              LogFilePath));

               System.Diagnostics.Trace.Listeners.Add(this);
               }
            }
         catch (Exception ex)
            {
            Trace.Fail(ex.ToString());
            }
         }

      //---------------------------------------------------------------------------------------
      //
      //   Method:      prepareWriter
      //   Author:      Black River Systems, Inc.
      //   Purpose:     Creates a writer and prepares for use.
      //
      //---------------------------------------------------------------------------------------
      private void prepareWriter()
      {
         DateTime now = DateTime.Now;
         StringBuilder logPath = new StringBuilder();

         if (now.Date != m_openDate.Date)
         {
            logPath.AppendFormat("{1}-{0:yyyyMMdd}.log", m_openDate, LogFilePath);
            this.Writer = TextWriter.Synchronized(new StreamWriter(File.Open(logPath.ToString(), FileMode.Append)));
            logPath.Length = 0;

            base.Flush();

            m_openDate = now.Date;
            m_logState = false;
            m_lineState = true;
         }

         logPath.AppendFormat("{1}-{0:yyyyMMdd}.log", m_openDate, LogFilePath);

         this.Writer = TextWriter.Synchronized(new StreamWriter(File.Open(logPath.ToString(), FileMode.Append)));

         if (!m_logState)
         {
            m_logState = true;
            base.WriteLine(format(String.Format("--- Opening Log : {0} ---", Path.GetFileName(logPath.ToString()))));
            m_lineState = true;
         }

      }

      private bool canWrite(string message)
         {
         TraceMask traceFlag = TraceMask.INFO;
         string[] tokens = message.Split(new char[] {':', ' '});
         Enum.TryParse(tokens[0], out traceFlag);
         return _traceMask.HasFlag(traceFlag);
         }

      #endregion

   };

}
