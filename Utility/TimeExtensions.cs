﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blackriverinc.Framework.Utility.Time
   {
   /// <summary>
   /// Static methods to compute time-periods relative to a Date.
   /// All periods are half-closed intervals [BeginDate, EndDate)
   /// </summary>
   public static class TimeExtensions
      {
      public static Tuple<DateTime, DateTime> LastMonth(this DateTime date)
         {
         DateTime beginDate = date.AddMonths(-1)
                                  .AddDays(-(date.Day - 1))
                                  .Subtract(date.TimeOfDay);

         DateTime endDate = date.AddDays(-(date.Day - 1))
                                .Subtract(date.TimeOfDay)
                                .AddTicks(-1);                                 
         return new Tuple<DateTime,DateTime>(beginDate, endDate);
         }

      public static Tuple<DateTime, DateTime> LastWeek(this DateTime date)
         {
         DateTime beginDate = date.AddDays(-7)
                                  .AddDays(-(date.DayOfWeek - DayOfWeek.Sunday))
                                  .Subtract(date.TimeOfDay);

         DateTime endDate = date.AddDays(-(date.DayOfWeek - DayOfWeek.Sunday)) 
                                .Subtract(date.TimeOfDay)
                                .AddTicks(-1);
         return new Tuple<DateTime, DateTime>(beginDate, endDate);
         }

      public static Tuple<DateTime, DateTime> Yesterday(this DateTime date)
         {
         DateTime beginDate = date.AddDays(-1)
                                  .Subtract(date.TimeOfDay);

         DateTime endDate = date.Subtract(date.TimeOfDay)
                                .AddTicks(-1);
         return new Tuple<DateTime, DateTime>(beginDate, endDate);
         }

      public static DateTime LastMonthStart(this DateTime date)
         {
         return date.LastMonth().Start();
         }

      public static DateTime LastMonthEnd(this DateTime date)
         {
         return date.LastMonth().End();
         }

      public static DateTime Start(this Tuple<DateTime, DateTime> period)
         {
         return period.Item1;
         }

      public static DateTime End(this Tuple<DateTime, DateTime> period)
         {
         return period.Item2;
         }
      }
   }
