﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Blackriverinc.Framework.Utility
{
    public static class ValidationHelper<T>
    {
        public static int Validate(T entity)
        {
            IList<ValidationResult> validationResults = new List<ValidationResult>();

            Validator.TryValidateObject(entity,
                                       new ValidationContext(entity),
                                       validationResults,
                                       true);

            foreach (var validationResult in validationResults)
            {
                LogTestHelper.WriteLog(string.Format("*Validation Failed* {0} : {1}",
                      string.Join(", ", validationResult.MemberNames),
                      validationResult.ErrorMessage));
            }
            return validationResults.Count;
        }

    }
}
