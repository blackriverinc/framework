﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Blackriverinc.Framework.Utility
{
    public class TestHelper<T> where T : class
    {
        /// <summary>
        /// Create an instance of an object with non-public constructors.
        /// </summary>
        /// <param name="args">Array of c-tor arguments; a full-boat of parameters must always
        /// be passed...no defaults!</param>
        /// <returns>Instance of Type T</returns>
        public static T CreateInstance(params object[] args)
        {
            Type type = typeof(T);

            // Interpret the Method Signature
            List<Type> signature = new List<Type>();
            foreach (var arg in args)
                signature.Add(arg.GetType());

            // Fetch the matching constructor
            ConstructorInfo ctor = type.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public,
                                                       null,
                                                       signature.ToArray(),
                                                       new ParameterModifier[] { });

            T instance = (ctor.Invoke(args) as T);
            if (instance == null)
                throw new InvalidOperationException();

            return instance;
        }

        /// <summary>
        /// Call a Method. 
        /// </summary>
        /// <param name="instance">Object to invoke method on.</param>
        /// <returns>true : sucess; false : falied</returns>
        public static object CallNonPublicMethod(T instance, string methodName, params object[] args)
        {
            object result = null;

            // Interpret the Method Signature
            List<Type> signature = new List<Type>();
            foreach (var arg in args)
                signature.Add(arg.GetType());

            // Fetcj Type object
            Type type = instance.GetType();

            MethodInfo method = type.GetMethod(methodName,
                                             BindingFlags.NonPublic | BindingFlags.Instance,
                                             null,
                                             signature.ToArray(),
                                             null);
            try
            {
                result = method.Invoke(instance, args);
            }
            catch (System.Reflection.TargetInvocationException tie)
            {
                Exception exp = tie;
                while (exp.InnerException != null)
                {
                    exp = exp.InnerException;
                }
                throw exp;
            }
            return result;
        }

        public static object CallStaticMethod(string methodName, params object[] args)
        {
            object result = null;

            // Interpret the Method Signature
            List<Type> signature = new List<Type>();
            foreach (var arg in args)
                signature.Add(arg.GetType());

            // Fetcj Type object
            Type type = typeof(T);

            MethodInfo method = type.GetMethod(methodName,
                                             BindingFlags.NonPublic | BindingFlags.Static,
                                             null,
                                             signature.ToArray(),
                                             null);
            result = method.Invoke(type, args);

            return result;
        }
    }

    public static class TestHelper
    {
        public static string UniqueId()
        {
            return string.Format("{0:ffffmm}", Now);
        }

        public static string UniqueName(string name)
        {
            return string.Format("{0}-{1}", name, UniqueId());
        }

        public static DateTime Now
        {
            get { return DateTime.UtcNow; }
            private set { }
        }

        public static string PublicPropertiesMatch(object expected, object actual)
        {
            PropertyInfo[] expectedProperties = expected.GetType().GetProperties(BindingFlags.Public
                                                  | BindingFlags.Instance
                                                  | BindingFlags.DeclaredOnly);
            PropertyInfo[] actualProperties = actual.GetType().GetProperties(BindingFlags.Public
                                                  | BindingFlags.Instance);
            foreach (var expectedProperty in expectedProperties)
            {
                string propertyName = expectedProperty.Name;

                Type expectedType = expectedProperty.PropertyType;

                var expectedValue = expectedProperty.GetValue(expected, null);
                if (expectedValue != null && expectedValue.GetType() == typeof(string))
                    expectedValue = expectedValue.ToString().Trim();

                // Look for a matching property
                var actualProperty = actualProperties
                                        .FirstOrDefault(ap => (ap.Name == propertyName));
                if (actualProperty == null)
                    return string.Format("Property[{0}] Expected({1}); Actual(*Missing Property*)",
                                                           propertyName, expectedValue);


                var actualValue = actualProperty.GetValue(actual, null);
                if (actualValue != null && actualValue.GetType() == typeof(string))
                    actualValue = actualValue.ToString().Trim();

                if (!((expectedValue == null && actualValue == null) ||
                       expectedValue.Equals(actualValue)))
                    return string.Format("Property[{0}] Expected=({1}); Actual=({2})",
                                                     propertyName, expectedValue, actualValue);
            }

            return null;
        }

        public static void AssertPublicPropertiesMatch(object expected, object actual)
        {
            string result = PublicPropertiesMatch(expected, actual);
            if (!string.IsNullOrEmpty(result))
                throw new AssertFailedException(result);
        }

        public static void AssertPublicPropertiesMatchExcept(object expected, object actual, string exceptList = "")
        {
            IList<string> except = exceptList
                                .Split(new char[] { ',', ' ', ';' }, StringSplitOptions.RemoveEmptyEntries)
                                .ToList();

            PropertyInfo[] expectedProperties = expected.GetType().GetProperties(BindingFlags.Public
                                                  | BindingFlags.Instance
                                                  | BindingFlags.DeclaredOnly);

            PropertyInfo[] actualProperties = actual.GetType().GetProperties(BindingFlags.Public
                                                  | BindingFlags.Instance);
            foreach (var expectedProperty in expectedProperties.Where(ep => !except.Contains(ep.Name)))
            {
                string propertyName = expectedProperty.Name;

                Type expectedType = expectedProperty.PropertyType;

                var expectedValue = expectedProperty.GetValue(expected, null);
                if (expectedValue != null && expectedValue.GetType() == typeof(string))
                    expectedValue = expectedValue.ToString().Trim();

                // Look for a matching property
                var actualProperty = actualProperties
                                        .FirstOrDefault(ap => (ap.Name == propertyName));
                if (actualProperty == null)
                    throw new AssertFailedException(string.Format("Property[{0}] Expected({1}); Actual(*Missing Property*)",
                                                           propertyName, expectedValue));


                var actualValue = actualProperty.GetValue(actual, null);
                if (actualValue != null && actualValue.GetType() == typeof(string))
                    actualValue = actualValue.ToString().Trim();

                if (!((expectedValue == null && actualValue == null) ||
                       expectedValue.Equals(actualValue)))
                    throw new AssertFailedException(string.Format("Property[{0}] Expected=({1}); Actual=({2})",
                                                     propertyName, expectedValue, actualValue));
            }
        }
    }
}

