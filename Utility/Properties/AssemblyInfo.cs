﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Framework Utililty")]
[assembly: AssemblyDescription("Common Classes and Extension Methods")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Black River Systems, Inc of Illinois")]
[assembly: AssemblyProduct("Framework.Utility")]
[assembly: AssemblyCopyright("Copyright © Black River Systems, Inc. of Illinois 2011 - 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2acf2c28-6c95-4619-a6e7-82eef7d36aba")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
