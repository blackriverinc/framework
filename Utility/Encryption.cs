﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Blackriverinc.Framework.Utility
   {
   public class Encryption
      {
      //-----------------------------------------------------------------------------
      // Generate a cryptographically strong random hex password 8 chars in length
      public static string GeneratePassword()
         {
         byte[] random = new byte[4];
         StringBuilder pwd = new StringBuilder();
         using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
            rng.GetBytes(random);
            foreach (byte b in random)
               {
               pwd.Append(b.ToString("0X"));
               }
            }
         return pwd.ToString();
         }

      //-----------------------------------------------------------------------------

      private RijndaelManaged _moRijn = null;

		private void initialize(string seedString)
			{
			_moRijn = new RijndaelManaged();
			ASCIIEncoding converter = new ASCIIEncoding();

			_moRijn.Key = converter.GetBytes("casenikeyurtrapt");
			_moRijn.IV = new byte[_moRijn.BlockSize / 8];
			byte[] seed = converter.GetBytes(seedString);
			int j = 0;
			for (int i = 0; i < _moRijn.IV.Length; i++)
				{
				if (i >= seed.Length)
					j++;
				_moRijn.IV[i] = seed[i - j * seed.Length];
				}
			}

		// Initializes the Rijndael encryption object
		public Encryption(string seed = null)
			{
			initialize(seed ?? "list1951java0936onyx1024tort2018");
			}

      //-----------------------------------------------------------------------------
      // Encrypts using Rijndael 128-bit and then Base-64 encodes the result
      public string Encrypt(string plainText)
         {
         ASCIIEncoding converter = new ASCIIEncoding();
         return Convert.ToBase64String(Encrypt(converter.GetBytes(plainText)));
         }

      //-----------------------------------------------------------------------------

      public byte[] Encrypt(byte[] plainText)
         {
         MemoryStream msEncrypt = new MemoryStream();

         ICryptoTransform encryptor = _moRijn.CreateEncryptor();
         using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
            {
            csEncrypt.Write(plainText, 0, plainText.Length);
            csEncrypt.FlushFinalBlock();
            }

         byte[] encrypted = msEncrypt.ToArray();
         return encrypted;
         }

      //-----------------------------------------------------------------------------
      // Base-64 decodes the input and then decrypts using Rijndael 128-bit

      public string Decrypt(string cryptText)
         {
         // Decode, decrypt, and trim trailing blanks
         ASCIIEncoding converter = new ASCIIEncoding();
			string plainText = converter.GetString(Decrypt(Convert.FromBase64String(cryptText)));
			return plainText.Trim(new char[] {'\0'});
         }

      //-----------------------------------------------------------------------------

      public byte[] Decrypt(byte[] cryptText)
         {
         byte[] plainText = new byte[cryptText.Length];
         MemoryStream msDecrypt = new MemoryStream(cryptText);
         ICryptoTransform decryptor = _moRijn.CreateDecryptor();
         using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
            {
            if (plainText.Length > 0)
               csDecrypt.Read(plainText, 0, cryptText.Length);
            }
         return plainText;
         }

      }
   }
