﻿#region Copyright
// <copyright file='AuditRequests.cs' company='Allstate'>2014</copyright>
// 2775 Sanders Road, Northbrook, Illinois, 60062, U.S.A.
// All rights reserved.
//
// This program contains proprietary and confidential information and trade
// secrets of Allstate Insurance Company. This program may not be duplicated,
// disclosed or provided to any third parties without the prior written consent
// of Allstate Insurance Company. Disassembly or decompilation of the software 
// and/or reverse engineering of the object code are prohibited.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PAWorkIntakeUI.Web.Extensions
{
   public static class HtmlHelperExtensions
   {
      public static string ActivePage(this HtmlHelper helper, string action, string controller)
      {
         string classValue = "";
         string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
         string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();
         if (currentController == controller && currentAction == action)
         {
            classValue = "selected";
         }
         return classValue;
      }
   }
}