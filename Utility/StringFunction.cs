﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace Blackriverinc.Framework.Utility
	{
	public static class StringFunction
		{
		static Regex camelCaseParser;
		static Regex commandLineParser;
      static Regex quotedTokensParser;

        public const string EmailPattern 
                = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" 
                + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
		public enum DictionaryNotFoundOptions
			{
			Ignore,
			AddKey,
			ThrowException
			}

		static StringFunction()
			{
			camelCaseParser = new Regex(@"(?<token>[A-Z]+[a-z0-9]*)", RegexOptions.IgnorePatternWhitespace);
         commandLineParser = new Regex(@"(?: (?:/|-) (?<keyword> \w+ )(?: \s*=\s* )(?<value> \S+ ) )",
                                                         RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
         quotedTokensParser = new Regex(@"(?: ((?:\s*)(?<value>(?:(?:""[^""]*"")|(?:'[^']*')|(?:\S+))))?)",
                    RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
         }

      public static string StandardNameBuilder(string lastName, string firstName, string alias)
      {
         StringBuilder bldr = new StringBuilder();
         bldr.AppendFormat("{0}, {1} ", lastName, firstName);
         if (!string.IsNullOrWhiteSpace(alias))
            bldr.AppendFormat(@" ""{1}""", alias);

         return bldr.ToString();
      }

		/// <summary>
		/// Converts 'CamelCased' string into a delimitted string. 'CamelCase' => 'Camel Case'.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="delimitter">Default is ' '</param>
		/// <returns></returns>
		public static string CamelCaseToDelimitted(this string input, char delimitter = ' ')
			{
			StringBuilder result = new StringBuilder();

			if (input != null)
				{
				MatchCollection matches = camelCaseParser.Matches(input);
				foreach (Match match in matches)
					result.AppendFormat("{0}{1}", match.Groups["token"].Value, delimitter);
				}

			return result.ToString().Trim(new char[] { delimitter });
			}

		public static string DelimittedToCamelCase(this string input, string delimitters = " ")
			{
			StringBuilder result = new StringBuilder();

			if (input != null)
				{
				char[] _delimitters = new char[delimitters.Length];
				for (int i = 0; i < delimitters.Length; i++)
					{
					_delimitters[i] = delimitters[i];
					}
				string[] components = input.Split(_delimitters, StringSplitOptions.RemoveEmptyEntries);
				foreach (string component in components)
					{
					result.AppendFormat("{0}{1}", component.Substring(0, 1).ToUpper(),
																		 component.Substring(1));
					}
				}

			return result.ToString();
			}

		public static string SafeSubstring(this string input, int startIndex, int? length = null)
			{
			if (!length.HasValue)
				length = input.Length - startIndex;

			if (length > input.Length)
				length = input.Length;

			return input.Substring(startIndex, length.Value);
			}

		public static string Squeeze(this string input, string delimiter = " ")
			{
			String result = string.Empty;

			if (input != null)
				{
				char[] _delimitters = new char[delimiter.Length];
				for (int i = 0; i < delimiter.Length; i++)
					{
					_delimitters[i] = delimiter[i];
					}
				string[] components = input.Split(_delimitters, StringSplitOptions.RemoveEmptyEntries);
				result = string.Join("", components);
				}

			return result;
			}

		/// <summary>
		/// Evaluates input and comparand by removing the delimitters between words
		/// in the two strings, and doing a case-invariant comparision on the results.
		/// </summary>
		public static bool Equivalent(this string input, string comparand, string delimitter = " ")
			{
			return input
								 .Trim()
								 .Squeeze(delimitter)
				 .Equals(comparand
								 .Trim()
								 .Squeeze(delimitter),
							StringComparison.OrdinalIgnoreCase);
			}

		public static string UrlEncode(this string input)
			{
			return HttpUtility.UrlEncode(input);
			}

		public static string HtmlDecode(this string input)
			{
			return HttpUtility.HtmlDecode(input);
			}

		public static string HtmlEncode(this string input)
			{
			return HttpUtility.HtmlEncode(input);
			}

		public static System.Collections.Specialized.NameValueCollection QueryStringDecode(this string input)
			{
			return HttpUtility.ParseQueryString(input);
			}

		/// <summary>
		/// Parse inpput string for Command-line style arguments
		/// </summary>
		/// <param name="input"></param>
		/// <param name="dictionary"></param>
		/// <param name="amendable">true : add new words to keyWords; false : throw Exception</param>
		/// <returns></returns>
		public static IDictionary<string, object> CommandLineParser(this string input, IDictionary<string, object> dictionary,
																						DictionaryNotFoundOptions option = DictionaryNotFoundOptions.Ignore)
			{

			MatchCollection matches = commandLineParser.Matches(input);
			if (matches.Count == 0)
				throw new ArgumentOutOfRangeException(string.Format("{0} : Does not parse.", input));

			foreach (Match match in matches)
				{
				string keyword = match.Groups["keyword"].Value;
				string value = match.Groups["value"].Value;

				if (!dictionary.ContainsKey(keyword))
					{
					switch (option)
						{
						case DictionaryNotFoundOptions.AddKey:
							dictionary.Add(keyword, value);
							break;
						case DictionaryNotFoundOptions.ThrowException:
							throw new ArgumentOutOfRangeException(keyword, "Not in list of acceptable keywords.");
						default:
							Debug.WriteLine(string.Format("{0} : Not in list of known keywords.", keyword));
							break;
						}
					}
				else
					dictionary[keyword] = value;
				}

			return dictionary;
			}

      public static IList<string> Tokenize(this string input)
         {
         List<string> results = new List<string>();

         MatchCollection matches = quotedTokensParser.Matches(input);
         if (matches.Count == 0)
            throw new ArgumentOutOfRangeException(string.Format("{0} : Does not parse.", input));

         foreach (Match match in matches)
            {
            if (!string.IsNullOrWhiteSpace(match.Value))
               results.Add(match.Value);
            }

         return results;
         }

		/////////////////////////////////////////////////////////////////////////////
		/// <summary>
		/// Resturns a subsetof the cardinal numbers as a string representing
		/// its name.
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		public static string CardinalNumbers(string numberString)
			{

			ushort number = ushort.MaxValue;
			ushort.TryParse(numberString, out number);
			return CardinalNumbers(number);
			}

		public static string CardinalNumbers(ushort number)
			{
			if (number == 0)
				return "zero";

			if (number >= 100)
				throw new ArgumentOutOfRangeException("number",
																string.Format("Input out of range [0..99]"));

			string[] onesPlace = 
                { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", 
                  "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", 
                                    "eighteen", "ninteen"};
			string[] tensPlace = { "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

			string result = "";

			int ones = 0;
			int tens = (int)Math.DivRem(number, 10, out ones);
			result = string.Format("{0}{2}{1}",
							tensPlace[tens],
							(number < 20) ? onesPlace[number] : onesPlace[ones],
							(tens > 1 && ones > 0) ? "-" : "");

			return result;
			}

		/// <summary>
		/// Return the result of a case-insensitive match of the input
		/// against the pattern.
		/// </summary>
		/// <returns>true: match found; false : not found</returns>
		public static bool IsMatch(this string input, string pattern)
			{
			bool result = false;
			result = Regex.IsMatch(input, pattern,
												 RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

			return result;
			}

      public static string ReplaceEx(this string input, string pattern, string target)
      {
         Regex regex = new Regex(pattern, RegexOptions.Compiled|RegexOptions.IgnoreCase);

         return regex.Replace(input, target);
      }
		#region Serializers
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <param name="formatter"></param>
		/// <param name="fieldList"></param>
		/// <returns></returns>
		public static string PropertySerializer<T>(this T obj, Func<T, StringBuilder, PropertyInfo[], IEnumerable<string>, string> formatter, string fieldList = "")
			{
			StringBuilder sb = new StringBuilder();
			Type type = typeof(T);

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public
																		| BindingFlags.Instance
																		| BindingFlags.DeclaredOnly);
			IEnumerable<string> allowedFields = fieldList.Split(new char[] { ',', ';', ' ' },
																				 StringSplitOptions.RemoveEmptyEntries);
			formatter(obj, sb, properties, allowedFields);
			return sb.ToString();
			}

		public static string ToObjectString<T>(this T obj, string fieldList = "")
			{
			return obj.PropertySerializer<T>(objectStringFormatter, fieldList);

			}

		private static string objectStringFormatter<T>(T obj, StringBuilder sb, PropertyInfo[] properties, IEnumerable<string> allowedFields)
			{
			foreach (var p in properties)
				{
				if (allowedFields.Count() == 0
				|| allowedFields.Contains(p.Name))
					{
					if (sb.Length == 0)
						{
						sb.Append("(");
						}
					else
						{
						sb.Append(",");
						}
					string name = p.Name;
					var value = p.GetValue(obj, null);
					if (value != null && value.GetType() == typeof(string))
						value = value.ToString().Trim();
					sb.AppendFormat("{0}={1}", name, value);
					}
				}
			if (sb.Length > 0) sb.Append(")");
			return sb.ToString();
			}

		public static string ToCSVString<T>(this T obj, string fieldList = "")
			{
			return obj.PropertySerializer<T>(delimittedStringFormatter, fieldList);
			}

		private static string delimittedStringFormatter<T>(T obj, StringBuilder sb, PropertyInfo[] properties, IEnumerable<string> allowedFields)
			{
			foreach (var p in properties)
				{
				if (allowedFields.Count() == 0
				|| allowedFields.Contains(p.Name))
					{
					if (sb.Length > 0)
						{
						sb.Append(",");
						}
					var value = p.GetValue(obj, null);
					if (value != null && value.GetType() == typeof(string))
						{
						sb.AppendFormat("\"{0}\"", value.ToString().Trim());
						}
					else
						{
						sb.AppendFormat("{0}", value);
						}
					}
				}
			return sb.ToString();
			}


		public static string ToPropertyNameList<T>(this T obj, string fieldList = "")
			{
			return obj.PropertySerializer<T>(delimittedPropertyNameFormatter, fieldList);
			}

		private static string delimittedPropertyNameFormatter<T>(T obj, StringBuilder sb, PropertyInfo[] properties, IEnumerable<string> allowedFields)
			{
			foreach (var p in properties)
				{
				if (allowedFields.Count() == 0
				|| allowedFields.Contains(p.Name))
					{
					if (sb.Length > 0)
						{
						sb.Append(",");
						}
					sb.Append(p.Name);
					}
				}
			return sb.ToString();
			}


		public static string ToHtmlAttributes<T>(this T obj)
			{
			StringBuilder sb = new StringBuilder();
			Type type = typeof(T);

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance
																							| BindingFlags.DeclaredOnly);
			foreach (var p in properties)
				{
				string name = p.Name;
				var value = p.GetValue(obj, null);
				sb.AppendFormat(" {0} = '{1}'", name, value);
				}
			return sb.ToString();
			}

		private class PublicPropertyConverter<T> : JavaScriptConverter
			{
			IEnumerable<string> _allowedFields = null;
			public PublicPropertyConverter(string fieldList = "")
				{
				_allowedFields = fieldList.Split(new char[] { ',', ';', ' ' },
																					 StringSplitOptions.RemoveEmptyEntries);
				}

			public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
				{
				throw new NotImplementedException();
				}

			public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
				{
				IDictionary<string, object> results = new Dictionary<string, object>();

				Type type = obj.GetType();
				PropertyInfo[] properties = type.GetProperties(BindingFlags.Public
																			| BindingFlags.Instance);
				foreach (var p in properties)
					{
					if (_allowedFields.Count() == 0 || _allowedFields.Contains(p.Name))
						results.Add(p.Name, p.GetValue(obj, null));
					}

				return results;
				}

			public override IEnumerable<Type> SupportedTypes
				{
				get { return new List<Type>() { typeof(T) }; }
				}
			}

		/// <summary>
		/// Create a JSON string by using the JavaScriptSerializer with the
		/// ability to use custom JavaScriptConverter created just for this Type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <param name="fieldList"></param>
		/// <returns></returns>
		public static string ToJSONString<T>(this T obj, string fieldList = "")
			{
			StringBuilder sb = new StringBuilder();
			Type type = typeof(T);

			JavaScriptSerializer serializer = new JavaScriptSerializer();
			serializer.RegisterConverters(new List<JavaScriptConverter>() { new PublicPropertyConverter<T>(fieldList) });

			serializer.Serialize(obj, sb);

			return sb.ToString();
			}
		#endregion

		/// <summary>

		/// <summary>
		/// Return the n-th token from a string. If [n] > number of tokens, return the last token.
		/// </summary>
		public static string TokenByIndex(this string input, int index, char? delimitter = null)
			{
			char[] delimitters = (delimitter.HasValue)
					  ? (new char[] { delimitter.Value })
					  : (new char[] { });
			string[] tokens = input.Split(delimitters, StringSplitOptions.RemoveEmptyEntries);

			return (index < tokens.Length) ? tokens[index] : tokens[tokens.Length - 1];
			}

      public static string Bind(this string val, params object[] args)
         {
         return string.Format(val, args);
         }
      }
	}
