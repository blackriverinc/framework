﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Blackriverinc.Framework.DataStore;
using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.Parser
   {
   public class RecordParser<TResult> where TResult : class, new()
      {
      Regex _tokenizer;

      public RecordParser(Regex tokenizer)
         {
         _tokenizer = tokenizer;
         }

      public IEnumerable<TResult> ReadAll(string inputFileName)
         {

         long extractCount = 0;

         Trace.WriteLine(string.Format("TRACE: +++ Read Extracts From File {0} +++", inputFileName));

         IList<TResult> results = new List<TResult>();

         Dictionary<int, string> fieldMap = new Dictionary<int, string>();
         Type resultType = typeof(TResult);

         using (Stream stream = StreamFactory.Create(@"file://" + inputFileName, FileMode.Open, FileAccess.Read))
            {
            //--------------------------------------------------------
            // Create a custom parser.
            //--------------------------------------------------------
            var parser = new CsvParser<TResult>(_tokenizer);

            // Intercept token recognizer.
            parser.TokenReady += ((sender, trea) =>
            {
               var context = trea.Context;

               // Build the Position-to-Field Map from the "Header" Line.
               if (context.Line == 0)
                  {
                  fieldMap.Add(context.TokenIndex, trea.Token);
                  return;
                  }

               if (context.TokenIndex == 0
               && trea.Token == "EOF")
                  return;

               string propertyName = fieldMap[context.TokenIndex];
               if (string.IsNullOrEmpty(propertyName))
                  throw new Exception(string.Format("Field Index [{0}] does not correlate to a property of {1}",
                                                      context.TokenIndex, resultType.Name));

               var property = resultType.GetProperty(propertyName);
               if (property == null)
                  throw new Exception(string.Format("Field Name [{0}] does not correlate to a property of {1}", propertyName, resultType.Name));

               // Populate the result object with tokens parsed from the input record.
               string propertyTypeName = (!property.PropertyType.IsGenericType)
                                                ? property.PropertyType.Name
                                                : property.PropertyType.GetGenericArguments()[0].Name;
               switch (propertyTypeName)
                  {
                  case "Int32":
                  case "Int16":
                     int intValue = int.MinValue;
                     if (int.TryParse(trea.Token, out intValue))
                        property.SetValue(context.Result, intValue, null);
                     break;
                  case "DateTime":
                     DateTime dateValue = DateTime.MinValue;
                     if (DateTime.TryParse(trea.Token, out dateValue))
                        property.SetValue(context.Result, dateValue, null);
                     break;
                  default:
                     property.SetValue(context.Result, trea.Token, null);
                     break;
                  }
            });

            parser.LineEnd += ((sender, trea) =>
            {
               if (trea.Context.Line > 0 && !trea.CancelParse)
                  results.Add(trea.Context.Result);
               extractCount++;
            });

            parser.Transform(stream, results);

            }
         Trace.WriteLine(string.Format("TRACE: --- Read Extracts From File - {0} records. ---".Bind(extractCount)));
         return results;

         }
      }
   }
