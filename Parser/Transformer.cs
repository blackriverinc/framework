﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace Blackriverinc.Framework.Parser
   {
   #region TransformEvents

   /// <summary>
   /// StreamEvents are used by ScheduledJobs to signal observers that
   /// a stream of results is ready for action.
   /// </summary>
   public class TransformEventArgs<TResult> : EventArgs
      {
      public readonly object Context = null;
      public readonly TResult Results = default(TResult);
      public readonly bool Completed;

      public TransformEventArgs(object context, TResult results, bool completed = true)
         {
         Context = context;
         Results = results;
         Completed = completed;
         }
      }


   public delegate TResult TransformEventHandler<TResult>(object sender, TransformEventArgs<TResult> srea);

   #endregion

   public interface ITransformer<TSource, TResult>
      {
      /// <summary>
      /// TRansform Input to Output
      /// </summary>
      /// <returns>Output</returns>
      TResult Transform(TSource input, TResult output = default(TResult));

      /// <summary>
      /// A Result is available.
      /// </summary>
      event TransformEventHandler<TResult> DataReady;

      /// <summary>
      /// Signal the Transform to stop
      /// </summary>
      void SignalStop();
      }

   /// <summary>
   ///
   /// </summary>
   public abstract class Transformer<TSource, TResult> : ITransformer<TSource, TResult>
      {
      // Delegate references worker-bee
      protected Func<TSource, TResult, TResult> _function;

      #region DataReady
      public event TransformEventHandler<TResult> DataReady;

      /// <summary>
      /// Send notification to all registered subscribers to the 'DataReady'
      /// event.
      /// </summary>
      protected void OnDataReady(object context, TResult result, bool completed = true)
         {
         if (DataReady != null)
            {
            DataReady(this, new TransformEventArgs<TResult>(context, result, completed));
            }
         }
      #endregion

      #region Signal Stop
      private int _stopFlag;
      protected bool StopFlagSet
         {
         get
            {
            int stopFlag = 0;
            Interlocked.Exchange(ref stopFlag, _stopFlag);
            return (stopFlag > 0);
            }
         }
      /// <summary>
      /// Signal the STOP Event, as to whether anybody is listening,
      /// that is a different issue. 
      /// </summary>
      public void SignalStop()
         {
         Interlocked.Exchange(ref _stopFlag, 1);
         }

      #endregion

      #region Signal 'Write Output'
      private int _writeOutputFlag;
      protected bool WriteOutputFlagSet
         {
         get
            {
            int writeOutputFlag = 0;
            Interlocked.Exchange(ref writeOutputFlag, _writeOutputFlag);
            return (writeOutputFlag > 0);
            }
         }
      /// <summary>
      /// Signal the WRITEOUTPUT Event, as to whether anybody is listening,
      /// that is a different issue. 
      /// </summary>
      public void SetWriteOutput()
         {
         Interlocked.Exchange(ref _writeOutputFlag, 1);
         }

      public void ResetWriteOutput()
         {
         Interlocked.Exchange(ref _writeOutputFlag, 0);
         }

      #endregion

      public Transformer(Func<TSource, TResult, TResult> function = null)
         {
         _function = function;
         }

      public virtual TResult Transform(TSource input, TResult output)
         {
         if (_function != null)
            output = _function(input, output);
         else
            output = default(TResult);

         OnDataReady(this, output, true);

         return output;
         }
      }
   }
