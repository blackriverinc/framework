﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Blackriverinc.Framework.Parser
   {
   /// <summary>
   /// Aggregation of RegEx-based tokenizers :
   /// <list type="u">
   /// <item>NonVDate</item>
   /// <item>CSVList</item>
   /// <item>FunctionInvocation</item>
   /// </list>
   /// </summary>
   static public class Tokenizers
      {

      #region Non-validating DateFormat
      private static Regex _dateTokenizer;
      static public Regex NonVDate
         {
         get
            {
            return _dateTokenizer;
            }
         }
      #endregion

      #region CSVList
      // A list of Quoted and unquoted Values, separated by ',' character
      static Regex _CSVTokenizer;
      static public Regex CSVTokenizer
         {
         get
            {
            return _CSVTokenizer;
            }
         }
      #endregion

		#region PSVList
		// A list of Quoted and unquoted Values, separated by '|' character
		static Regex _PSVTokenizer;
		static public Regex PSVTokenizer
			{
			get
				{
				return _PSVTokenizer;
				}
			}
		#endregion

      #region SCSVTokenizer 
      // A list of Quoted and unquoted Values, separated by ';' character
      static Regex _SCSVTokenizer;
      static public Regex SCSVTokenizer
         {
         get
            {
            return _SCSVTokenizer;
            }
         }
      #endregion

		#region DSVTokenizer
		static string _delimitter = ",";
		// A list of Quoted and unquoted Values, separated by a parameterized delimitter
		static Regex _DSVTokenizer;
		static public Regex DSVTokenizer
			{
			get
				{
				return _DSVTokenizer;
				}
			}
		#endregion

      #region FunctionInvocation
      // FunctionName( ArgumentList )
      static Regex _functionInvocationTokenizer;
      static public Regex FunctionInvocation
         {
         get
            {
            return _functionInvocationTokenizer;
            }
         }
      #endregion

      static Tokenizers()
         {
         // Extracts individual fields from a comma-delimited string.
			_CSVTokenizer = new Regex(@"(?<Value>(?:([""])(?<QuotedValue>[^""]*)(?:\1)|[^,]*))(?:(\s*,\s*)|\Z)",
				RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

			_PSVTokenizer = new Regex(@"(?<Value>(?:([""])(?<QuotedValue>[^""]*)(?:\1)|[^\|]*))(?:(\s*\|\s*)|\Z)",
				RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

			_SCSVTokenizer = new Regex(@"(?<Value>(?:([""])(?<QuotedValue>[^""]*)(?:\1)|[^;]*))(?:(\s*;\s*)|\Z)",
            RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

			_DSVTokenizer = new Regex(string.Format(@"(?<Value>(?:([""])(?<QuotedValue>[^""]*)(?:\1)|[^;]*))(?:(\s*;\s*)|\Z)", _delimitter),
				RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

			_functionInvocationTokenizer = new Regex(@"(?<FunctionName> \s* \w[\w\d]*)\((?<ArgumentList>[^\)]*) \s* \)",
            RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.ExplicitCapture);

         _dateTokenizer = new Regex(@"(?<month>\d\d?)([-/])\s?(?<day>\d\d?)\1(?<year>\d{2,4})",
                        RegexOptions.Compiled);
         }
      }
   }
