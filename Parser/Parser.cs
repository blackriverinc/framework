﻿using System;
using System.IO;
using System.Text;

namespace Blackriverinc.Framework.Parser
   {
   public struct ParserContext<TResult>
      {
      /// <summary>
      /// Line Counter; 0-relative
      /// </summary>
      public long Line;

      /// <summary>
      /// Offset from start of Line
      /// </summary>
      public long Position;

      /// <summary>
      /// Index per Line; 0-relative
      /// </summary>
      public int TokenIndex;

      public TResult Result;

      public override string ToString()
         {
         return string.Format("Line = {0}, Position = {1}, TokenIndex = {2}",
                  Line, Position, TokenIndex);
         }

      }

   #region ParserEvents

   public class ParserEventArgs<TResult>
      {
      public ParserContext<TResult> Context;
      public readonly String Token = null;
      public readonly bool CancelParse;

      public override string ToString()
         {
         return string.Format("Token = '{0}' ; Context ( {1} )", Token, Context);
         }

      public ParserEventArgs(ParserContext<TResult> context, string token, bool cancelParse = true)
         {
         Context = context;
         Token = token;
         CancelParse = cancelParse;
         }
      }

   public delegate void ParserEventHandler<TResult>(object sender, ParserEventArgs<TResult> trea);

   public class ParserException<TResult> : ApplicationException
      {
      public readonly Parser<TResult> Parser;
      public readonly ParserEventArgs<TResult> ParserEventArgs;

      public override string Message
         {
         get
            {
            StringBuilder sb = new StringBuilder(ParserEventArgs.ToString());
            if (base.Message != null && base.Message.Length > 0)
               sb.AppendFormat("\n{0}", base.Message);
            return sb.ToString();
            }
         }
      public ParserException(Parser<TResult> parser, ParserEventArgs<TResult> pea, Exception innerException = null) :
         base("Parser Exception", innerException)
         {
         this.Parser = parser;
         this.ParserEventArgs = pea;
         }
      public ParserException(Parser<TResult> parser, ParserEventArgs<TResult> pea, string message) :
         base(message)
         {
         this.Parser = parser;
         this.ParserEventArgs = pea;
         }
      }


   #endregion

   /// <summary>
   /// A Parser is a sub-class of Transformer that emits tokens by
   /// parsing a Stream.
   /// </summary>
   public abstract class Parser<TResult> : Transformer<string, TResult>
      {
      #region Parser Event Handling

      public event ParserEventHandler<TResult> TokenReady;

      protected void OnTokenReady(ParserContext<TResult> context, string token)
         {
         if (TokenReady != null)
            {
            try
               {
               TokenReady(this, new ParserEventArgs<TResult>(context, token));
               }
            catch (ParserException<TResult> pexp)
               {
               OnParserError(context, token);
               if (pexp.ParserEventArgs.CancelParse)
                  throw;

               }
            }
         }

		public bool ErrorCaught
			{
			get;
			protected set;
			}
      public event ParserEventHandler<TResult> ParserError;
      protected void OnParserError(ParserContext<TResult> context, string token)
         {
			ErrorCaught = true;
         if (ParserError != null)
            {
            ParserError(this, new ParserEventArgs<TResult>(context, token));
            }
         }

      #endregion

      protected Parser(Func<string, TResult, TResult> transformer)
         : base(transformer)
         {
         }

      #region ITransformer
      public override TResult Transform(string input, TResult output = default(TResult))
         {
         return base.Transform(input, output);
         }
      #endregion

      }

   }
