﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.Parser
   {
   /// <summary>
   /// Accepts input stream of CSV data; emits a translated stream.
   /// </summary>
   public class CsvParser<TResult> : Parser<TResult> 
			where TResult  : class, new()
      {

      public event ParserEventHandler<TResult> LineStart;
      protected void OnLineStart(ParserContext<TResult> context, string token)
         {
         if (LineStart != null)
            {
            try
               {
               LineStart(this, new ParserEventArgs<TResult>(context, token));
               }
            catch (ParserException<TResult> pexp)
               {
               OnParserError(context, token);
               if (pexp.ParserEventArgs.CancelParse)
                  throw;

               }
            }
         }

      public event ParserEventHandler<TResult> LineEnd;
      protected void OnLineEnd(ParserContext<TResult> context, string token)
         {
         if (LineEnd != null)
            {
            try
               {
               LineEnd(this, new ParserEventArgs<TResult>(context, token, ErrorCaught));
					ErrorCaught = false;
               }
            catch (ParserException<TResult> pexp)
               {
               OnParserError(context, token);
               if (pexp.ParserEventArgs.CancelParse)
                  throw;

               }
            }
         }

      public event ParserEventHandler<TResult> StreamEnd;
      protected void OnStreamEnd(ParserContext<TResult> context, string token)
         {
         if (StreamEnd != null)
            {
            try
               {
               StreamEnd(this, new ParserEventArgs<TResult>(context, token));
               }
            catch (ParserException<TResult> pexp)
               {
               OnParserError(context, token);
               if (pexp.ParserEventArgs.CancelParse)
                  throw;

               }
            }
         }

      // Regular Expression recognizer
      Regex _regex;

      int _lineIndex = 0;

      /// <summary>
      /// Create a delimited-field parser using the 'recognizer' to 
      /// extract 'Value' and 'QuotedValue' fields.
      /// </summary>
      /// <param name="recognizer"></param>
      public CsvParser(Regex recognizer)
         : base(null)
         {
         //-------------------------------------------------
         // Validate the recognizer.
         //-------------------------------------------------
         string[] groupNames = recognizer.GetGroupNames();
         string [] requiredGroups = {"QuotedValue", "Value"};
         if ((from groupName in groupNames
              join requiredGroup in requiredGroups on groupName equals requiredGroup
              select groupName).Count() != requiredGroups.Length)
            {
            throw new ApplicationException(string.Format("recognizer : {1}\nThe 'recognizer' pattern must contain named groups ({0}) to work correctly.",
                        string.Join(", ", requiredGroups), recognizer.ToString()));
            }

         //-------------------------------------------------
         // Stash the recognizer.
         //-------------------------------------------------
         _regex = recognizer;

         _lineIndex = 0;

         if (_function == null)
            _function = function;
         }

      protected TResult function(string line, TResult result)
         {
         ParserContext<TResult> context = new ParserContext<TResult>();

         // Signal S-O-L
         if (result == null)
            result = new TResult();
         context.Result = result;
         context.Line = _lineIndex;
         context.Position = -1;
         context.TokenIndex = -1;
         OnLineStart(context, null);

         int tokenCount = 0;
         MatchCollection matches = _regex.Matches(line);
         foreach (Match match in matches)
            {
            if (line.Length > match.Index)
               {
               // Extract either "quoted" or unquoted values
               string token = (match.Groups["QuotedValue"].Success) ? match.Groups["QuotedValue"].Value : match.Groups["Value"].Value;

               context.Line = _lineIndex;
               context.Position = match.Index;
               context.TokenIndex = tokenCount++;
               OnTokenReady(context, token);
               }
            }

         // Signal E-O-L
         context.Line = _lineIndex;
         context.Position = -1;
         context.TokenIndex = -1;
         result = context.Result;
         OnLineEnd(context, null);

         OnDataReady(this, result, false);

         return result;
         }

      public IList<TResult> Transform(Stream input, IList<TResult> output)
         {
         IList<TResult> results = new List<TResult>();

         //-------------------------------------------------
         // Define the function performed by the CSVParser
         //-------------------------------------------------
         if (_function == null)
            _function = function;

         using (StreamReader reader = new StreamReader(input))
            {
            string line = null;
            while ((line = reader.ReadLine()) != null)
               {
               if (line.Length < 1 || line[line.Length - 1] == '\u001a')
                  break;

               Transform(line, default(TResult));

               if (StopFlagSet)
                  break;

               _lineIndex++;
               }
            }

         // Signal E-O-F
         OnStreamEnd(new ParserContext<TResult>()
         {
            Result = default(TResult),
            Line = -1,
            Position = -1,
            TokenIndex = -1
         }, null);

         return results;
         }

      /// <summary>
      /// Default parser uses the CSV Tokenizer to extract fields
      /// from each line of the input stream.
      /// </summary>
      public CsvParser()
         :  this(Tokenizers.CSVTokenizer)      
         {

         }
      }

   }
