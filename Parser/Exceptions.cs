﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.Parser
   {
   #region DataModelingException

   public class DataModelingArgs
      {
      public readonly int LineIndex;
      public readonly string FieldName;
      public readonly string Token;

      public DataModelingArgs(string fieldName, string token, int lineIndex)
         {
         LineIndex = lineIndex;
         FieldName = fieldName;
         Token = token;
         }

      public override string ToString()
         {
         return string.Format("lineIndex : {0}, fieldName = '{1}', token = '{2}'",
            LineIndex, FieldName, Token);
         }
      }

   public class DataModelingException : ApplicationException
      {
      private DataModelingArgs _dma;
      private object _sender;
      public DataModelingException(object sender, DataModelingArgs dma, string message = null) :
         base((message != null) ? message : "")
         {
         _sender = sender;
         _dma = dma;
         }
      public override string ToString()
         {
         return string.Format(@"DataModelingException ({0})", _dma);
         }
      public override string Message
         {
         get
            {
            return ToString() + " - " + base.Message;
            }
         }
      }
   #endregion

   #region TableEditorException

   public class TableEditorArgs
      {
      public readonly string ReportID;
      public readonly string TransformPath;
      public readonly string TransformFunction;

      public TableEditorArgs(string reportID, string transformPath = null, string transformFunction = null)
         {
         ReportID = reportID;
         TransformPath = transformPath;
         TransformFunction = transformFunction;
         }

      public override string ToString()
         {
         StringBuilder msgBldr = new StringBuilder();
         msgBldr.AppendFormat("reportID = '{0}'", ReportID);
         if (TransformPath != null)
            msgBldr.AppendFormat(",path = '{0}'", TransformPath);
         if (TransformFunction != null)
            msgBldr.AppendFormat(",function = '{0}'", TransformFunction);
         return msgBldr.ToString();
         }
      }

   public class TableEditorException : ApplicationException
      {
      private TableEditorArgs _dma;
      private object _sender;
      public TableEditorException(object sender, TableEditorArgs dma, string message = null) :
         base((message != null) ? message : "")
         {
         _sender = sender;
         _dma = dma;
         }
      public override string ToString()
         {
         return string.Format(@"({0})", _dma);
         }
      public override string Message
         {
         get
            {
            return ToString() + " - " + base.Message;
            }
         }
      }
   #endregion

   }

namespace Framework.Parser.Exceptions
   {
   #region ParserEvents

   public class TokenizerEventArgs<TContext>
      {
      public readonly TContext Context;
      public readonly String Token = null;
      public readonly bool CancelParse;

      public override string ToString()
         {
         return string.Format("Token = '{0}' ; Context ( {1} )", Token, Context);
         }

      public TokenizerEventArgs(TContext context, string token, bool cancelParse = true)
         {
         Context = context;
         Token = token;
         CancelParse = cancelParse;
         }
      }

   public delegate void TokenizerEventHandler<TContext>(object sender, ParserEventArgs<TContext> trea);

   public class TokenizerException<TContext> : ApplicationException
      {
      public readonly TokenizerEventArgs<TContext> TokenizerEventArgs;

      public override string Message
         {
         get
            {
            StringBuilder sb = new StringBuilder(TokenizerEventArgs.ToString());
            if (base.Message != null && base.Message.Length > 0)
               sb.AppendFormat("\n{0}", base.Message);
            return sb.ToString();
            }
         }
      public TokenizerException(TokenizerEventArgs<TContext> tea, Exception innerException = null) :
         base("Tokenizer Exception", innerException)
         {
           this.TokenizerEventArgs = tea;
         }
      public TokenizerException(TokenizerEventArgs<TContext> tea, string message) :
         base(message)
         {
         this.TokenizerEventArgs = tea;
         }
      }


   #endregion
 
   }
