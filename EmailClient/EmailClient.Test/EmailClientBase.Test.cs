﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using HRPlus.Common.EmailClient;
using Common.ConfigurationSettingsProvider;

namespace EmailClient.TestFixture
{
    
    
    /// <summary>
    ///This is a test class for EmailClientBase and is intended
    ///to contain all EmailClientBase Unit Tests
    ///</summary>
   [TestClass()]
   public class EmailClientBaseTest
      {

      string serverMockAddress = "mock:127.0.0.2:25";
      string serverMRSAddress = "mrs:127.0.0.2:25";
      string serverSMTPAddress = "smtp:mail.blackriverinc.com";

      private TestContext testContextInstance;

      /// <summary>
      ///Gets or sets the test context which provides
      ///information about and functionality for the current test run.
      ///</summary>
      public TestContext TestContext
         {
         get
            {
            return testContextInstance;
            }
         set
            {
            testContextInstance = value;
            }
         }


      #region Additional test attributes
      [ClassInitialize()]
      public static void MyClassInitialize(TestContext testContext)
         {
         
         }
      //
      //Use ClassCleanup to run code after all tests in a class have run
      //[ClassCleanup()]
      //public static void MyClassCleanup()
      //{
      //}
      //
      //Use TestInitialize to run code before running each test
      //[TestInitialize()]
      //public void MyTestInitialize()
      //{
      //}
      //
      //Use TestCleanup to run code after each test has run
      //[TestCleanup()]
      //public void MyTestCleanup()
      //{
      //}
      //
      #endregion


      /// <summary>
      ///A test for Create
      ///</summary>
      [TestMethod()]
      [DeploymentItem("EmailClient.dll")]
      public void Create_MockClient()
         {
         IEmailClient actual;
         actual = EmailClientFactory.Create(serverMockAddress);
         Assert.IsInstanceOfType(actual, typeof(Mock_EmailClient));
         }
      [TestMethod()]
      [DeploymentItem("EmailClient.dll")]
      public void Create_MRSClient()
         {
         IEmailClient actual;
         actual = EmailClientFactory.Create(serverMRSAddress);
         Assert.IsInstanceOfType(actual, typeof(MRS_EmailClient));
         }
      [TestMethod()]
      [DeploymentItem("EmailClient.dll")]
      public void Create_SMTPClient()
         {
         IEmailClient actual;
         actual = EmailClientFactory.Create(serverSMTPAddress);
         Assert.IsInstanceOfType(actual, typeof(Smtp_EmailClient));
         }
      }
}
