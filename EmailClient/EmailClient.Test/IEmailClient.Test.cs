﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using HRPlus.Common.EmailClient;

using HRPlus.ScheduledJobs.DataAccess;
using HRPlus.ScheduledJobs.DataView;

using Common.ConfigurationSettingsProvider;

namespace HRPlus.Common.EmailClient.TestFixture
{
    
    
    /// <summary>
    ///This is a test class for IEmailClient and is intended
    ///to contain all IEmailClient Unit Tests
    ///</summary>
   [TestClass()]
   public class IEmailClientTest
      {
      string serverMockAddress = "mock:127.0.0.2:25";
      string serverMRSAddress = "mrs:localhhost:13011";
      string serverSmtpAddress = "smtp:mail.blackriverinc.com";

      private TestContext testContextInstance;
		[Serializable]
		public class CountyTask
			{
			public string CountyName { get; set; }
			public string StateCode { get; set; }
			public string SourceName { get; set; }
			public int TaskID { get; set; }
			}

		ASCIIEncoding _encoding;
		IList<CountyTask> _dataList;
		DateTime _now;

		public IEmailClientTest()
			{
			_dataList = new List<CountyTask>
				{
				new CountyTask { CountyName = "Cook", StateCode = "IL", SourceName = "Bob's House of Security", TaskID = 12345},
				new CountyTask { CountyName = "Cook", StateCode = "IL", SourceName = "Bob's House of Security", TaskID = 12346},
				new CountyTask { CountyName = "Lake", StateCode = "IL", SourceName = "MRM Securitas, LLC", TaskID = 22345},
				new CountyTask { CountyName = "Lake", StateCode = "IL", SourceName = "MRM Securitas, LLC", TaskID = 22346},
				};
			_encoding = new ASCIIEncoding();

			_now = DateTime.Now;
			}

      /// <summary>
      ///Gets or sets the test context which provides
      ///information about and functionality for the current test run.
      ///</summary>
      public TestContext TestContext
         {
         get
            {
            return testContextInstance;
            }
         set
            {
            testContextInstance = value;
            }
         }

      #region Additional test attributes
      [ClassInitialize()]
      public static void MyClassInitialize(TestContext testContext)
         {

         }
      //
      //Use ClassCleanup to run code after all tests in a class have run
      //[ClassCleanup()]
      //public static void MyClassCleanup()
      //{
      //}
      //
      //Use TestInitialize to run code before running each test
      //[TestInitialize()]
      //public void MyTestInitialize()
      //{
      //}
      //
      //Use TestCleanup to run code after each test has run
      //[TestCleanup()]
      //public void MyTestCleanup()
      //{
      //}
      //
      #endregion

		private string IView_RenderViewDataSource()
			{
			MemoryStream stream = null;

			HTMLReportDocumentView<ReportGridControl> view = new HTMLReportDocumentView<ReportGridControl>();
			view.ReportHeading = string.Format("Run Date: {0:MM/dd/yyyy hh:mm:ss}", _now);
			view.Text = "DataView.Text::IView_Render";
			view.DataSource = _dataList;

			stream = (MemoryStream)view.Render();

			string text = null;
			ASCIIEncoding encoding = new ASCIIEncoding();
			text = encoding.GetString(stream.GetBuffer()).Trim(new char[] { ' ', '\0' });
			Trace.WriteLine(text);
			return text;
			}
      internal virtual IEmailClient CreateMockEmailClient()
         {
         IEmailClient target = EmailClientFactory.Create(serverMockAddress);
         return target;
         }

      internal virtual IEmailClient CreateMRSEmailClient()
         {
         IConfigSettings iconfig = new ApplicationConfiguration();
			Assert.IsNotNull(iconfig, "Failed to create SystemSettingsStore");

         serverMRSAddress = iconfig["MRSServer"];
			Assert.IsNotNull(serverMRSAddress);

         IEmailClient iclient = EmailClientFactory.Create(serverMRSAddress);
         Assert.IsNotNull(iclient);
         return iclient;
         }

      internal virtual IEmailClient CreateSmtpEmailClient()
         {
         IEmailClient target = EmailClientFactory.Create(serverSmtpAddress);
         return target;
         }

      internal virtual MailMessage CreateTextMailMessage(string subject)
         {
         MailMessage msg = new MailMessage();
         msg.To.Add("mmohrbacher@inceptconsulting.com");
         msg.From = new MailAddress("mrmohrbacher@comcast.net");
         msg.CC.Add("mrmohrbacher@gmail.com");
         msg.CC.Add("mike@blackriverinc.com");
         msg.Subject = subject;
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("!!!! ------------------------------------------------------ !!!!");
			sb.AppendLine("");
			sb.AppendLine("     Text Message  Text Message  Text Message  Text Message ");
			sb.AppendLine("");
			sb.AppendLine("!!!! ------------------------------------------------------ !!!!");
			msg.Body = sb.ToString();
         return msg;
         }

		internal virtual MailMessage CreateHtmlMailMessage(string subject)
			{
			MailMessage msg = new MailMessage();
			msg.To.Add("mmohrbacher@inceptconsulting.com");
			msg.From = new MailAddress("mrmohrbacher@comcast.net");
			msg.CC.Add("mrmohrbacher@gmail.com");
			msg.CC.Add("mike@blackriverinc.com");
			msg.Subject = "HTML - " + subject;
			msg.Body = IView_RenderViewDataSource();
			msg.IsBodyHtml = true;
			return msg;
			}

      internal virtual void SendText(IEmailClient client, string subject)
         {
         MailMessage msg = CreateTextMailMessage(subject);
         int msgCount = client.MsgCount;
         bool actual = client.Send(msg);
         Assert.AreEqual(true, actual);
         Assert.AreEqual(msgCount + 1, client.MsgCount);
         }


		internal virtual void SendHTML(IEmailClient client, string subject)
			{
			MailMessage msg = CreateHtmlMailMessage(subject);
			int msgCount = client.MsgCount;
			bool actual = client.Send(msg);
			Assert.AreEqual(true, actual);
			Assert.AreEqual(msgCount + 1, client.MsgCount);
			}

      public void SendAsync(IEmailClient client, string subject)
         {
         MailMessage msg = CreateTextMailMessage(subject);
         object token = null;
         object actual = null;
         bool done = false;
         client.SendCompleted += ((sender, acea) => { actual = acea.UserState;  done = true; });
         client.SendAsync(msg, out token);
         Assert.IsNotNull(token);
         DateTime start = DateTime.Now;
         TimeSpan timeOutInterval = TimeSpan.FromSeconds(15.0);
         while (!done)
            {
            System.Threading.Thread.Sleep(50);
            DateTime now = DateTime.Now;
            if (!(now - start < timeOutInterval))
               Assert.IsFalse(now - start < timeOutInterval);
            }
         Assert.AreEqual(token, actual);
         }

      /// <summary>
      ///A test for BeginSend
      ///</summary>
      [TestMethod()]
      public void Mock_SendAsync()
         {
         string subject = string.Format("Test Message via 'Mock' : {0}", DateTime.Now);
         IEmailClient target = CreateMockEmailClient();
         SendAsync(target, subject);
         }

      /// <summary>
      ///A test for Send
      ///</summary>
      [TestMethod()]
      public void Mock_Send()
         {
         string subject = string.Format("Test Message via 'Mock' : {0}", DateTime.Now);
         IEmailClient target = CreateMockEmailClient();
         SendText(target, subject);
         }

      /// <summary>
      ///A test for BeginSend
      ///</summary>
      [TestMethod()]
      public void Smtp_SendAsync()
         {
         string subject = string.Format("Test Message via 'SMTP' : {0}", DateTime.Now);
         IEmailClient target = CreateSmtpEmailClient();
         SendAsync(target, subject);
         }

		/// <summary>
		///A test for Send
		///</summary>
		[TestMethod()]
		public void Smtp_TextSend()
			{
			string subject = string.Format("Test Message via 'SMTP' : {0}", DateTime.Now);
			IEmailClient target = CreateSmtpEmailClient();
			SendText(target, subject);
			}
		/// <summary>
		///A test for Send
		///</summary>
		[TestMethod()]
		public void Smtp_HtmlSend()
			{
			string subject = string.Format("Test Message via 'SMTP' : {0}", DateTime.Now);
			IEmailClient target = CreateSmtpEmailClient();
			SendHTML(target, subject);
			}

      /// <summary>
      ///A test for BeginSend
      ///</summary>
      [TestMethod()]
      public void MRS_SendAsync()
         {
         string subject = string.Format("Test Message via 'MRS' : {0}", DateTime.Now);
         IEmailClient target = CreateMRSEmailClient();
         SendAsync(target, subject);
         }

      /// <summary>
      ///A test for Send
      ///</summary>
      [TestMethod()]
      public void MRS_Text_Send()
         {
         string subject = string.Format("Test Message via 'MRS' : {0}", DateTime.Now);
         IEmailClient target = CreateMRSEmailClient();
         SendText(target, subject);
			Assert.IsTrue(true);
         }

		[TestMethod()]
		public void MRS_HTML_Send()
			{
			string subject = string.Format("Test Message via 'MRS' : {0}", DateTime.Now);
			IEmailClient target = CreateMRSEmailClient();
			SendHTML(target, subject);
			Assert.IsTrue(true);
			}

      /// <summary>
      ///A test for Server
      ///</summary>
      [TestMethod()]
      public void Server()
         {
         IEmailClient target = CreateMockEmailClient();
         Uri expected = new Uri(serverMockAddress);
         Uri actual = target.Server;
         Assert.AreEqual(expected, actual);
         }
      }
}
