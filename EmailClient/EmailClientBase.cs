﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.EmailClient
	{
	public class EmailClientFactory
		{
		/// <summary>
		/// Static constructor is guarenteed to be called only once
		/// by CLI.
		/// </summary>
		static EmailClientFactory()
			{
			// Register Email URI schemes
			if (!UriParser.IsKnownScheme("mock"))
				UriParser.Register(new BasicURIParser(), "mock", 8000);
			if (!UriParser.IsKnownScheme("smtp"))
				UriParser.Register(new BasicURIParser(), "smtp", 25);
			}

		/// <summary>
		/// Parse address; call EmailClient factory depending on
		/// scheme.
		/// </summary>
		/// <param name="address">SCHEME:HOST:PORT</param>
		/// <returns>IEmailClient</returns>
		/// <remarks>
		/// Yes, I know, a fixed registry embedded in code is "gnarshish"
		/// </remarks>
		public static IEmailClient Create(string address)
			{
			bool secure = false;
			IEmailClient client = null;
			Uri serverURI = new Uri(address);
			switch (serverURI.Scheme)
				{
				case "smtp":
				case "":
					NameValueCollection queryParams = HttpUtility.ParseQueryString(serverURI.Query);
					if (queryParams.AllKeys.Contains("secure", StringComparer.CurrentCultureIgnoreCase))
						bool.TryParse(queryParams["secure"], out secure);
					client = new Smtp_EmailClient(serverURI, 
								queryParams["User"], queryParams["Password"], secure);
					break;

				case "mock":
					client = new Mock_EmailClient(serverURI);
					break;

				default:
					throw new InvalidOperationException(
										string.Format("Scheme '{0}' is not associated with any known Email Client.",
										serverURI.Scheme));
				}

			return client;
			}
		}

	public abstract class EmailClientBase : IEmailClient
		{
		/// <summary>
		/// Raised to notify any Observers of completion of the Send operation.
		/// </summary>
		public event SendCompletedEventHandler SendCompleted;

		protected delegate bool SendDelegate(MailMessage msg);
		protected struct AsyncResultObject
			{
			public SendDelegate Sender;
			public object Token;
			}

		private static object generateToken()
			{
			DateTime result = DateTime.UtcNow;
			return result;
			}

		#region "IEmailClient Interface"

		/// <summary>
		/// Composes and dispatches the Message to the Server
		/// </summary>
		/// <param name="msg">Formatted Email Message</param>
		/// <returns>true : operation successful; false: operation failed.</returns>
		public abstract bool Send(MailMessage msg);

		/// <summary>
		/// Composes and dispatches the Message to the Server
		/// </summary>
		/// <param name="from">Message originator</param>
		/// <param name="to">Message receipients in ";" delimited list</param>
		/// <param name="subject">Subject of message</param>
		/// <param name="body">Text of message</param>
		/// <returns>true : operation successful; false: operation failed.</returns>
		public bool Send(string from, string to, string subject, string body)
			{
			bool result = false;
			try
				{
#if _TRACE
				Trace.WriteLine(string.Format("IEmailClient::Send from='{0}', 'to='{1}', subject='{2}'",
														from, to, subject));
#endif
				MailMessage msg = new MailMessage(from, to, subject, body);
				msg.IsBodyHtml = Regex.IsMatch(msg.Body, @"\<html", RegexOptions.IgnoreCase);
				result = Send(msg);
				}
			catch (FormatException fe)
				{
				Trace.WriteLine("====================================================");
				Trace.WriteLine(fe.Message);
				Trace.WriteLine(string.Format("IEmailClient::Send from='{0}', 'to='{1}', subject='{2}'",
														from, to, subject));
				Trace.WriteLine("====================================================");
				}
			return result;
			}

		/// <summary>
		/// Initiate an asynchronous Send of the Message.
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="token">Returned to caller to identify this invocation
		/// of the call. Will be returned in the SendCompleted Event.</param>
		public virtual void SendAsync(System.Net.Mail.MailMessage msg, out object token)
			{
			// Create the delegate to invoke the async operation 
			// and generate the operation token to return the caller.
			AsyncResultObject aro = new AsyncResultObject
			{
				Sender = (m => Send(m)),
				Token = generateToken()
			};

			// Return token to caller; toekn is unique per message and can be 
			// used to identify an event retruning from a specific BeginSend call.
			token = aro.Token;

			// Invoke the Send Method; callback will return 'token'
			aro.Sender.BeginInvoke(msg, new AsyncCallback(OnSendCompleted), aro);
			}

		public void SendAsync(string from, string to, string subject, string body, out object token)
			{
			MailMessage msg = new MailMessage(from, to, subject, body);
			SendAsync(msg, out  token);
			}

		Uri _server;
		public Uri Server
			{
			get
				{
				Uri result = null;
				Interlocked.Exchange<Uri>(ref result, _server);
				return result;
				}
			protected set
				{
				Interlocked.Exchange<Uri>(ref _server, value);
				}
			}

		public int MsgCount
		{ get; protected set; }


		public abstract void Dispose();

		protected void OnSendCompleted(IAsyncResult iar)
			{
			try
				{
				// Retrieve the callback state object.
				AsyncResultObject aro = (AsyncResultObject)iar.AsyncState;
				object token = aro.Token;

				// Complete the asynchronous call
				bool result = aro.Sender.EndInvoke(iar);

				// Notify any Observers of completion of the operation
				if (SendCompleted != null)
					{
					SendCompleted(this, new AsyncCompletedEventArgs(null, false, token));
					}
				}
			catch (Exception exp)
				{
				Trace.WriteLine(exp.ToString());
				}
			}


		#endregion

		protected virtual string compose(MailMessage msg)
			{
			return msg.Body;
			}

		}

	}
