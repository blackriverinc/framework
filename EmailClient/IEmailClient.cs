﻿using System;
using System.Diagnostics;
using System.Net.Mail;


namespace Blackriverinc.Framework.EmailClient
   {
   public interface IEmailClient : IDisposable
      {
      /// <summary>
      /// Raised to notify any Observers of completion of the Send operation.
      /// </summary>
      event SendCompletedEventHandler SendCompleted;

      /// <summary>
      /// Send MailMessage; wait for completion.
      /// </summary>
      bool Send(MailMessage msg);

      /// <summary>
      /// Construct MailMessage; send MailMessage; wait for completion.
      /// </summary>
      bool Send(string from, string to, string subject, string body);

      /// <summary>
      /// Initiate sending MailMessage; returns [token];  signals on [SendCompleted]
      /// when finished. [SendCompletedEvent] emits [AsyncCompletedEventArgs] when
      /// send completes. [AsyncCompletedEventArgs] contains [token] returned by 
      /// initial call to [SendAsync]. Event will not be triggered on the same thread
      /// as the initial call.
      /// </summary>
      void SendAsync(MailMessage msg, out object token);

      /// <summary>
      /// Compose MailMessage using [from], [to], [subject], [body] input
      /// paarameters. Initiate sending MailMessage; returns [token];  
      /// signals on [SendCompleted] when finished. 
      /// [SendCompletedEvent] emits [AsyncCompletedEventArgs] when
      /// send completes. [AsyncCompletedEventArgs] contains [token] returned by 
      /// initial call to [SendAsync] so caller can tie event to initial call.
      /// </summary>
      void SendAsync(string from, string to, string subject, string body, out object token);

      Uri Server { get; }

      int MsgCount { get; }

		new void Dispose();
      }
   }
