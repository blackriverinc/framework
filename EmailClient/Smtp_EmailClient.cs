﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;

namespace Blackriverinc.Framework.EmailClient
   {
   public class Smtp_EmailClient : EmailClientBase
      {

      SmtpClient _client = null;

      /// <summary>
      /// Restricted...nay! forbidden access to naked constructor!
      /// </summary>
      private Smtp_EmailClient()
         {
         }

      internal Smtp_EmailClient(Uri server, string userName = null, string passWord = null, bool secure = false)
         : this()
         {
         Server = server;
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

         _client = new SmtpClient(server.Host, server.Port);
			if (_client == null)
				throw new ApplicationException(string.Format("Could open connection to server '{0}'",
													server));
			_client.DeliveryMethod = SmtpDeliveryMethod.Network;
			{
			};
			if (userName != null)
				{
				_client.Credentials = new NetworkCredential(userName, passWord);
				}
			_client.EnableSsl = secure;
			
         Trace.WriteLine(string.Format("Create 'SMTP_EmailClient' [{0}:{1}] {2}", Server.Host, Server.Port, (string)(secure?"SECURE":"")));
         }

      #region IEmailClient Members

      public override bool Send(System.Net.Mail.MailMessage msg)
         {
			try
				{
				lock (this)
					{
					if (_client == null)
						throw new ObjectDisposedException("Smtp_EmailClient");

					_client.Send(msg);

					MsgCount = MsgCount + 1;
					Trace.WriteLineIf(false, base.compose(msg));
					}
				return true;
				}
			catch (Exception exp)
				{
				Trace.WriteLine(string.Format("FAIL: {0}", exp.ToString()));
				}
			return false;
         }


		public override void Dispose()
			{
			lock (this)
				{
				_client.Dispose();
				_client = null;
				}
			}

      #endregion
      }
   }
