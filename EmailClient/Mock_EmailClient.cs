﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Xml;

namespace Blackriverinc.Framework.EmailClient
   {

   public class Mock_EmailClient : EmailClientBase
      {
      protected override string compose(MailMessage msg)
         {
         // MemoryStream holds the buffer and a means to access it (the Stream part)
         MemoryStream stream = new MemoryStream();

         // Writer "guides" the pen that writes into the Stream
         XmlTextWriter writer = new XmlTextWriter(stream, ASCIIEncoding.ASCII);

         writer.Indentation = 3;
         writer.IndentChar = ' ';

         writer.WriteStartDocument();
         writer.WriteStartElement("MailMessage");

         writer.WriteStartElement("MailAddress");
         writer.WriteAttributeString("From", msg.From.Address);
         writer.WriteEndElement();

         foreach (MailAddress to in msg.To)
            {
            writer.WriteStartElement("MailAddress");
            writer.WriteAttributeString("To", to.Address);
            writer.WriteEndElement();
            }

         foreach (MailAddress cc in msg.CC)
            {
            writer.WriteStartElement("MailAddress");
            writer.WriteAttributeString("CC", cc.Address);
            writer.WriteEndElement();
            }

         writer.WriteElementString("Subject", msg.Subject);
         writer.WriteStartElement("Body");
			if (msg.IsBodyHtml)
				writer.WriteRaw(msg.Body);
			else
				writer.WriteString(msg.Body);
         writer.WriteEndElement();

         writer.WriteEndElement();
         writer.WriteEndDocument();

         writer.Flush();
         // Encoding interprets an array of bytes into a String
         ASCIIEncoding encoding = new ASCIIEncoding();
         return encoding.GetString(stream.GetBuffer());
         }
      
      /// <summary>
      /// Restricted...nay! forbidden access to naked constructor!
      /// </summary>
      private Mock_EmailClient()
         {
         
         }

      #region IEmailClient Members

      public override bool Send(System.Net.Mail.MailMessage msg)
         {
			lock (this)
				{
				string composition = compose(msg);
				MsgCount += 1;
				Debug.WriteLine("============ Start Email =================");
				Debug.WriteLine(composition);
				Debug.WriteLine("============   End Email =================");
            return true;
				}
         }

		public override void Dispose()
			{
			lock (this)
				{
				}
			}

      #endregion

      internal Mock_EmailClient(Uri server)
         : base()
         {
         Server = server;

         }

      }
   }


