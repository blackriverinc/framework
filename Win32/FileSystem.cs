﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.Common
   {
   public static class FileSystem
      {
      /// <summary>
      /// Move all files from the location specified by the Source to the location
      /// specified by the Destination.
      /// </summary>
      public static int MoveFiles(string sourceFilePathSpec, string destinationPath, bool overwrite = false)
         {
         int fileCount = 0;
         string sourceDirectory = Path.GetDirectoryName(sourceFilePathSpec);
         string sourceFileSpec = Path.GetFileName(sourceFilePathSpec);

         if (!Directory.Exists(destinationPath))
            Directory.CreateDirectory(destinationPath);

         foreach (string sourceFilePath in Directory.EnumerateFiles(sourceDirectory, sourceFileSpec))
            {
            string fileName = Path.GetFileName(sourceFilePath);
            string destinationFilePath = Path.Combine(destinationPath, fileName);

            if (File.Exists(destinationFilePath) && overwrite)
               File.Delete(destinationFilePath);

            File.Move(sourceFilePath, destinationFilePath);

            Trace.WriteLine("INFO: Move '{0}' => {1}".Bind(sourceFilePath, destinationFilePath));

            fileCount++;
            }

         return fileCount;
         }

      /// <summary>
      /// Move all files from the location specified by the Source to the location
      /// specified by the Destination.
      /// </summary>
      public static int CopyFiles(string sourceFilePathSpec, string destinationPath)
         {
         int fileCount = 0;
         string sourceDirectory = Path.GetDirectoryName(sourceFilePathSpec);
         string sourceFileSpec = Path.GetFileName(sourceFilePathSpec);

         if (!Directory.Exists(destinationPath))
            Directory.CreateDirectory(destinationPath);

         foreach (string sourceFilePath in Directory.EnumerateFiles(sourceDirectory, sourceFileSpec))
            {
            string fileName = Path.GetFileName(sourceFilePath);
            string destinationFilePath = Path.Combine(destinationPath, fileName);

            File.Copy(sourceFilePath, destinationFilePath);

            Trace.WriteLine("INFO: Copy '{0}' => {1}".Bind(sourceFilePath, destinationFilePath));

            fileCount++;
            }

         return fileCount;
         }
      }
   }
