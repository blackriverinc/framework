﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Win32.SafeHandles;
using Microsoft.Win32;

namespace Blackriverinc.Framework.Common.Win32.Kernel
   {

   public static class FileSystem
      {
      [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
      static extern SafeFileHandle CreateFile(
         string lpFileName,
         [MarshalAs(UnmanagedType.U4)] FileAccess dwDesiredAccess,
         [MarshalAs(UnmanagedType.U4)] FileShare dwShareMode,
         IntPtr lpSecurityAttributes,
         [MarshalAs(UnmanagedType.U4)] FileMode dwCreationDisposition,
         [MarshalAs(UnmanagedType.U4)] FileAttributes dwFlagsAndAttributes,
         IntPtr hTemplateFile);

      public static FileStream CreateFileStream(string filePath, params object[] args)
         {
         // Parse File Access parameters out of argument list.
         Func<object[], Type, object> argParser =
            ((al, type) =>
            {
               if (al != null && al.Length > 0)
                  return (from fm in al where fm.GetType() == type select fm).FirstOrDefault();
               return null;
            });

         
         FileMode fileMode = (FileMode)(argParser(args, typeof(FileMode)) ?? FileMode.Open);
         FileAccess fileAccess = (FileAccess)(argParser(args, typeof(FileAccess)) ?? FileAccess.Read);
         FileShare fileShare = (FileShare)(argParser(args, typeof(FileShare)) ?? FileShare.Read);

         SafeFileHandle handle = CreateFile(filePath, fileAccess, fileShare, IntPtr.Zero, fileMode, 0, IntPtr.Zero);

         return new FileStream(handle, fileAccess);
         }

      }


   }
