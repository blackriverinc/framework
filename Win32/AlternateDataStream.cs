﻿using System.IO;

using Win32Kernel = Blackriverinc.Framework.Common.Win32.Kernel;

namespace Blackriverinc.Framework.Common
   {
   public class AlternateDataStream
      {
      public static FileStream Create(string fileName, string streamName, params object[] args)
         {
         var altStream = Win32Kernel.FileSystem.CreateFileStream(
                                                      fileName 
                                                         + (string)(!string.IsNullOrEmpty(streamName)?":":"") 
                                                         + streamName,
                                                      args);
         return altStream;
         }

      }
   }
