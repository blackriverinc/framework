﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.DataStore
   {
   public static class GlobalCache 
      {
      static IKeyedDataStore _cache;

      static ReaderWriterLockSlim _cacheLock;

      static Regex _parser;

		static TextTemplate _template;
      static GlobalCache()
         {
         _cacheLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

			IDataStoreProvider provider = new NullProvider();
         _cache = new KeyedDataStore(provider);

			_template = new TextTemplate(_cache);

         _parser = new Regex(@"\{(?<key>\w+(?:\.\w+)?)}", 
                    RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
         }

      public static int CacheSize
         {
         get 
            {
            _cacheLock.EnterReadLock();
            try
               {
               return _cache.Count;
               }
            finally
               {
               _cacheLock.ExitReadLock();
               }
            }
         }

      public static void LoadConfigurationSettings(IDataStoreProvider provider, bool loadDatabaseConfig = false)
         {
         if (provider != null)
            {
            try
               {
               IKeyedDataStore store = GlobalCache.LockData;
               provider.Load(store);

					if (loadDatabaseConfig)
						{
						string path = "databaseConfigurationSection";
						var databaseSection = ((ConfigProviderBase)provider).GetSection(path)
								as DatabaseConfigurationSection;

						if (!store.ContainsKey(path))
							databaseSection.Configurations.Load(store, "DatabaseConfigurations");
						}
					//_template = new TextTemplate(_cache);
					}
            finally
               {
               GlobalCache.ReleaseData();
               }
            }

         Assembly assembly = Assembly.GetExecutingAssembly();

         // {Version}
         GlobalCache.Update("Version", assembly.GetName().Version.ToString());

			// {Environment}
			string environment = GlobalCache.Get("Environment") as string;
			//if (string.IsNullOrWhiteSpace(environment))
			//	{
			//	var configAttr = assembly.GetCustomAttribute<AssemblyConfigurationAttribute>();
			//	GlobalCache.Update("Environment", configAttr.Configuration);
			//	}

			//// {BuildSource}
			//var metaData = assembly.GetCustomAttribute<AssemblyMetadataAttribute>();
			//if(metaData != null)
			//	GlobalCache.Update(metaData.Key, metaData.Value);
         }

      /// <summary>
      /// Loads GlobalCache with application settings and DatabaseConfigurations
      /// from web.config
      /// </summary>
      /// <param name="appStart"></param>
      public static void LoadAppConfigurationSettings(bool loadDatabaseConfig = false)
         {
         IDataStoreProvider provider = new AppConfigProvider();
         LoadConfigurationSettings(provider, loadDatabaseConfig);
         return;
         }


      /// <summary>
      /// Loads GlobalCache with application settings and DatabaseConfigurations
      /// from app.config
      /// </summary>
      /// <param name="appStart"></param>
      public static void LoadWebConfigurationSettings(bool loadDatabaseConfig = false)
         {
         IDataStoreProvider provider = WebConfigProvider.Open();
         LoadConfigurationSettings(provider, loadDatabaseConfig);
         return;
         }


      /// <summary>
      /// Used when a client needs to make a mass-update to the cache.
      /// Must always be followed by a ReleaseData call.
      /// </summary>
      public static IKeyedDataStore LockData
         {
         get 
            {
            Debug.WriteLine("+ WriteLock +");
            _cacheLock.EnterWriteLock();
            return _cache;
            }
         }

      public static IKeyedDataStore ReadLockData
         {
         get
            {
            Debug.WriteLine("+ ReadLock +");
            _cacheLock.EnterReadLock();
            return _cache;
            }
         }

      public static void ReleaseData()
         {
         if (_cacheLock.IsWriteLockHeld)
            {
            _cacheLock.ExitWriteLock();
            Debug.WriteLine("- WriteLock -");
            }
         else if (_cacheLock.IsReadLockHeld)
            {
            _cacheLock.ExitReadLock();
            Debug.WriteLine("- ReadLock -");
            }
         }

      public static object Get(string key)
         {
         object value = null;

         _cacheLock.EnterReadLock();
         try
            {
            value = _cache[key];
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         return value;
         }

		/// <summary>
		/// Attempt to retrieve a value from the cache, and then recursively
		/// resolve the embedded tokens.
		/// </summary>
		/// <param name="key">Key into cache</param>
		/// <returns>resolved value, or null, if not present.</returns>
      public static string GetResolvedString(string key)
         {
         string value = null;

         _cacheLock.EnterReadLock();
         try
            {
				value = ResolveString(_cache[key] as string);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         return value;
         }


      public static string ResolveString(string value)
         {
         if (value != null)
            {
				value = _template.ResolveToString(value);
            }
         return value;
         }

      public static object Update(string key, object value)
         {
         _cacheLock.EnterUpgradeableReadLock();
         try
            {
            object result = null;
            if (_cache.TryGetValue(key, out result))
               {
               if (result == value)
                  {
                  return value;
                  }
               else
                  {
                  _cacheLock.EnterWriteLock();
                  try
                     {
                     _cache[key] = value;
                     }
                  finally
                     {
                     _cacheLock.ExitWriteLock();
                     }
                  return value;
                  }
               }
            else
               {
               _cacheLock.EnterWriteLock();
               try
                  {
                  _cache.Add(key, value);
                  }
               finally
                  {
                  _cacheLock.ExitWriteLock();
                  }
               return value;
               }
            }
         finally
            {
				//_template = new TextTemplate(_cache);
				_cacheLock.ExitUpgradeableReadLock();
            }
         }

		#region Get overloads
		public static bool Get(string key, ref bool value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref int value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref double value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref string value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref DateTime value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref Nullable<int> value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref Nullable<bool> value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref Nullable<double> value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
         }
      public static bool Get(string key, ref Nullable<DateTime> value)
         {
         _cacheLock.EnterReadLock();
         try
            {
            return _cache.get(key, ref value);
            }
         finally
            {
            _cacheLock.ExitReadLock();
            }
			}
		#endregion
		}
   }
