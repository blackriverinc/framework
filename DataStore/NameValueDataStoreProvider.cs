﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Blackriverinc.Framework.DataStore
   {
   public class NameValueDataStoreProvider : IDataStoreProvider
      {
      readonly NameValueCollection _collection = null;

      public NameValueDataStoreProvider(NameValueCollection collection, bool own = false)
         {
         if (own)
            _collection = new NameValueCollection(collection);
         else
            _collection = collection;
         }

      public void Load(IKeyedDataStore store, string path = null)
         {
         foreach (string key in _collection.AllKeys)
            {
            store.Add(key, _collection[key]);
            }
         }

      public void Save(IKeyedDataStore store)
         {
         throw new NotImplementedException();
         }

      public void Dispose()
         {
         return;
         }
      }
   }
