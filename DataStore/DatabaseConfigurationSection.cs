﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.DataStore
   {
   public class DatabaseConfigurationSection : ConfigurationSection
      {
      public DatabaseConfigurationSection()
         {
         Trace.WriteLine(this.ToString());
         }

      public static DatabaseConfigurationSection Get()
         {
         return (DatabaseConfigurationSection)ConfigurationManager.GetSection("databaseConfigurationSection");
         }

      [ConfigurationProperty("databaseConfigurations", IsDefaultCollection = true)]
      public DatabaseConfigurationCollection Configurations
         {
         get
            {
            return (DatabaseConfigurationCollection)base["databaseConfigurations"];
            }
         }
   }

   [ConfigurationCollection(typeof(Environment), AddItemName = "environment")]
   public class DatabaseConfigurationCollection : 
      ConfigurationElementCollection,
      IDataStoreProvider
      {
      protected override ConfigurationElement CreateNewElement()
         {
         return new Environment();
         }

      protected override object GetElementKey(ConfigurationElement element)
         {
         var l_configElement = element as Environment;
         if (l_configElement != null)
            return l_configElement.Name;
         else
            return null;
         }
      
      public Environment this[int index]
         {
         get
            {
            return BaseGet(index) as Environment;
            }
         }
      
      public new Environment this[string environment]
         {
         get
            {
            return BaseGet(environment) as Environment;
            }
         }


      public void Load(IKeyedDataStore store, string path = null)
         {

         // Add the sub-root element
         if (!store.ContainsKey(path))
            store.Add(path, new Dictionary<string, Environment>());

         IDictionary<string, Environment> connections 
               = store[path] as Dictionary<string, Environment>;
         foreach (Environment connection in this)
            {
            if (!connections.ContainsKey(connection.Name))
               connections.Add(connection.Name, connection);
            connections[connection.Name] = connection;
            Debug.WriteLine(connection.ToString());
            }
         }

      public void Save(IKeyedDataStore store)
         {
         throw new NotImplementedException();
         }

      public void Dispose()
         {
         ;
         }

		public void Load(IKeyedDataStore store)
			{
			throw new NotImplementedException();
			}
		}

   public class Environment : ConfigurationElement
   {
      [ConfigurationProperty("name", IsRequired = true, IsKey=true)]
      public string Name
         {
         get { return (string)this["name"]; }
         set { this["name"] = value; }
         }

      [ConfigurationProperty("server", IsRequired = true)]
      public string Server
         {
         get { return (string)this["server"]; }
         set { this["server"] = value; }
         }

      [ConfigurationProperty("catalog")]
      public string Catalog
         {
         get { return (string)this["catalog"]; }
         set { this["catalog"] = value; }
         }

      [ConfigurationProperty("integratedAuth", DefaultValue = "true", IsRequired = false)]
      public bool IntegratedAuth
         {
         get { return (bool)this["integratedAuth"]; }
         set { this["integratedAuth"] = value; } 
         }

      [ConfigurationProperty("uid", IsRequired = false)]
      public string UID
         {
         get { return (string)this["uid"]; }
         set { this["uid"] = value; }
         }

      [ConfigurationProperty("password", IsRequired = false)]
      public string Password
         {
         get { return (string)this["password"]; }
         set { this["password"] = value; }
         }

      public override string ToString()
         {
         return this.ToObjectString();
         }
      }
   }
