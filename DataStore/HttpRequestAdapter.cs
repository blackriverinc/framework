﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace Blackriverinc.Framework.DataStore
   {
   public class HttpRequestDataStoreProvider : DataStoreProvider, IDisposable
      {
      NameValueCollection _collection = null;

		private void initialize(HttpRequestBase request)
			{
			if (request.HttpMethod == "POST")
				{
				_collection = new NameValueCollection(request.Form);
				}
			else
				{
				_collection = new NameValueCollection(request.QueryString);
				}
			}

      public HttpRequestDataStoreProvider(HttpRequest request, IDataStoreProvider interiorProvider = null) :
			base(interiorProvider)
         {
			initialize(request.RequestContext.HttpContext.Request);
         }

      public HttpRequestDataStoreProvider(HttpRequestBase request, IDataStoreProvider interiorProvider = null) :
			base(interiorProvider)
         {
			initialize(request);			
			}

      public override void Load(IKeyedDataStore store, string path = null)
         {
			base.Load(store);

         foreach (string key in _collection.AllKeys)
            {
            store.Add(key, _collection[key]);
            }
         }

      public override void Save(IKeyedDataStore store)
         {
         throw new NotImplementedException();
         }

      public override void Dispose()
         {
         return;
         }
      }
   }
