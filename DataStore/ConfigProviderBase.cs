﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Web.Configuration;

namespace Blackriverinc.Framework.DataStore
	{
	public class ConfigProviderBase : DataStoreProvider
		{
		bool _trace = false;

		protected System.Configuration.Configuration _configuration = null;

		public ConfigProviderBase(IDataStoreProvider interiorProvider = null)
			: base(interiorProvider)
			{
			}

		public System.Configuration.Configuration Configuration { get { return _configuration; } }

		public override void Load(IKeyedDataStore store, string path = null)
			{
			try
				{
				Trace.WriteLine("*** Load Configuration ***");
				base.Load(store);

				foreach (string key in _configuration.AppSettings.Settings.AllKeys)
					{
					store[key] = _configuration.AppSettings.Settings[key].Value;
					Trace.WriteLineIf(_trace, string.Format("   Configuration[{0}] = '{1}'", key, store[key]));
					}
				}
			catch (Exception ex)
				{
				Trace.TraceError(ex.ToString());
				throw;
				}
			}

		public override void Save(IKeyedDataStore store)
			{
			KeyValueConfigurationCollection appSettings = _configuration.AppSettings.Settings;
			appSettings.Clear();
			foreach (string key in store.Keys)
				{
				appSettings.Add(key, store[key].ToString());
				}
			}


		static public IDataStoreProvider Open()
			{
			IDataStoreProvider adapter = null;
			try
				{
				string configPath = AppDomain.CurrentDomain.GetData("APP_CONFIG_FILE").ToString();
				if (string.IsNullOrEmpty(configPath))
					{
					adapter = new NullProvider();
					}
				else 
					{
					string module = Path.GetFileNameWithoutExtension(configPath) ?? string.Empty;
					if ((module).Equals("web", StringComparison.OrdinalIgnoreCase))
						adapter = WebConfigProvider.Open();
					else
						adapter = AppConfigProvider.Open(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, module));
					}
				}
			catch (Exception exp)
				{
				Trace.WriteLine(exp.ToString());
				}
			return adapter;
			}

		public ConfigurationSection GetSection(string path)
			{
			ConfigurationSection section = null;
			try
				{
				section =
					  _configuration.GetSection(path);

				if (section == null)
					throw new ApplicationException(
						String.Format("Could not find section '{0}' in the configuration file.",
									  path));
#if _LATER
            System.Xml.XmlNode _node = null;
            Dictionary<string, object> _settings = store[path] as Dictionary<string, object>;

            if (_settings == null)
               _settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);


            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.LoadXml(section.SectionInformation.GetRawXml());

            System.Xml.XmlNode xList = xmlDoc.ChildNodes[0];
            foreach (System.Xml.XmlNode node in xList)
               {
               _node = node;
               if (node.NodeType == System.Xml.XmlNodeType.Element
               && node.Attributes.Count > 1)
                  {
                  if (!((Dictionary<string, object>)_settings).ContainsKey(node.Attributes[0].Value))
                     _settings.Add(node.Attributes[0].Value, node.Attributes[1].Value);
                  else
                     Trace.WriteLine(node.ToString());
                  }
               }
#else

#endif
				}
			catch (Exception exp)
				{
				Trace.TraceError(exp.ToString());
				throw;
				}

			return section;
			}
		}
	}
