﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.EntityClient;
using System.Data.SqlClient;

namespace Blackriverinc.Framework.DataStore
   {
   public class ConnectionFactory
      {

      /// <summary>
      /// Return an EntityFramework connection string.
      /// </summary>
      public static string GetConnectionString()
         {
         var builder = new EntityConnectionStringBuilder();
         builder.Metadata = "res://*/Library.Model.csdl|res://*/Library.Model.ssdl|res://*/Library.Model.msl";
         builder.Provider = "System.Data.EntityClient";
         builder.ProviderConnectionString = GetDbConnectionString();

         return builder.ToString();
         }
		
      /// <summary>
      /// Return a SQLClient connection string.
      /// </summary>
      /// <returns></returns>
      public static string GetDbConnectionString()
         {
         var connections = GlobalCache.Get("DatabaseConfigurations") as Dictionary<string, Environment>;
	      string environment = GlobalCache.Get("Environment") as string;

         var connection = connections[environment];


         var scsb = new SqlConnectionStringBuilder();
         scsb.DataSource = GlobalCache.ResolveString(connection.Server)??".";
         scsb.InitialCatalog = GlobalCache.ResolveString(connection.Catalog);
         scsb.IntegratedSecurity = connection.IntegratedAuth;
         if (!scsb.IntegratedSecurity)
            {
            scsb.UserID = GlobalCache.ResolveString(connection.UID);
            scsb.Password = GlobalCache.ResolveString(connection.Password);
            }
         scsb.MultipleActiveResultSets = true;
         return scsb.ConnectionString;
         }

      /// <summary>
      /// Return an EntityFramework connection.
      /// </summary>
      /// <returns>EntityConnection for the current context.</returns>
      public static EntityConnection GetConnection()
         {
         string connectionString = GetConnectionString();
         return new EntityConnection(connectionString);
         }
      }
   }
