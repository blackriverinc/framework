﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Blackriverinc.Framework.DataStore
   {
	public interface IKeyedDataStore :
		IEnumerable<KeyValuePair<string, object>>,
		IDictionary<string, object>
		{
		new object this[string key] { get; set; }

		#region Type-conversion Getters
		/// <summary>
		/// Return value from configuration source or default value
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns>true : value retrieved; false : value unchanged</returns>
		bool get(string key, ref bool value);
		bool get(string key, ref int value);
		bool get(string key, ref double value);
		bool get(string key, ref string value);
		bool get(string key, ref DateTime value);
		bool get(string key, ref Nullable<int> value);
		bool get(string key, ref Nullable<bool> value);
		bool get(string key, ref Nullable<double> value);
		bool get(string key, ref Nullable<DateTime> value);

		#endregion

		}

	/// <summary>
	/// Basically, a Dictionary<string, object> with the ConfigSettings
	/// converting accessors added.
	/// </summary>
	public class KeyedDataStore : IKeyedDataStore
		{
		protected IDictionary<string, object> _dictionary;

		public object this[string key]
			{
			get
				{
				return ((IDictionary<string, object>)this)[key];
				}
			set
				{
				((IDictionary<string, object>)this)[key] = value;
				}
			}

		protected void initialize(IEnumerable<KeyValuePair<string, object>> rows)
			{
			try
				{
				//------------------------------------------------------------------
				// Create the settings cache on the first call into this method.
				//------------------------------------------------------------------
				_dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
				}
			catch (Exception ex)
				{
				Trace.TraceError(ex.ToString());
				throw;
				}

			if (rows != null)
				foreach (KeyValuePair<string, object> key_value in rows)
					((IDictionary<string, object>)this)[key_value.Key] = key_value.Value;
			}

		public KeyedDataStore(IEnumerable<KeyValuePair<string, object>> ienum)
			{
			initialize(ienum);
			}

		public KeyedDataStore(IDataStoreProvider provider)
			{
			initialize(null);
			provider.Load(this);
			}

		public KeyedDataStore(System.Collections.Specialized.NameValueCollection nameValueCollection)
			{
			initialize(null);
			foreach (var key in nameValueCollection.AllKeys)
				{
				((IDictionary<string, object>)this)[key] = nameValueCollection[key];
				}
			}


		#region Type-conversion gets
		/// <summary>
		/// Type-converting version of 'get'. Retrieve string-value
		/// from store. Attempt to convert to type of [value]. Return [false]
		/// if conversion fails or key not present in store. On fail, do not 
		/// modify the [value] parameter.
		/// </summary>
		public bool get(string key, ref bool value)
			{
			string strValue = this[key] as string;
			if (strValue == null)
				return false;
			int intValue = 0;
			if (int.TryParse(strValue, out intValue))
				value = (intValue > 0);
			if (!bool.TryParse(strValue, out value))
				// We are here because we have a non-standard token in the value 
				value = (strValue.ToLower() == "yes" || strValue.ToLower() == "y" || strValue.ToLower() == "t");
			return true;
			}


		public bool get(string key, ref Nullable<bool> value)
			{
			// Fetch raw string
			string strValue = this[key] as string;
			if (strValue == null)
				return false;

			bool result = false;
			// Attempt parsing as integer (0 : false; >0 : true)
			int intValue = 0;
			if (int.TryParse(strValue, out intValue))
				{
				result = (intValue > 0);
				}
			// Attempt parse as a standard {'True','False'} value.
			else if (!bool.TryParse(strValue, out result))
				// We are here because we have a non-standard token in the value 
				result = (strValue.ToLower() == "yes" || strValue.ToLower() == "y" || strValue.ToLower() == "t");

			// Regardless of results of parsing; we did find the key in the store.
			value = result;
			return true;
			}


		public bool get(string key, ref int value)
			{
			if (!this.ContainsKey(key))
				return false;
			string strValue = this[key].ToString();
			if (strValue == null)
				return false;
			return int.TryParse(strValue, out value);
			}


		public bool get(string key, ref double value)
			{
			if (!this.ContainsKey(key))
				return false;
			string strValue = this[key].ToString();
			if (strValue == null)
				return false;
			return double.TryParse(strValue, out value);
			}

		public bool get(string key, ref string value)
			{
			string strValue = null;
			if (this.ContainsKey(key))
				{
				strValue = this[key].ToString();
				if (strValue != null)
					value = strValue;
				}
			return (strValue != null);
			}

		public bool get(string key, ref DateTime value)
			{
			if (!this.ContainsKey(key))
				return false;
			string strValue = this[key].ToString();
			if (strValue == null)
				return false;
			return DateTime.TryParse(strValue, out value);
			}


		public bool get(string key, ref Nullable<int> value)
			{
			if (!this.ContainsKey(key))
				return false;
			string strValue = this[key].ToString();
			if (strValue == null)
				return false;

			int result;
			if (int.TryParse(strValue, out result))
				{
				value = result;
				return true;
				}
			return false;
			}


		public bool get(string key, ref Nullable<double> value)
			{
			if (!this.ContainsKey(key))
				return false;
			string strValue = this[key].ToString();
			if (strValue == null)
				return false;

			double result;
			if (double.TryParse(strValue, out result))
				{
				value = result;
				return true;
				}
			return false;
			}


		public bool get(string key, ref Nullable<DateTime> value)
			{
			if (!this.ContainsKey(key))
				return false;
			string strValue = this[key].ToString();
			if (strValue == null)
				return false;

			DateTime result;
			if (DateTime.TryParse(strValue, out result))
				{
				value = result;
				return true;
				}
			return false;
			}

		#endregion

		#region IEnumerable<KeyValuePair<string,string>> Members

		IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
			{
			return (IEnumerator<KeyValuePair<string, object>>)_dictionary.GetEnumerator();
			}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
			return _dictionary.GetEnumerator();
			}

		#endregion

		#region IDictionary<string,string> Members

		object IDictionary<string, object>.this[string key]
			{
			get
				{
				object result = null;
				try
					{
					lock (_dictionary)
						{
						if (_dictionary.ContainsKey(key))
							{
							result = _dictionary[key];
							}
						}
					}
				catch (Exception ex)
					{
					Trace.TraceError(ex.ToString());
					throw;
					}

				return result;
				}
			set
				{
				try
					{
					lock (_dictionary)
						{
						if (!_dictionary.ContainsKey(key))
							_dictionary.Add(key, value);
						else
							_dictionary[key] = value;
						}
					}
				catch (Exception ex)
					{
					Trace.TraceError(ex.ToString());
					throw;
					}

				}
			}

		void IDictionary<string, object>.Add(string key, object value)
			{
			_dictionary.Add(key, value);
			}

		public bool ContainsKey(string key)
			{
			return _dictionary.ContainsKey(key);
			}

		ICollection<string> IDictionary<string, object>.Keys
			{
			get { return _dictionary.Keys; }
			}

		bool IDictionary<string, object>.Remove(string key)
			{
			return _dictionary.Remove(key);
			}

		bool IDictionary<string, object>.TryGetValue(string key, out object value)
			{
			return _dictionary.TryGetValue(key, out value);
			}

		ICollection<object> IDictionary<string, object>.Values
			{
			get { return _dictionary.Values; }
			}

		#endregion

		#region ICollection<KeyValuePair<string,string>> Members

		void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
			{
			((ICollection<KeyValuePair<string, object>>)_dictionary).Add(item);
			}

		void ICollection<KeyValuePair<string, object>>.Clear()
			{
			((ICollection<KeyValuePair<string, object>>)_dictionary).Clear();
			}

		bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item)
			{
			return ((ICollection<KeyValuePair<string, object>>)_dictionary).Contains(item);
			}

		void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
			{
			((ICollection<KeyValuePair<string, object>>)_dictionary).CopyTo(array, arrayIndex);
			}

		int ICollection<KeyValuePair<string, object>>.Count
			{
			get { return ((ICollection<KeyValuePair<string, object>>)_dictionary).Count; }
			}

		bool ICollection<KeyValuePair<string, object>>.IsReadOnly
			{
			get { return ((ICollection<KeyValuePair<string, object>>)_dictionary).IsReadOnly; }
			}

		bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item)
			{
			return ((ICollection<KeyValuePair<string, object>>)_dictionary).Remove(item);
			}

		#endregion

		}

   }
