﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.WindowsAzure.ServiceRuntime;

namespace Blackriverinc.Framework.DataStore
	{
	public class CloudSettingsProvider : DataStoreProvider
      {
		public CloudSettingsProvider() : base(null)
         {
         }

		public CloudSettingsProvider(IDataStoreProvider interiorProvider)
			: base(interiorProvider)
			{
			}

      public override void Load(IKeyedDataStore store, string path = null)
         {
			base.Load(store);

			try
				{
				// Probe the Cloud Settings for any Settings to override the existing values.
				if (RoleEnvironment.IsAvailable)
					{
					// Duplicate Keys collection so we do not muck up the Enumerator.
					string[] keys = new string[store.Keys.Count];
					store.Keys.CopyTo(keys, 0);
					foreach (string key in keys)
						{
						string value = null;
						try
							{
							value = RoleEnvironment.GetConfigurationSettingValue(key);
							store[key] = value;
							}
						catch (Exception)
							{
							Trace.WriteLine(string.Format("!Information! Key [{0}] not in CloudSettings", key));
							}
						}
					}
				}
			catch (TypeInitializationException tie)
				{
				Trace.WriteLine(tie.Message);
				}
         }

      public override void Save(IKeyedDataStore store)
         {
         }
		}
	}
