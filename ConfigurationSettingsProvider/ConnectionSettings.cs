﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Text.RegularExpressions;

using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.ConnectionSettingsProvider
	{

   public class ConnectionSettings
		{
      private static ConnectionStringSettingsCollection cnxnSettings;

      static ConnectionSettings()
			{
			try
				{
				//------------------------------------------------------------------
				// Create the cnxnStrings cache on the first call into this method.
				//------------------------------------------------------------------
				cnxnSettings = new ConnectionStringSettingsCollection();
				ConnectionStringSettingsCollection dbConnectionStrings =
						ConfigurationManager.ConnectionStrings;
				if (dbConnectionStrings != null)
					{
					foreach (ConnectionStringSettings setting in dbConnectionStrings)
						cnxnSettings.Add(setting);
					}
				}
			catch (Exception ex)
				{
				Trace.TraceError(ex.ToString());
				throw;
				}
			}

      public string this[string dbConnectionKey]
         {
         get
            {
            string cnxnString = null;

            try
               {
               ConnectionStringSettings cnxnSetting = null;

               if (dbConnectionKey == null)
                  {
                  // Pull default connectionKey from AppSettings
                  dbConnectionKey = ConfigurationManager.AppSettings["DbConnectionKey"];
                  if (dbConnectionKey == null)
                     dbConnectionKey = "DEFAULT";
                  }

                  if (dbConnectionKey != null)
                     {
                     //-------------------------------------------------
                     // Pull Database connectivity and credentials from
                     // the cache.
                     //-------------------------------------------------
                     cnxnSetting = cnxnSettings[dbConnectionKey];
                     }

               if (cnxnSetting == null)
                  throw new ApplicationException(string.Format("Could not load ConnectionString [{0}]", dbConnectionKey));

               cnxnString = cnxnSetting.BindTokens();
               }
            catch (Exception ex)
               {
               Trace.TraceError(ex.ToString());
               throw;
               }

            return cnxnString;
            }

         }
		}

  
   public static class ConnectionStringSettingsExtension
      {
      public static string BindTokens(this ConnectionStringSettings cnxnSetting)
         {
         string cnxnString = null;

         //---------------------------------------------
         // Add the encrypted UID and PWD to CnxnString
         //---------------------------------------------
         Encryption decryptor = null;

         // MRM : Great place for a lambda expressions...no time today, maybe later...
         // MRM : ...It's later! Define a decoder functor.
         Func<string, string, bool, string> decoder = ((input, key, decrypt) =>
         {
            Regex keyPattern = new Regex(string.Format(@"(?:(?:{0})=\[(?<SettingKey>\w*)\])", key),
                            RegexOptions.IgnoreCase);
            var match = keyPattern.Match(cnxnSetting.ConnectionString);
            if (match.Success)
               {
               string lookup = match.Groups["SettingKey"].Value;
               string text = ConfigurationManager.AppSettings[lookup];
               if (text == null)
                  throw new ApplicationException(string.Format("Token '{0}' in ConnectionString cannot be resolved to any key in the <appSettings>.",
                                              lookup));
               if (decrypt)
                  {
                  if (decryptor == null)
                     decryptor = new Encryption();
                  text = decryptor.Decrypt(text);
                  }
               return input.Replace("[" + lookup + "]", text);
               }
            else
               return input;
         }
         );

         cnxnString = decoder(cnxnSetting.ConnectionString, "Data Source|Server", false);
         cnxnString = decoder(cnxnString, "Initial Catalog|Database", false);
         cnxnString = decoder(cnxnString, "UID|Username", true);
         cnxnString = decoder(cnxnString, "PWD|Password", true);
         return cnxnString;
         }

      }
  }
