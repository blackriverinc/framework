﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Reflection;
using System.Data.Entity;
using System.Data.Objects;
using System.Web.Script.Serialization;

namespace Blackriverinc.Framework.Utility.DataAccessLayer
{
    public enum SortDirection
    {
        Ascending = 0,
        asc = 0,
        Descending = 1,
        desc = 1
    };

    public class QueryFilters
    {
        public enum GroupOp
        {
            AND,
            OR
        }
        public enum Operations
        {
            eq, // "equal"
            ne, // "not equal"
            lt, // "less"
            le, // "less or equal"
            gt, // "greater"
            ge, // "greater or equal"
            bw, // "begins with"
            bn, // "does not begin with"
            ew, // "ends with"
            en, // "does not end with"
            cn, // "contains"
            nc,  // "does not contain"
            @in, // "in"
            ni, // "not in"
        }
        public class Rule
        {
            public string field { get; set; }
            public Operations op { get; set; }
            public string data { get; set; }
            public string display { get; set; }
            public string range { get; set; }

            public override string ToString()
            {
                return this.ToObjectString();
            }

            public string Serialize()
            {
                var serializer = new JavaScriptSerializer();
                return serializer.Serialize(this);
            }
        }
        public string searchTerm { get; set; }
        public GroupOp groupOp { get; set; }
        public List<Rule> rules { get; set; }
        private static readonly string[] FormatMapping = {
            "(it.{0} = @{1})",                 // "eq" - equal
            "(it.{0} <> @{1})",                // "ne" - not equal
            "(it.{0} < @{1})",                 // "lt" - less than
            "(it.{0} <= @{1})",                // "le" - less than or equal to
            "(it.{0} > @{1})",                 // "gt" - greater than
            "(it.{0} >= @{1})",                // "ge" - greater than or equal to
            "(it.{0}.StartsWith(@{1}))",       // "bw" - begins with
            "!(it.{0}.StartsWith(@{1}))",      // "bn" - does not begin with
            "(it.{0}.EndsWith(@{1}))",         // "ew" - ends with
            "!(it.{0}.EndsWith(@{1}))",        // "en" - does not end with
            "(it.{0}.Contains(@{1}))",         // "cn" - contains
            "!(it.{0}.Contains(@{1}))",        //" nc" - does not contain
            "(@{1}.Contains(it.{0}))",         // in - field is in the selection list.
            "!(@{1}.Contains(it.{0}))"        // in - field is in the selection list.
        };

        public QueryFilters()
        {
            rules = new List<Rule>();
            groupOp = GroupOp.AND;
        }

        public IQueryable<T> Query<T>(IQueryable<T> inputQuery) where T : class
        {
            if (rules == null || rules.Count <= 0)
                return inputQuery;

            var sb = new StringBuilder();
            var paramValues = new List<object>(rules.Count);

            foreach (Rule rule in rules)
            {
                PropertyInfo propertyInfo = typeof(T).GetProperty(rule.field);
                if (propertyInfo == null)
                    continue; // skip wrong entries

                var iParam = paramValues.Count;
                ObjectParameter param = generateParameter(rule.data, propertyInfo, iParam);
                if (param != null)
                {
                    if (sb.Length != 0)
                        sb.Append(groupOp);
                    sb.AppendFormat(FormatMapping[(int)rule.op], rule.field, iParam);
                    paramValues.Add(param.Value);
                }
            }

            string predicate = string.IsNullOrEmpty(sb.ToString()) ? "true" : sb.ToString();
            IQueryable<T> filteredQuery = inputQuery.Where(predicate, paramValues.ToArray());

            return filteredQuery;
        }

        private static ObjectParameter generateParameter(string ruleData, PropertyInfo propertyInfo, int iParam)
        {
            ObjectParameter param;
            Type type = propertyInfo.PropertyType;
            string typeName = type.FullName;

            //------------------------------------------------
            // Account for Nullable types.
            // If the value is null, do not add as a filter parameter,
            // otherwise return the underlying type.
            if (type.IsGenericType
            && type.Name.StartsWith("Nullable", StringComparison.OrdinalIgnoreCase))
            {
                if (string.IsNullOrEmpty(ruleData))
                    return null;                                            /* EXIT */

                typeName = type.GenericTypeArguments[0].FullName;
            }

            switch (typeName)
            {
                case "System.Int32":  // int
                    param = new ObjectParameter("p" + iParam, Int32.Parse(ruleData));
                    break;
                case "System.Int64":  // bigint
                    param = new ObjectParameter("p" + iParam, Int64.Parse(ruleData));
                    break;
                case "System.Int16":  // smallint
                    param = new ObjectParameter("p" + iParam, Int16.Parse(ruleData));
                    break;
                case "System.SByte":  // tinyint
                    param = new ObjectParameter("p" + iParam, SByte.Parse(ruleData));
                    break;
                case "System.Single": // Edm.Single, in SQL: float
                    param = new ObjectParameter("p" + iParam, Single.Parse(ruleData));
                    break;
                case "System.Double": // float(53), double precision
                    param = new ObjectParameter("p" + iParam, Double.Parse(ruleData));
                    break;
                case "System.Boolean": // Edm.Boolean, in SQL: bit
                    param = new ObjectParameter("p" + iParam,
                        String.Compare(ruleData, "1", StringComparison.Ordinal) == 0 ||
                        String.Compare(ruleData, "yes", StringComparison.OrdinalIgnoreCase) == 0 ||
                        String.Compare(ruleData, "true", StringComparison.OrdinalIgnoreCase) == 0);
                    break;
                case "System.DateTime":
                    param = new ObjectParameter("p" + iParam, DateTime.Parse(ruleData));
                    break;
                default:
                    // TODO: Extend to other data types
                    // binary, date, datetimeoffset,
                    // decimal, numeric,
                    // money, smallmoney
                    // and so on

                    param = new ObjectParameter("p" + iParam, ruleData);
                    break;
            }
            return param;
        }

        /// <summary>
        /// Create a Filters object by parsing a JSON-formatted string
        /// </summary>
        /// <returns>An instance of type Filters.</returns>
        public static QueryFilters Create(string filterSpec = null)
        {
            var serializer = new JavaScriptSerializer();

            QueryFilters instance = string.IsNullOrWhiteSpace(filterSpec) ? new QueryFilters() :
                                 serializer.Deserialize<QueryFilters>(filterSpec);
            return instance;
        }

        /// <summary>
        /// Remove a named rule from the Filters
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public QueryFilters RemoveRule(string field)
        {

            Rule rule = (from r in rules
                            where r.field == field
                            select r).FirstOrDefault();
            if (rule != default(Rule))
                rules.Remove(rule);

            return this;
        }

        public QueryFilters UpdateRules(string field, Operations op, string data)
        {
            return UpdateRules(new Rule() { field = field, op = op, data = data });
        }

        public QueryFilters UpdateRules(Rule rule)
        {
            if (rules == null)
            {
                rules = new List<Rule>();
                groupOp = GroupOp.AND;
            }

            Rule oldRule = (from r in rules
                            where r.field == rule.field
                            select r).FirstOrDefault();
            if (oldRule == null)
                rules.Add(rule);
            else
            {
                oldRule.data = rule.data;
                oldRule.op = rule.op;
            }

            return this;
        }

        public QueryFilters UpdateRules(NameValueCollection parameters)
        {
            foreach (string key in parameters.Keys)
            {
                if (!string.IsNullOrEmpty(parameters[key].ToString()))
                    UpdateRules(key, Operations.eq, parameters[key].ToString());
            }
            return this;
        }

        public QueryFilters AddRule(Rule rule)
        {
            if (rules == null)
            {
                rules = new List<Rule>();
                groupOp = GroupOp.AND;
            }
            rules.Add(rule);
            return this;
        }

        public QueryFilters AddRule(string field, Operations op, string data, string range = null)
        {
            return AddRule(new Rule() { field = field, op = op, data = data, range = range });
        }
        public QueryFilters AddRule(string field, Operations op, int data, string range = null)
        {
            return AddRule(new Rule() { field = field, op = op, data = data.ToString(), range = range });
        }
        public QueryFilters AddRule(string field, Operations op, DateTime data, string range = null)
        {
            return AddRule(new Rule() { field = field, op = op, data = data.ToString(), range = range });
        }
        public QueryFilters AddRule(string field, Operations op, bool data, string range = null)
        {
            return AddRule(new Rule() { field = field, op = op, data = data.ToString(), range = range });
        }

        /// <summary>
        /// Serialize to JSON-formatted string.
        /// </summary>
        public string Serialize()
        {
            var serializer = new JavaScriptSerializer();
            return serializer.Serialize(this);
        }
    }

}
