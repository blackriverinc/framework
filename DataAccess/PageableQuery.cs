﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace Blackriverinc.Framework.Utility.DataAccessLayer
{
    public static class PageableQuery<T> where T : class
    {
        /// <summary>
        /// Generate a filtered result set.
        /// </summary>
        /// <param name="totalRecords">Return parameter containing the total number of records that meet the search criteria</param>
        /// <param name="filters">Serialized representation of the filter/search criteria</param>
        /// <returns>Fitlered and sorted entity set for the requested query.</returns>
        public static IQueryable<T> Generate(IQueryable<T> query, out int totalRecords, QueryFilters filters = null)
            {
                if (filters == null)
                    filters = new QueryFilters();
                IQueryable<T> filteredQuery = filters.Query<T>(query);

                totalRecords = filteredQuery.Count();
                return filteredQuery;
            }

        /// <summary>
        /// Generates an entity set of the appropriate type for the parameters passed.  This method is to be used to generate the 
        /// data for the pageable grid UI objects.
        /// </summary>
        /// <param name="totalRecords">Return parameter containing the total number of records that meet the search criteria</param>
        /// <param name="sortByFieldName">Column to sort by</param>
        /// <param name="sortDirection">Sort Order {Ascending, Descending}</param>
        /// <param name="filters">Serialized representation of the filter/search criteria</param>
        /// <param name="page">Page of the result set to display to the user</param>
        /// <param name="rows">How many rows are displayed to the user</param>
        /// <returns>Fitlered and sorted entity set for the requested query.</returns>
        public static IQueryable<T> Generate(IQueryable<T> objectSet, out int totalRecords, string sortByFieldName, SortDirection sortDirection = SortDirection.Ascending, QueryFilters filters = null, int page = 1, int rows = int.MaxValue)
            {
                IQueryable<T> filteredQuery = Generate(objectSet, out totalRecords, filters);

                var pagedQuery = filteredQuery
                                    .OrderBy("it." + sortByFieldName + " " + sortDirection.ToString())
                                    .Skip((page - 1) * rows)
                                    .Take(rows)
                                    .AsQueryable();
                return pagedQuery;
            }

        }
    }

