﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Blackriverinc.Framework.DataStore;
using Blackriverinc.Framework.Utility;

using Win32Kernel = Blackriverinc.Framework.Common.Win32.Kernel;

using Renci.SshNet;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;

namespace Blackriverinc.Framework.RemoteAccess
   {
   public class SFTPUploader : FTPXferBase
      {
      public bool ForceUpload { get; set; }

      public override bool Run()
         {
         bool result = false;
         int uploadCount = 0;

         Regex filePattern = new Regex(FileMask, 
            RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);

         try
            {
            IList<RemoteFileInfo> downloadedFiles = new List<RemoteFileInfo>();

            Trace.WriteLine(string.Format("+++ SFTPUploader.Run  : fileMask='{1}' localPath='{2}' remotePath='{0}' +++",
                                             RemotePath, FileMask, LocalPath));

            LocalPath = new DirectoryInfo(LocalPath).FullName;

            //---------------------------------------------------------------
            // Retrieve file-info on all local files that meet criteria.
            DirectoryInfo directoryInfo = new DirectoryInfo(LocalPath);
            var localFileInfos = directoryInfo.EnumerateFiles()
                                          .Where(lfi => lfi.Name.IsMatch(FileMask))
                                          .OrderByDescending(lfi => lfi.LastWriteTimeUtc)
                                          .Select(lfi => new RemoteFileInfo()
                                          {
                                             Name = lfi.Name,
                                             IsDirectory = false,
                                             LastWriteTimeUtc = lfi.LastWriteTimeUtc,
                                             Path = LocalPath,
                                             Length = lfi.Length
                                          })
                                          .AsQueryable();

            //---------------------------------------------------------------
            // Extract the "name" of of a file using the FileMask as a 
            // pattern to extract the core name of a file.
            // This logic can be used to account for transformations on the 
            // file as it passes through the server's processing pipline.
            //
            //  filename.ext => filename#jobnumber.ext
            //
            var nameExtractor
                  = new Func<RemoteFileInfo, string>((rfi) => 
                     {
                     var match = filePattern.Match(rfi.Name);
                     if (!match.Success)
                        return null;

                     string extractedName = match.Groups["name"].Value;
                     if (string.IsNullOrEmpty(extractedName))
                        return rfi.Name;
                     string extension = match.Groups["extension"].Value;

                     return extractedName + extension;
                     });

            //---------------------------------------------------------------
            // Define a Functor to return 'true' for a missing or stale file.
            // Upload a file, if at least one of these conditions is met...
            // 1) "Force Upload" flag is set, or
            // 2) No local file matching any remote file, or
            // 3) "Synch On Date" flag set and the latest item in the list
            //    of remote files that match by name is earlier than the
            //    current version of the local file.
            var missingOrStale 
               = new Func<RemoteFileInfo, IEnumerable<RemoteFileInfo>, bool>((lfi, rfis) =>
               {
               bool needsUpload = 
                       ForceUpload
                    || !rfis.Contains(lfi, keyExtractor: nameExtractor)
                    || (SynchOnDate
                           // Latest matching file is older than current local file.
                        && rfis.Where(rfi => nameExtractor(rfi) == lfi.Name)
                               .OrderByDescending(rfi => rfi.LastWriteTimeUtc) 
                               .First().LastWriteTimeUtc.AddMilliseconds(ClockSkewMs) 
                                          < lfi.LastWriteTimeUtc)
                    ;
               Trace.WriteLine("{0}: Local File {1}".Bind((needsUpload) ? "INFO" : "TRACE", lfi));

               return needsUpload;
               });

            using (var ftpClient = new SftpClient(RemoteHostName, RemoteUserName, RemotePassword))
               {
               //---------------------------------------------------------------
               // Open  connection to remote site.
               ftpClient.Connect();
               ftpClient.BufferSize = BufferSize;

               List<RemoteFileInfo> remoteFileInfos = new List<RemoteFileInfo>();
               //---------------------------------------------------------------
               // Retrieve file-info on all files on the Remote Site that meet 
               // our search criteria.
               string[] remotePaths = RemotePath.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
               foreach(string remotePath in remotePaths)
                  remoteFileInfos.AddRange(ftpClient.ListDirectory(remotePath)
                                                .Where(sftp => sftp.Name.IsMatch(FileMask))
                                                .OrderByDescending(sftp => sftp.LastAccessTimeUtc)
                                                .Select(sftp => new RemoteFileInfo()
                                                   {
                                                      Name = sftp.Name,
                                                      IsDirectory = sftp.IsDirectory,
                                                      LastWriteTimeUtc = sftp.LastWriteTimeUtc,
                                                      Path = remotePath,
                                                      Length = sftp.Length
                                                   }));

               foreach (var rfi in remoteFileInfos)
                  OnRemoteFileFound(rfi);

               //---------------------------------------------------------------
               // Upload missing or "stale" files.
               foreach (var localFileInfo in localFileInfos
                                                .Where(lfi => missingOrStale(lfi, remoteFileInfos)))
                  {
                  if (SuppressXfer)
                     continue;

                  long uploadSize = 0;

                  string localFilePath = Path.Combine(LocalPath, localFileInfo.Name);
                  using (var stream = StreamFactory.Create(@"file://" + localFilePath,
                                                            FileAccess.Read,
                                                            FileMode.Open))
                     {
                     //------------------------------------------------------
                     // Upload the local file to remote site.
                     string remoteFilePath = remotePaths[0] + "/" + localFileInfo.Name;

                     ftpClient.UploadFile(stream, remoteFilePath, true,
                                             new Action<ulong>((arg) =>
                                             {
                                                Interlocked.Read(ref uploadSize);
                                             }));
                        
                     //------------------------------------------------------
                     // Verify entire file was downloaded.
                     var sftpFile = ftpClient.GetAttributes(remoteFilePath);
                     if (sftpFile.Size != stream.Length)
                        {
                        throw new SshException("Upload size ({0}) not equal to input size ({1})."
                                                   .Bind(uploadSize, stream.Length));
                        }

                     if (!OnFileTransferComplete(localFileInfo.Name,
                                                   sftpFile.Size,
                                                   RemotePath,
                                                   sftpFile.LastWriteTime.ToUniversalTime()))
                        throw new ApplicationException("FAIL: File transfer aborted.");

                     uploadCount++;

                     }
                  }
               }
            }
         catch (Exception exp)
            {
            Trace.WriteLine("FAIL: " + exp.Message);
            Trace.WriteLine("INFO: " + exp.ToString());
            }
         finally
            {
            Trace.WriteLine("--- SFTPUploader.Run : {0} files ---".Bind(uploadCount));
            }

         return result;
         }
      }
   }
