﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackriverinc.Framework.RemoteAccess
   {
   interface IFTPClient
      {
      string RemotePath { get; set; }

      string RemoteHostName { get; set; }

      string RemoteUserName { get; set; }

      string RemotePassword { get; set; }

      string RemoteSSHFingerprint { get; set; }

      long TimeoutMs { get; set; }

      uint BufferSize { get; set; }
      }
   }
