﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackriverinc.Framework.RemoteAccess
   {

   public class FTPEventArgs : EventArgs
      {
      public RemoteFileInfo RemoteFileInfo { get; set; }
      }


   public delegate bool FTPEventHandler(object sender, FTPEventArgs ftpEventArg);

   }
