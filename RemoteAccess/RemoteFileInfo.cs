﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.RemoteAccess
{
   public class RemoteFileInfo
      {
      public string Name { get; set; }
      public DateTime LastWriteTimeUtc { get; set; }
      public bool IsDirectory { get; set; }
      public long Length { get; set; }
      public string Path { get; set; }
      public byte[] Fingerprint { get; set; }

      public override string ToString()
         {
         return this.ToObjectString("Name LastWriteTimeUtc IsDirectory Length Path");
         }
      }
}
