﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using Blackriverinc.Framework.Common;
using Blackriverinc.Framework.Common.Win32;
using Blackriverinc.Framework.DataStore;
using Blackriverinc.Framework.Utility;

using Win32Kernel = Blackriverinc.Framework.Common.Win32.Kernel;

using Renci.SshNet;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;

namespace Blackriverinc.Framework.RemoteAccess
   {
   public class SFTPDownloader : FTPXferBase
      {
      public SFTPDownloader()
         {
         RemoteFileFound += ((sender, fea) =>
            {
            return missingOrStale(fea.RemoteFileInfo.Name,
                                  fea.RemoteFileInfo.Length,
                                  fea.RemoteFileInfo.Path,
                                  fea.RemoteFileInfo.LastWriteTimeUtc);
            });
         }

      bool missingOrStale(string fileName,
                           long bytesTransferred,
                           string path = null,
                           DateTime? eventTime = null)
        {
        bool result = true;

        string localFilePath = Path.Combine(LocalPath, fileName);

        result = !File.Exists(localFilePath)
               || File.GetLastWriteTimeUtc(localFilePath) < eventTime;

        return result;
        }

      public override bool Run()
         {
         bool result = false;

         Task task = new Task(() =>
            {
            string fileName = null;

            IList<RemoteFileInfo> downloadedFiles = new List<RemoteFileInfo>();

            Trace.WriteLine(string.Format("+ SFTPDownloader::Run fileMask='{1}' remotePath='{0}' localPath='{2}' +",
                                             RemotePath, FileMask, LocalPath));

            // Create Temp Folder to hold scratch versions of downloaded files.
            string tempPath = Path.Combine(LocalPath, Guid.NewGuid().ToString());
            Directory.CreateDirectory(tempPath);

            LocalPath = new DirectoryInfo(LocalPath).FullName;

            try
               {
               using (var ftpClient = new SftpClient(RemoteHostName, RemoteUserName, RemotePassword))
                  {
                  //---------------------------------------------------------------
                  // Open  connection to remote site.
                  ftpClient.Connect();

                  //---------------------------------------------------------------
                  // Retrieve file-info on all files on the Remote Site that meet 
                  // our search criteria.
                  var remotefiles = ftpClient.ListDirectory(RemotePath);
                  foreach (var remoteFile in remotefiles.Where(f => f.Name.IsMatch(FileMask))
                                             .OrderByDescending(f => f.LastWriteTimeUtc)
                                             )
                     {
                     fileName = remoteFile.Name;

                     //byte[] fingerprint = null;

                     //------------------------------------------------------------
                     // Synchronize missing files, and stale files.
                     string localFilePath = Path.Combine(LocalPath, remoteFile.Name);

                     bool needsDownload = OnRemoteFileFound(fileName, 
                                           remoteFile.Length,
                                           RemotePath,
                                           remoteFile.LastWriteTimeUtc);
                     Trace.WriteLine(string.Format("{1}: {0}", remoteFile, (needsDownload)?"INFO":"TRACE"));
                     if (needsDownload && !SuppressXfer)
                        {
                        //---------------------------------------------------------
                        // Create File in Temp folder to contain scratch copy of file
                        string tempFilePath = Path.Combine(tempPath, remoteFile.Name);
                        ulong downloadSize = 0;
                        using (var stream = StreamFactory.Create(@"file://" + tempFilePath,
                                                                  FileAccess.ReadWrite,
                                                                  FileMode.OpenOrCreate))
                           {
                           //------------------------------------------------------
                           // Download file from remote folder to scratch folder.
                           ftpClient.DownloadFile(remoteFile.FullName,
                                                  stream,
                                                  new Action<ulong>((arg) =>
                                                  { downloadSize = arg; }));

                           //------------------------------------------------------
                           // Verify entire file was downloaded.
                           if (downloadSize != (ulong)stream.Length)
                              {
                              throw new SshException(string.Format("Download size ({0}) not equal to output size ({1}).",
                                                               downloadSize, stream.Length));
                              }

                           //------------------------------------------------------
                           // Compute fingerprint.
                           //fingerprint = stream.ComputeSHA1Fingerprint();
                           }

                        //---------------------------------------------------------
                        // Callback to any event listeners.
                        if (!OnFileTransferComplete(fileName,
                                                    (long)downloadSize,
                                                    tempPath, 
                                                    remoteFile.LastWriteTimeUtc))
                           throw new ApplicationException(string.Format("FAIL: File transfer aborted."));

                        //---------------------------------------------------------
                        // Record results of download as a Remote File Info record.
                        var remoteFileInfo = new RemoteFileInfo()
                           {
                           Name = remoteFile.Name,
                           Path = RemotePath,
                           IsDirectory = false,
                           LastWriteTimeUtc = remoteFile.LastWriteTimeUtc,
                           Length = (long)downloadSize
                           };

                        //---------------------------------------------------------
                        // Move files from scratch folder to local folder.
                        FileSystem.MoveFiles(Path.Combine(tempPath, @"*.*"), LocalPath, true);

                        //---------------------------------------------------------
                        // Update list of downloaded files.
                        downloadedFiles.Add(remoteFileInfo);
                        }
                     }
                  }
               }
            catch (Exception exp)
               {
               OnFileTransferComplete(fileName, -1);
               Trace.WriteLine(string.Format("FAIL: {0} {1}", fileName, exp.Message));
               Trace.WriteLine(exp.ToString());
               }
            finally
               {
               //---------------------------------------------------------
               // Clean up Temp Download Folder.
               if (!string.IsNullOrEmpty(tempPath))
                  Directory.Delete(tempPath, true);

               Trace.WriteLine(string.Format("- SFTPDownloader::Run  = {0} -", downloadedFiles.Count()));
               }
            });

         task.Start();

         task.Wait();

         return result;
         }
      }
   }
