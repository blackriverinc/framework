﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using Framework.Common;
using Framework.Utility;

namespace Framework.RemoteAccess
   {
   public class SyncedFile
      {
      class RemoteSource : RemoteFileInfo
         {
         public string RemoteHost { get; set; }
         }

      FTPBase _downloader = null;

      public RemoteFileInfo RemoteFileInfo { get; private set;}

      public SyncedFile(string remoteHost, string remoteUsername, string remotePassword)
         {
         _downloader = new SFTPDownloader()
            {
            RemoteHostName = remotePassword,
            RemoteUserName = remoteUsername,
            RemotePassword = remotePassword
            };
         }

      public SyncedFile(FTPBase downloader)
         {
         _downloader = downloader;
         }

      /// <summary>
      /// Download the requested file, if it is later vintage.
      /// </summary>
      public bool DownloadIfNewer(string remotePath, string localPath)
         {
         bool result = false;

         _downloader.RemotePath = remotePath;
         _downloader.LocalPath = localPath;

         RemoteFileInfo remoteFileInfo = _downloader.GetRemoteFileInfo();
         using (var altStream = AlternateDataStream.Create(localPath, "remoteSource",
                                                         FileMode.OpenOrCreate,
                                                         FileAccess.ReadWrite))
            {
            RemoteSource remoteSource = null;
            if (altStream.Length > 0)
               {
               if (XmlSerializeHelper.Deserialize<RemoteSource>(altStream, ref remoteSource))
                  {
                  if ((remoteSource == null)
                  ||  (remoteSource.LastWriteTime < remoteFileInfo.LastWriteTime))
                     {
                     _downloader.Run();
                     }
                  }              
               }

            XDocument xdoc = new XDocument();
            var root = new XElement(XName.Get("RemoteSource", ""));

            root.Add(new XElement(XName.Get("hostname", ""), _downloader.RemoteHostName));
            root.Add(new XElement(XName.Get("path", ""), _downloader.RemotePath));
            root.Add(new XElement(XName.Get("filename", ""), _downloader.LocalPath));
            root.Add(new XElement(XName.Get("size", ""), remoteFileInfo.Length));
            root.Add(new XElement(XName.Get("modificationTime", ""), remoteFileInfo.LastWriteTime));

            xdoc.Add(root);

            xdoc.Save(altStream);
            }

         return result;
         }
      }
   }
