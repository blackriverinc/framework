﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackriverinc.Framework.RemoteAccess
   {
   public interface IRemoteDirectory
      {
      IEnumerable<RemoteFileInfo> List(string fileMask);

      bool Create(string path = ".");

      bool Remove(string path = ".");
      }
   }
