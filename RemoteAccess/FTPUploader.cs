﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Blackriverinc.Framework.DataStore;

namespace Blackriverinc.Framework.RemoteAccess
   {
   public class FTPUploader : FTPXferBase
      {
      public bool Secure { get; set; }

      public override bool Run()
         {
         bool result = true;

         FtpWebRequest request = (FtpWebRequest)WebRequest.Create(RemoteHostName);
         request.EnableSsl = Secure;

         request.Method = WebRequestMethods.Ftp.UploadFile;

         request.Credentials = new NetworkCredential(RemoteUserName, RemotePassword);

         Task task = new Task(() =>
         {
         try
            {
            Trace.WriteLine(string.Format("+++ FTPUpload {0} +++", RemoteHostName));

            using (Stream inputStream = StreamFactory.Create("@file://" + LocalPath))
               {
               // Copy the contents of the file to the request stream.
               StreamReader sourceStream = new StreamReader(inputStream);
               byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
               sourceStream.Close();
               request.ContentLength = fileContents.Length;

               Stream requestStream = request.GetRequestStream();
               requestStream.Write(fileContents, 0, fileContents.Length);
               requestStream.Close();

               FtpWebResponse response = (FtpWebResponse)request.GetResponse();

               Trace.WriteLine(string.Format("--- FTPUpload {0} ---", RemoteHostName));
               }
            }
         catch (InvalidOperationException ioexp)
            {
            Trace.Fail(ioexp.ToString());
            }
         finally
            {
            }

         });

         task.Start();

         task.Wait();

         return result;
         }

      }
   }
