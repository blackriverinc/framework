﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Blackriverinc.Framework.Utility;

using Renci.SshNet;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;

namespace Blackriverinc.Framework.RemoteAccess
   {
   public class RemoteDirectory : IFTPClient, IRemoteDirectory
      {

      #region IFTPClient

      public string RemotePath { get; set; }
      public string RemoteHostName { get; set; }
      public string RemoteUserName { get; set; }
      public string RemotePassword { get; set; }
      public string RemoteSSHFingerprint { get; set; }
      public long TimeoutMs { get; set; }
      public uint BufferSize { get; set; }

      #endregion

      #region IRemoteDirectory

      public IEnumerable<RemoteFileInfo> List(string fileMask = ".*")
         {
         using (var ftpClient = new SftpClient(RemoteHostName, RemoteUserName, RemotePassword))
            {
            //---------------------------------------------------------------
            // Open  connection to remote site.
            ftpClient.Connect();

            var remoteFiles = ftpClient.ListDirectory(RemotePath)
                     .Where(rf => rf.Name.IsMatch(fileMask))
                     .Select(rf => new RemoteFileInfo()
                                       {
                                       Path = RemotePath,
                                       IsDirectory = rf.IsDirectory,
                                       Length = rf.Length,
                                       LastWriteTimeUtc = rf.LastWriteTimeUtc,
                                       Name = rf.Name
                                       });

            return remoteFiles;
            }
         }

      public bool Create(string path = ".")
         {
         throw new NotImplementedException();
         }

      public bool Remove(string path = ".")
         {
         throw new NotImplementedException();
         }

      #endregion

      }
   }
