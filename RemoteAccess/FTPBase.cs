﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using Blackriverinc.Framework.DataStore;

using Win32Kernel = Blackriverinc.Framework.Common.Win32.Kernel;

namespace Blackriverinc.Framework.RemoteAccess
   {
   public abstract class FTPXferBase : IFTPClient
      {
      #region Parameters

      public string FileMask { get; set; }

      public string LocalPath { get; set; }

      public string RemotePath { get; set; }

      public string RemoteHostName { get; set; }

      public string RemoteUserName { get; set; }

      public string RemotePassword { get; set; }

      public string RemoteSSHFingerprint { get; set; }

      public long TimeoutMs { get; set; }

      public uint BufferSize { get; set; }

      public bool SynchOnDate { get; set; }

      public int ClockSkewMs { get; set; }

      public bool SuppressXfer { get; set; }

      #endregion

      /// <summary>
      /// Either File Transfer OK (Length > 0) or failed (Length == -1)
      /// </summary>
      public event FTPEventHandler FileTransferComplete;

      /// <summary>
      /// Remote File located.
      ///   Name : file part of name
      ///   Length : Size of remote file
      ///   LastWriteTime : Remote File's Last Write Time (UTC)
      ///   Path : Current Local Path
      /// </summary>
      public event FTPEventHandler RemoteFileFound;

      /// <summary>
      /// Remote File located.
      ///   Name : file part of name
      ///   Length : Size of local file
      ///   LastWriteTime : File's Last Write Time (UTC)
      ///   Path : Current Local Path
      /// </summary>
      //public event FTPEventHandler LocalFileFound;

      protected FTPXferBase()
         {
         // Default Timeout = 30sec.
         TimeoutMs = 30 * 1000;

         BufferSize = 8 * 1024;

         ClockSkewMs = 500;
         }

      public abstract bool Run();

      protected bool OnRemoteFileFound(string fileName,
                                       long bytesTransferred,
                                       string path = null,
                                       DateTime? eventTime = null)
         {
         if (RemoteFileFound != null)
            return RemoteFileFound(this, new FTPEventArgs()
                                    {
                                    RemoteFileInfo = new RemoteFileInfo()
                                       {
                                          Name = fileName,
                                          Length = bytesTransferred,
                                          LastWriteTimeUtc = (DateTime)(!eventTime.HasValue ? DateTime.Now.ToUniversalTime() : eventTime),
                                          Path = string.IsNullOrEmpty(path) ? RemotePath : path
                                       }
                                    });

         return true;
         }

      protected bool OnRemoteFileFound(RemoteFileInfo remoteFileInfo)
         {
         if (RemoteFileFound != null)
            return RemoteFileFound(this, new FTPEventArgs()
                                             {
                                             RemoteFileInfo = remoteFileInfo
                                             });

         return true;
         }

      protected bool OnFileTransferComplete(string fileName,
                                            long bytesTransferred, 
                                            string path = null,
                                            DateTime? eventTime = null)
         {
         if (FileTransferComplete != null)
            return FileTransferComplete(this, new FTPEventArgs()
                                          {
                                          RemoteFileInfo = new RemoteFileInfo()
                                                {
                                                Name = fileName,
                                                Length = bytesTransferred,
                                                LastWriteTimeUtc = (DateTime)(!eventTime.HasValue?DateTime.Now.ToUniversalTime():eventTime),
                                                Path = string.IsNullOrEmpty(path)?LocalPath:path
                                                }
                                          });
         return true;
         }

      /// <summary>
      /// Append alternate data stream containing Fle Info
      /// from the Remote site.
      /// </summary>
      /// <param name="localFilePath"></param>
      /// <param name="remoteHostname"></param>
      /// <param name="remoteFileInfo"></param>
      protected RemoteFileInfo readRemoteSourceDataStream(string localFilePath, string remoteHostname, RemoteFileInfo remoteFileInfo)
         {
         using (var altStream = Win32Kernel.FileSystem.CreateFileStream(localFilePath + ":remoteSource",
                                                        FileMode.Open, FileAccess.Read))
            {
            XDocument xdoc = XDocument.Load(altStream);

            return new RemoteFileInfo()
               {
               Name = xdoc.Element(XName.Get("filename", "")).Value,
               Path = xdoc.Element(XName.Get("path", "")).Value,
               Length = long.Parse(xdoc.Element(XName.Get("size", "")).Value),
               LastWriteTimeUtc = DateTime.Parse(xdoc.Element(XName.Get("size", "")).Value)
               };
            }
         }

      /// <summary>
      /// Append alternate data stream containing Fle Info
      /// from the Remote site.
      /// </summary>
      /// <param name="localFilePath"></param>
      /// <param name="remoteHostname"></param>
      /// <param name="remoteFileInfo"></param>
      protected void writeRemoteSourceDataStream(string localFilePath, string remoteHostname, RemoteFileInfo remoteFileInfo)
         {
         using (var altStream = Win32Kernel.FileSystem.CreateFileStream(localFilePath + ":remoteSource",
                                                        FileMode.CreateNew, FileAccess.ReadWrite))
            {
            StringBuilder bldr = new StringBuilder();
            foreach (byte b in remoteFileInfo.Fingerprint)
               {
               if (bldr.Length == 0)
                  bldr.AppendFormat("SHA-{0} ", b);
               else
                  bldr.Append(" ");
               bldr.AppendFormat("{0:X2}", b);
               }

            XDocument xdoc = new XDocument();
            var root = new XElement(XName.Get("RemoteSource", ""));

            root.Add(new XElement(XName.Get("hostname", ""), remoteHostname));
            root.Add(new XElement(XName.Get("path", ""), remoteFileInfo.Path));
            root.Add(new XElement(XName.Get("filename", ""), remoteFileInfo.Name));
            root.Add(new XElement(XName.Get("size", ""), remoteFileInfo.Length));
            root.Add(new XElement(XName.Get("modificationTime", ""), remoteFileInfo.LastWriteTimeUtc));
            root.Add(new XElement(XName.Get("fingerprint", ""), bldr.ToString()));

            xdoc.Add(root);

            xdoc.Save(altStream);
            }
         }
      }
   }
