﻿using System;
using System.IO;
using Blackriverinc.Framework.Utility;


namespace Blackriverinc.Framework.ScheduledJob
	{

	#region StreamEvents

	/// <summary>
	/// StreamEvents are used by ScheduledJobs to signal observers that
	/// a stream of results is ready for action.
	/// </summary>
	public class StreamReadyEventArgs : EventArgs
		{
		string _subject = null;
		public string Subject
			{
			get
				{
				if (_subject != null)
					return _subject;
				return "";
				}
			}
		Stream _outputStream = null;
		public Stream OutputStream
			{
			get
				{
				return _outputStream;
				}
			}

		bool _complete;
		public bool Complete
			{
			get
				{
				return _complete;
				}
			}
		public StreamReadyEventArgs(string subject, Stream outputStream, bool complete = true)
			{
			_subject = subject;
			_outputStream = outputStream;
			_complete = complete;
			}
		}

	public delegate void StreamEventHandler(object sender, StreamReadyEventArgs srea);

	#endregion

	public enum JobState
		{
		Created,
		Started,
		Running,
		Failed,
		Done
		}

	public struct JobStateEventArg
		{

		JobState _jobState;
		public JobState JobState 
			{
			get
				{
				return _jobState;
				}
			}

		int? _status;
		public int? Status
			{
			get
				{
				return _status;
				}
			}

		DateTime _stamp;
		public DateTime Stamp 
			{
			get 
				{ return _stamp; }
			}

		public JobStateEventArg(JobState jobState, int? status = null, DateTime? stamp = null)
			{
			if (!stamp.HasValue)
				_stamp = DateTime.Now;
			else
				_stamp = stamp.Value;

			_status = status;

			_jobState = jobState;
			}
		}

	public delegate void JobStateEventHandler(IScheduledJob ijob, JobStateEventArg jsea);

	public interface IScheduledJob : IRunnable, IDisposable
		{
		#region IScheduledJob

		/// <summary>
		/// Signals when data available on stream
		/// </summary>
		event StreamEventHandler StreamReady;

		event JobStateEventHandler JobStateChange;

		/// <summary>
		/// Retrieve reference to the JobParameters
		/// </summary>
      bool GetJobParameter(string key, out string value);
      bool GetJobParameter(string key, out DateTime value);
      bool GetJobParameter(string key, out int value);

      void SetJobParameter(string key, object value);

		/// <summary>
		/// RunDate defaults to Now unless overriden by JobParameters["RunDate"]
		/// or parsed from command line
		/// </summary>
		DateTime RunDate { get; set;  }

		/// <summary>
		/// ScheduledJobs Controller has thrown and caught an internal exception. The 
		/// ExceptionReport has been dispatched to the StreamDone subscribers.
		/// </summary>
		bool IsExceptionReported { get; }

		#endregion

		}
	}
