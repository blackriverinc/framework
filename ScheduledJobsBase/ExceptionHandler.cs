﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;

using Blackriverinc.Framework.DataView;
using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.ScheduledJob
	{
	/// <summary>
	/// 
	/// </summary>
	public class ExceptionHandler
		{

		public static Stream ExceptionToStream(Exception exp, Stream stream = null)
			{
			IHTMLReportView errorBlock
				= new HTMLReportView<TextTemplateControl>(stream);

			StringBuilder bodyText = new StringBuilder();
			bodyText.Append(exp.StackTrace.Replace("\n", @"<br/>"));
			bodyText.Append("<span style='font-weight: bold'>Contact systems support and report the circumstances of this error.</span>");

			errorBlock.Text = bodyText.ToString();

			return errorBlock.Render((IEnumerable)null);
			}

		public static Stream ExceptionPageToStream(Exception exp, Stream stream = null)
			{
			HTMLReportDocumentView<TextTemplateControl> errorPage
				= new HTMLReportDocumentView<TextTemplateControl>(stream);

			errorPage.ReportHeading = "Contact systems support and report the circumstances of this error.";
			
			StringBuilder bodyText = new StringBuilder();

			do
				{
            // Once HandleableExceptions are "handled", we should not deal with them again
            HandleableException hex = exp as HandleableException;

            // HandleableExceptions contain the real Exception as the InnerException
				if (hex == null || hex.Handled)
					{
					bodyText.AppendFormat("<p style='font-weight: bold;'>{0:MM/dd/yyyy hh:mm:ss} - {2} : {1}</p>",
								DateTime.Now,
								exp.Message.Replace("\n", @"<br/>"),
                        exp.GetType().Name);
               if (exp.StackTrace != null)
                  {
                  bodyText.AppendLine("<p>");
                  bodyText.Append(exp.StackTrace.Replace("\n", @"<br/>"));
                  bodyText.AppendLine("</p>");
                  }
					}
				else
					hex.Handled = true;
				}
			while ((exp = exp.InnerException) != null);
	
			errorPage.Text = bodyText.ToString();

			return errorPage.Render((IEnumerable)null);
			}

		}
	}
