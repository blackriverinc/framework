﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using System.Text;

using Blackriverinc.Framework.DataStore;

namespace Blackriverinc.Framework.ScheduledJob
   {
   public delegate void FailureNotificationHandler(object source, string message);


   public class FailureNotificationTraceListener : TraceListener
      {
      static bool _notifying = false;

      public event FailureNotificationHandler FailureNotified;

      protected void OnFailureNotification(string message)
         {
         try
            {
            if (!_notifying 
            && message.StartsWith("FAIL", StringComparison.CurrentCultureIgnoreCase)
            && FailureNotified != null)
               {
               // Protect from recursive failure of the failure notifications.
               _notifying = true;

               IKeyedDataStore cache = GlobalCache.LockData;
               cache["LastErrorMessage"] = message;

               StackFrame invoker = new StackFrame(3);
               MethodBase method = invoker.GetMethod();
               FailureNotified(method.Name, message);
               _notifying = false;
               }
            }
         finally
            {
            GlobalCache.ReleaseData();
            }
         }

      public FailureNotificationTraceListener()
         {
         System.Diagnostics.Trace.Listeners.Add(this);
         }

      public override void Write(string message)
         {
         OnFailureNotification(message);   
         }

      public override void WriteLine(string message)
         {
         OnFailureNotification(message);
         }
      }
   }
