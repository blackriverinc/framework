﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Linq;
using System.Text;

namespace Blackriverinc.Framework.ScheduledJob
	{
	public interface IRunnable
		{
		void Run();

		// Sets STOP Flag
		void Stop();
		}

	/// <summary>
	/// Operator pressed Ctrl-C
	/// </summary>
	public class StopSignalled : ApplicationException
		{
		public StopSignalled() : base("*Stop* Signalled")
			{

			}
		}

	}
