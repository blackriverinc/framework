﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.Text;

using Blackriverinc.Framework.DataStore;
using Blackriverinc.Framework.Utility;

namespace Blackriverinc.Framework.ScheduledJob
	{
   public abstract class ScheduledJobsConsoleLauncherBase<TScheduledJob>
      where TScheduledJob : ScheduledJobBase, new()
		{
      static int? _exitCode = null;
      static public int ExitCode
         {
         get
            {
            return (_exitCode.HasValue) ? _exitCode.Value : 1;
            }
         }

      protected static void LaunchJob()
			{
         string jobName = null;

         try
				{
            var cache = GlobalCache.LockData;
            foreach (var property in typeof(TScheduledJob).GetProperties(BindingFlags.Public
                            | BindingFlags.Instance))
                  {
                  switch(property.PropertyType.Name)
                     {
                     case "String":
                     default:
                        cache.Add(property.Name, string.Empty);
                        break;
                     case "DateTime":
                        cache.Add(property.Name, DateTime.Now);
                        break;
                     case "Int32":
                        cache.Add(property.Name, 0);
                        break;
                     case "Boolean":
                        cache.Add(property.Name, false);
                        break;
                     }
                  }
				}
         finally
            {
            GlobalCache.ReleaseData();
            }
         IDataStoreProvider provider = ConfigProviderBase.Open();
         GlobalCache.LoadConfigurationSettings(provider);

         //----------------------------------------------------------------
         // Parse CommandLine for overrides
         //----------------------------------------------------------------
         try
            {
            IKeyedDataStore parameters = GlobalCache.LockData;
            CommandLineParser.Parse(parameters, System.Environment.CommandLine, true);
            jobName = GlobalCache.GetResolvedString("JobName");
            if (jobName == null)
               {
               Assembly assembly = Assembly.GetExecutingAssembly();
               jobName = Path.GetFileNameWithoutExtension(assembly.GetName().Name);
               parameters.Add("JobName", jobName);
            }

            parameters.Add("CommandLine", System.Environment.CommandLine);
            parameters.Add("MachineName", System.Environment.MachineName);
            parameters.Add("Username", string.Format(@"{0}\{1}", 
                                       System.Environment.UserDomainName,
                                       System.Environment.UserName));
            }
         finally
				{
            GlobalCache.ReleaseData();
				}

         string logPath = GlobalCache.GetResolvedString("LogPath");
         AppTraceListener tracer = new AppTraceListener(Path.Combine((!string.IsNullOrEmpty(logPath)) ? logPath : "",
                                                                     jobName));
			StringBuilder sb = new StringBuilder();

			try
				{
				//-----------------------------------------------------------------
				// Attach new tracer to the System.Diagnostics.Trace. After this
				// point, all Write/WriteLine commands to Trace and Debug.
				//-----------------------------------------------------------------
				tracer.DisplayTaskID = true;
				
				//-----------------------------------------------------------------
				// Create a Controller and Run it.
				//-----------------------------------------------------------------
            using (TScheduledJob controller
               = new TScheduledJob())
					{
               // Construction/Initialization tossed an exception...bye! bye!
               if (controller.IsExceptionReported)
                  return;

               //---------------------------------------------------------------
               // Synch any matching GlobalCache items to public instance
               // properties in the Controller or it's base.
               var properties = typeof(TScheduledJob).GetProperties(BindingFlags.Public
                                                      | BindingFlags.Instance);

               foreach (var property in properties)
                  {
                  if (property.GetSetMethod() != null)
                     {
                     object value = GlobalCache.Get(property.Name);

                     if (value != null)
                        {
                        string strValue = GlobalCache.ResolveString(value.ToString());
                        switch (property.PropertyType.Name)
                           {
                           case "String":
                           default:
                              value = strValue;
                              break;
                           case "DateTime":
                              DateTime dtValue = default(DateTime);
                              DateTime.TryParse(strValue, out dtValue);
                              value = dtValue;
                              break;
                           case "Int32":
                              int intValue = default(int);
                              int.TryParse(strValue, out intValue);
                              value = intValue;
                              break;
                           case "Boolean":
                              bool boolValue = true;
                              bool.TryParse(strValue, out boolValue);
                              value = boolValue;
                              break;

                           }
                        property.SetValue(controller, value, null);

                        Trace.WriteLine("TRACE: Property[{0}] = '{1}'".Bind(property.Name, value));
                        }
                     }
                  }

					//----------------------------------------------------------------
					// Setup Standard Callbacks to issue an Email and write a log file
					//	with any Reports issued by the Controller.
					//----------------------------------------------------------------
					//controller.StreamDone += controller.NotifyEmail;
					controller.StreamReady += controller.NotifyLog;

               controller.LogPath = Path.GetDirectoryName(tracer.LogFilePath);

					//----------------------------------------------------------------
					// Dump the appSettings to the trace log.
					//----------------------------------------------------------------
               try
                  {
                  var appSettings = GlobalCache.LockData;
					sb.Length = 0;
                  sb.AppendLine(String.Format("<{0}>", appSettings));
					foreach (KeyValuePair<string, object> item in appSettings)
						XmlSerializeHelper.SerializeToString(ref sb, item);
                  sb.AppendLine(String.Format("</{0}>", appSettings));
					Trace.WriteLine(sb.ToString());
					sb.Length = 0;

                  appSettings["LogPath"] = controller.LogPath;
                  }
               finally
                  {
                  GlobalCache.ReleaseData();
                  }
               // Break into the debugger
               bool debug = false;
               GlobalCache.Get("Debug", ref debug);
               if (debug)
                  Debugger.Break();

					//----------------------------------------------------------------
					// Hook Ctrl-C to signal Controller to Stop running.
					//----------------------------------------------------------------
					Console.CancelKeyPress +=
						((sender, ea) =>
							{
								Trace.WriteLine("Operator aborted.");

								// Set the stop flag
								((IRunnable)controller).Stop();

								// Let application wind-down
								ea.Cancel = true;
                        _exitCode = 1;
							});

					Console.WriteLine("Press Ctrl-C to stop...");

					//----------------------------------------------------------------
					//	Synchronous "Go!"
					//----------------------------------------------------------------
               
               if (!controller.DiagnosticsOnly)
					controller.Run();

               if (!_exitCode.HasValue)
                  _exitCode = (controller.IsExceptionReported)?-1:0;
					}
				}
         catch (HandleableException hexp)
            {
            if (!hexp.Handled)
               {
               Trace.WriteLine("*** Caught Exception at 'Run' ***");
               Trace.Fail(hexp.Message);
               Trace.Fail(hexp.InnerException.ToString());
               }
            }
			catch (Exception exp)
				{            
				Trace.WriteLine("*** Caught Exception at 'Run' ***");
            Trace.Fail(exp.Message);
            Trace.WriteLine(exp.StackTrace);
            while ((exp = exp.InnerException) != null)
               {
				Trace.WriteLine(exp.ToString());
               }
            _exitCode = -1;
				}
			finally
				{
				tracer.Dispose();
				}
			}
		}
	}

