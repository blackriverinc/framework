﻿using System;
using BlackRiverInc.Framework.DataStore;


namespace BlackRiverInc.Framework.ScheduledJobs
	{
	public interface IScheduledJobs : IRunnable, IDisposable
		{

		#region IScheduledJobs

		/// <summary>
		/// Signals when data available on stream
		/// </summary>
		event StreamEventHandler StreamDone;

		/// <summary>
		/// Retrieve reference to the JobParameters
		/// </summary>
		Parameters JobParameters { get; }

		/// <summary>
		/// RunDate defaults to Now unless overriden by JobParameters["RunDate"]
		/// or parsed from command line
		/// </summary>
		DateTime RunDate { get; set;  }

		/// <summary>
		/// ScheduledJobs Controller has thrown and caught an internal exception. The 
		/// ExceptionReport has been dispatched to the StreamDone subscribers.
		/// </summary>
		bool IsExceptionReported { get; }

		#endregion

		}
	}
