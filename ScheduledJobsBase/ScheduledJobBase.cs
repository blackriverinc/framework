﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Data;
using System.Data.Objects;
using System.Data.Entity;

using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;

using Blackriverinc.Framework.DataStore;
using Blackriverinc.Framework.DataView;
using Blackriverinc.Framework.EmailClient;
using Blackriverinc.Framework.Utility;


namespace Blackriverinc.Framework.ScheduledJob
	{

   public abstract class ScheduledJobBase : IScheduledJob
		{

		#region "External Resource Providers"

		/// <summary>
		/// Key into _cnxnSettings
		/// </summary>
		protected string _contextKey;

		#endregion

		#region "Common Fields"

		#region "RunDate, RunDateDisplay"
		private Nullable<DateTime> _runDate;
		// Default value is computed as 'Now' on first reference; may be 
		// overrridden by 'RunDate' key/value pair in {JobName} section
		// of application configuration file.
		public DateTime RunDate
			{
			get
				{
				if (!_runDate.HasValue)
					{
					_runDate = _runDate.GetValueOrDefault(DateTime.Now);
               GlobalCache.Get("RunDate", ref _runDate);
					return _runDate.Value;
					}
				else
					return _runDate.Value;
				}
			set
				{
				_runDate = value;
				}
			}

		public string RunDateDisplay
			{
			get
				{
				return string.Format("{0:MM-dd-yyyy} {1}",
											RunDate,
							(RunDate.Hour == 0 && RunDate.Minute == 0)
								? ""
								: string.Format("{0:hh:mm:ss}", RunDate));
				}
			}
		#endregion

		#region JobName
		string _jobName;
		public string JobName
			{
			get
				{
				if (_jobName == null)
               GlobalCache.Get("JobName", ref _jobName);
				return _jobName;
				}
			set
				{
				_jobName = value;
				}
			}
		#endregion

		#region CommandTimeout
		private int? _commandTimeout;
		public int CommandTimeout
			{
			get
				{
				if (!_commandTimeout.HasValue)
					{
					// Default is 30sec
					_commandTimeout = 30;
               GlobalCache.Get("CommandTimeout", ref _commandTimeout);
					return _commandTimeout.Value;
					}
				else
					return _commandTimeout.Value;
				}
         set
			{
            _commandTimeout = value;
				}
			}
      #endregion

      public string DataPath { get; set; }

      public string LogPath { get; set; }

		#region "ResultCount"
		// Thread-safe operations on the number of items processed by ScheduledJob.
		private int _resultCount;
      protected int ResultCount
			{
			get
				{
				int resultCount = -1;
				Interlocked.Exchange(ref resultCount, _resultCount);
				return resultCount;
				}
			}

		protected int SetResultCount(int value)
			{
			int resultCount = Interlocked.Exchange(ref _resultCount, value);
			return resultCount;
			}

		protected int IncrementResultCount()
			{
			return Interlocked.Increment(ref _resultCount);
			}

		protected int IncrementResultCount(int value)
			{
			return Interlocked.Add(ref _resultCount, value);
			}

		#endregion

		#region "DatabaseQueryOnly"
		bool? _databaseQueryOnly = null;
		/// <summary>
		/// If this setting is present and has a value of 'true', can be used by controllers
		/// to suppress database updates for development and testing purposes.
		/// <para>true : controller may suppress database updates for testing</para>
		/// <para>false(default) : normal processing</para>
		/// </summary>
		public bool DatabaseQueryOnly
			{
			get
				{
				if (!_databaseQueryOnly.HasValue)
					{
					bool result = false;
               GlobalCache.Get("DatabaseQueryOnly", ref result);
					_databaseQueryOnly = result;
					}
				return _databaseQueryOnly.Value;
				}
			}
		#endregion

      public string NotificationEmail { get; set; }

      public string EmailServerUri { get; set; }

      public bool DiagnosticsOnly { get; set; }

		#endregion

      FailureNotificationTraceListener _failureNotifier = null;

      protected ScheduledJobBase()
         {
         _failureNotifier = new FailureNotificationTraceListener();

         _failureNotifier.FailureNotified += failureNotified;
         }


      /// <summary>
      /// Handle Filaure Notifications.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="message"></param>
      void failureNotified(object source, string message)
         {
         if (!string.IsNullOrEmpty(NotificationEmail))
			{
         try
            {
               TextTemplate msgBldr = new TextTemplate(GlobalCache.ReadLockData);
               string body = msgBldr.ResolveToString(Framework.ScheduledJob.Resource.FailureNotification);

               SendEmail(JobName + "@horizonpharma.com",
                         NotificationEmail,
                         string.Format("Job '{0}' has recorded a failure.", JobName),
                         body);
				}
            finally 
            {
               GlobalCache.ReleaseData();
            }
			}
         }

		#region Database Transaction : CommitTransaction, RollbackTransaction

		protected bool CommitTransaction(ObjectContext context, IEnumerable transactionSet = null)
			{
			bool result = false;
			try
				{
				if (StopFlagSet)
					throw new StopSignalled();

				if (!DatabaseQueryOnly)
					{
					//-------------------------------------------------------------------
					// Save every change current in the context
					//-------------------------------------------------------------------
					context.SaveChanges();

					//-------------------------------------------------------------------
					// We need to refresh from the DataStore because all of our Entities
					// are referenced with synthetic keys generated by the database.
					//-------------------------------------------------------------------
               if (transactionSet != null)
					   context.Refresh(RefreshMode.StoreWins, transactionSet);
					}

				result = true;
				}
			catch (StopSignalled)
				{
				RollbackTransaction(context);
				throw;
				}
			catch (Exception exp)
				{
				ExceptionReport(exp);

				RollbackTransaction(context);
				}

			return result;
			}

		/// <summary>
		/// Rollback existing added objects
		/// </summary>
		/// <param name="context"></param>
		protected void RollbackTransaction(ObjectContext context)
			{
			Trace.WriteLine(string.Format("=== Aborting Save ==="));
			StringBuilder msg = new StringBuilder();
			msg.AppendLine();

			// Backout Added Objects and Relationships
			var addedObjects = context.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
			foreach (ObjectStateEntry ose in addedObjects)
				if (ose.State != EntityState.Detached)
					{
					msg.AppendFormat("   Entity: '{0}'", ose.Entity);
					Trace.WriteLine(msg.ToString());
					msg.Length = 0;
					ose.Delete();
					}
			Trace.WriteLine(string.Format("=== Aborted Save ==="));
			}

		#endregion

		#region Stream Events

		/// <summary>
		/// Stream is ready to begin processing
		/// </summary>
		public event StreamEventHandler StreamReady;

		/// <summary>
		/// Send notification to all registered subscribers to the 'StreamDone'
		/// event.
		/// </summary>
		protected void OnStreamDone(string subject,
												Stream stream)
			{
			if (StreamReady != null && stream != null)
				{
				Trace.WriteLine(string.Format("Report Dispatched ({0})", subject));
				StreamReady(this, new StreamReadyEventArgs(subject, stream));
				}
			}

		#endregion

		#region JobStatus Events

		/// <summary>
		/// Stream is ready to begin processing
		/// </summary>
		public event JobStateEventHandler JobStateChange;

		/// <summary>
		/// Send notification to all registered subscribers to the 'StreamDone'
		/// event.
		/// </summary>
		protected void OnJobStateChange(IScheduledJob job, JobState jobState, int? status = null) 
			{
			if (JobStateChange != null)
				{
				JobStateChange(this, new JobStateEventArg(jobState, status, DateTime.Now));
				}
			}

		#endregion
 
#if _LATER
		/// <summary>
      /// Extract file meta-data. This implementation uses filename decoration
      /// as the primary 
      /// </summary>
      /// <param name="filePath"></param>
      /// <returns></returns>
      protected IDictionary<string, object> extractMetaData(string filePath)
         {
         IDictionary<string, object> metaData = new Parameters(filePath);
         string[] reportIDParts =  Path.GetFileNameWithoutExtension(filePath).Split(new char[] {'.'});

         // ReportID needs to always be the first "."-delimited 
         // portion of the path specification.
         metaData.Add("ReportId",reportIDParts[0]);

         // Compute the 'format' from the extension, unless we already
         // had a meta-data definition in the path specification.
         if (!metaData.ContainsKey("format"))
            metaData.Add("format", Path.GetExtension(filePath).TrimStart(new char[] {'.'}));

         foreach (var metaItem in metaData)
            Debug.WriteLine("'{0}' = '{1}'", metaItem.Key, metaItem.Value);
         return metaData;
         }
#endif

		#region Notifications : NotifyEmail, NotifyLog
		//
		//	Notifications are standard StreamEvent sinks for ScheduledJobs
		//
#if _LATER
		/// <summary>
		/// Format the output stream as an Email message and send to the
		/// Auditor's e-mail address.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="srea"></param>
		public void NotifyEmail(object sender, StreamDoneEventArgs srea)
			{
			string fromAddr = AppSettings["GeneratorEmailAddress"];
			string toAddr = AppSettings["AuditorEmailAddress"];
         try
            {
            SendEmail(fromAddr, toAddr, srea.Subject, srea.OutputStream);
            }
         catch (Exception exp)
            {
            ExceptionReport(exp, srea.OutputStream);
            }
			}
#endif
		/// <summary>
		/// Copy the Stream to a Log File.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="srea"></param>
		public void NotifyLog(object sender, StreamReadyEventArgs srea)
			{
         try
            {
            WriteStreamToLogFile(srea.Subject, srea.OutputStream);
            }
         catch (Exception exp)
            {
            ExceptionReport(exp, srea.OutputStream);
            }
			}


		protected void SendEmail(string fromAddr,
												  string toAddr,
												  string subject,
                                      string mailBody)
				{
			Trace.WriteLine(string.Format("ScheduledJobsBase::SendEmail (To : '{0}' From: '{1}', Subject : '{2}')", 
													toAddr,
													fromAddr,
													subject));

         if (!string.IsNullOrEmpty(EmailServerUri))

            using (var emailClient = EmailClientFactory.Create(EmailServerUri))
               {
               emailClient.Send(fromAddr,
								 toAddr,
								 subject,
								 mailBody);
               }
#if _TRACE_MAIL
			string outputName = string.Format(@"file://{0}{1}-{2:yyyyMMdd-hhmmss}.html",
											LogPath, JobName, DateTime.Now);
			Stream outputStream = StreamFactory.Create(outputName,
											FileMode.Create, FileAccess.Write);
			stream.Position = 0;
			stream.CopyTo(outputStream);
			outputStream.Close();
#endif

			}

		/// <summary>
		/// Copy the contents of the stream to a log file.
		/// </summary>
		/// <param name="subject">Main part of FileName</param>
		/// <param name="inputStream">Contents of payload.</param>
		protected void WriteStreamToLogFile(string subject, Stream inputStream)
			{
			long position = inputStream.Position;
			try
				{
				string outputName = string.Format("file://{0}{1}-{2:yyyyMMdd-HHmmss}-{3}.html",
												LogPath,
												subject.DelimittedToCamelCase(" ',:"),
												DateTime.Now,
												Identifier.ID);
				Trace.WriteLine(string.Format("ScheduledJobsBase::WriteStreamToLogFile( {0} )", outputName));

				using (Stream outputStream = StreamFactory.Create(outputName,
									FileMode.Create, FileAccess.Write))
					{
					inputStream.Position = 0;
					inputStream.CopyTo(outputStream);
					}
				}
			catch (Exception)
				{
            throw;
				}
			finally
				{
				inputStream.Position = position;
				}
			}

		#endregion

		#region Exception Handling

		public event ExceptionReportedDelegate ExceptionReported;

		int _isExceptionReportedCount = 0;

		/// <summary>
		/// true :	ExceptionReport has been generated and published to
		///			subscribers on 'OnStreamDone'.
		/// </summary>
		public bool IsExceptionReported
			{
			get
				{
				int exceptionReported = 0;
				Interlocked.Exchange(ref exceptionReported, _isExceptionReportedCount);
				return exceptionReported > 0;
				}
			}

      public void ClearExceptionReport()
         {
         Interlocked.Exchange(ref _isExceptionReportedCount, 0);
         }

		public Stream ExceptionReport(Exception exp, Stream stream = null)
			{
         Interlocked.Add(ref _isExceptionReportedCount, 1);
			Trace.WriteLine(exp.ToString());
			stream = ExceptionHandler.ExceptionPageToStream(exp, stream);
			if (!OnExceptionReported(this, exp))
				OnStreamDone(string.Format("Exception-{0}", JobName), stream);
			return stream;
			}

		/// <summary>
		/// Fire ExceptionReported Event if it has not already been fired.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="exp"></param>
		/// <returns>true : Fired; false : not fired.</returns>
		public bool OnExceptionReported(object sender, Exception exp)
			{
			bool fired = false;
			if (!IsExceptionReported
			&& ExceptionReported != null)
				{
				ExceptionReported(sender, exp);
				fired = true;
				}

			return fired;
			}

		#endregion


		/// <summary>
      /// Generate path to the data file.
      /// </summary>
      /// <returns></returns>
      protected string GenerateDataPath(string filePart)
         {
         string dataPath = (DataPath) ?? @"";
         string destinationPath = Path.Combine(dataPath, filePart);

         string baseDir = null;
         if (!Path.IsPathRooted(destinationPath))
            {
            Process process = Process.GetCurrentProcess();
            ProcessModule mainModule = process.MainModule;
            baseDir = System.IO.Path.GetDirectoryName(mainModule.FileName);
            destinationPath = Path.Combine(baseDir, destinationPath);
            }
         baseDir = System.IO.Path.GetDirectoryName(destinationPath);
         if (!Directory.Exists(baseDir))
            Directory.CreateDirectory(baseDir);

         return destinationPath;
         }

      /// <summary>
		/// Render a standard "No Results" report as an HTML document
		/// onto the Stream.
		/// </summary>
		/// <param name="text">Text to appear in Caption area of report.</param>
		/// <param name="stream">Output Stream</param>
		/// <returns></returns>
		protected Stream NoResultsReport(string text, Stream stream = null)
			{
			//-----------------------------------------------------------------
			// Bind the QueryParameters to a View; Render onto a Stream
			//-----------------------------------------------------------------
			HTMLReportDocumentView<HTMLDictionaryTableControl> pageView =
				new HTMLReportDocumentView<HTMLDictionaryTableControl>(stream);

			pageView.Text = text;
			pageView.ReportHeading = string.Format("{0} - Run Date : {1}",
														JobName.CamelCaseToDelimitted(),
														RunDateDisplay);
         try
            {
            stream = pageView.Render(GlobalCache.ReadLockData);
            }
         finally
            {
            GlobalCache.ReleaseData();
            }


			return stream;
			}

		protected Stream NoResultsReport(Stream stream = null)
			{
			return NoResultsReport("Based on these parameters, no tasks are scheduled for processing.",
											stream);
			}

		#region IRunnable Members

		protected int StopFlag;
		protected bool StopFlagSet
			{
			get
				{
				int stopFlag = 0;
				Interlocked.Exchange(ref stopFlag, StopFlag);
				return (stopFlag > 0);
				}
			}

		public abstract void Run();

		/// <summary>
		/// Signal the STOP Event, as to whether anybody is listening,
		/// that is a different issue. 
		/// </summary>
		public void Stop()
			{
			Interlocked.Exchange(ref StopFlag, 1);
			}

		#endregion

		#region Disposable

		~ScheduledJobBase()
			{
			Dispose(false);
			}

		public void Dispose(bool disposing)
			{
			if (disposing)
				{
            _failureNotifier.Dispose();
#if _LATER
				if (_mailClient != null)
					{
					_mailClient.Dispose();
					_mailClient = null;
					}
#endif
				}
			}

		public virtual void Dispose()
			{
         GC.SuppressFinalize(this);
         Dispose(true);
         }

		#endregion


      public object GetJobParameter(string key)
         {
         return GlobalCache.Get(key);
         }

      public void SetJobParameter(string key, object value)
         {
         try
            {
            IKeyedDataStore parameters = GlobalCache.LockData;
            parameters[key] = value;
            }
         finally
            {
            GlobalCache.ReleaseData();
            }
         }

      public bool GetJobParameter(string key, out string value)
         {
         throw new NotImplementedException();
         }

      public bool GetJobParameter(string key, out DateTime value)
         {
         throw new NotImplementedException();
         }

      public bool GetJobParameter(string key, out int value)
         {
         throw new NotImplementedException();
         }
		}
	}
