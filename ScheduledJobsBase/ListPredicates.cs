﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace HRPlus.ScheduledJobs
	{
	public static class ListPredicates
		{
		public static IEnumerable<ItemType> RandomSubset<ItemType>(IEnumerable<ItemType> sourceList,
													 Predicate<ItemType> predicate,
													 int sampleCount)
			{
			List<ItemType> result = new List<ItemType>();

			try
				{
				//------------------------------------------------------------
				// Return a new List from a randomly selected sub-set of
				// an initial List.
				Func<IEnumerable<ItemType>, int, IEnumerable<ItemType>> randomList
					= ((IEnumerable<ItemType> list, int limit) =>
					{
					List<ItemType> randomResults = new List<ItemType>();
						Random rg = new Random(System.DateTime.Now.Millisecond);

						int groupCount = list.Count<ItemType>();

						for (int i = 0; i < groupCount && i < limit; i++)
							{
							int hitIndex = (groupCount > limit)?rg.Next(groupCount):i;
							ItemType cat = list.ElementAt(hitIndex);

							if (!randomResults.Contains(cat))
								randomResults.Add(cat);
							}
						return randomResults;
					});

				//---------------------------------------------------------------
				// Generate a List of Random Items from the Source set.
				//---------------------------------------------------------------
				var sampleList = from X in 
										randomList((from item 
														  in sourceList
													  where predicate(item)
													 select item), sampleCount)
									select X;
				result.AddRange(sampleList);

				}
			catch (Exception exp)
				{
				Trace.WriteLine(exp.ToString());
				throw;
				}

			return result;
			}

		}
	}
