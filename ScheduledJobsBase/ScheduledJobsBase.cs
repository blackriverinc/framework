﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Data;
using System.Data.Objects;
using System.Data.Entity;

using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;

using BlackRiverInc.Framework.Utility;
using BlackRiverInc.Framework.DataStore;
using BlackRiverInc.Framework.DataView;


namespace BlackRiverInc.Framework.ScheduledJobs
	{

	#region StreamEvents

	/// <summary>
	/// StreamEvents are used by ScheduledJobs to signal observers that
	/// a stream of results is ready for action.
	/// </summary>
	public class StreamDoneEventArgs : EventArgs
		{
		string _subject = null;
		public string Subject
			{
			get
				{
				if (_subject != null)
					return _subject;
				return "";
				}
			}
		Stream _outputStream = null;
		public Stream OutputStream
			{
			get
				{
				return _outputStream;
				}
			}

		public StreamDoneEventArgs(string subject, Stream outputStream)
			{
			_subject = subject;
			_outputStream = outputStream;
			}
		}

	public delegate void StreamEventHandler(object sender, StreamDoneEventArgs srea);

	#endregion

	public abstract class ScheduledJobsBase<TConfigSettings> : IScheduledJobs
				where TConfigSettings : IConfigSettings, new()
		{

		#region "External Resource Providers"


		/// <summary>
		/// Key into _cnxnSettings
		/// </summary>
		protected string _contextKey;

		/// <summary>
		/// Application Configuration 
		/// </summary>
		private IConfigSettings _appSettings;
		public IConfigSettings AppSettings
			{
			get
				{
				return (IConfigSettings)_appSettings;
				}
			}

		#endregion

		#region "Common Fields"

		#region "RunDate, RunDateDisplay"
		private Nullable<DateTime> _runDate;
		// Default value is computed as 'Now' on first reference; may be 
		// overrridden by 'RunDate' key/value pair in {JobName} section
		// of application configuration file.
		public DateTime RunDate
			{
			get
				{
				if (!_runDate.HasValue)
					{
					_runDate = _runDate.GetValueOrDefault(DateTime.Now);
					JobParameters.get("RunDate", ref _runDate);
					return _runDate.Value;
					}
				else
					return _runDate.Value;
				}
			set
				{
				_runDate = value;
				}
			}

		public string RunDateDisplay
			{
			get
				{
				return string.Format("{0:MM-dd-yyyy} {1}",
											RunDate,
							(RunDate.Hour == 0 && RunDate.Minute == 0)
								? ""
								: string.Format("{0:hh:mm:ss}", RunDate));
				}
			}
		#endregion

		#region JobName
		string _jobName;
		public string JobName
			{
			get
				{
				if (_jobName == null)
					_jobName = AppSettings["JobName"];
				return _jobName;
				}
			set
				{
				_jobName = value;
				}
			}
		#endregion

		#region CommandTimeout
		private int? _commandTimeout;
		public int CommandTimeout
			{
			get
				{
				if (!_commandTimeout.HasValue)
					{
					// Default is 30sec
					_commandTimeout = 30;
					JobParameters.get("CommandTimeout", ref _commandTimeout);
					return _commandTimeout.Value;
					}
				else
					return _commandTimeout.Value;
				}
			}
		#endregion

		public string LogPath
			{
			get
				{
				return AppSettings["LogPath"];
				}
			}

		#region "JobParameters"
		private Parameters _jobParameters;
		public Parameters JobParameters
			{
			get
				{
				if (_jobParameters == null)
					{
               string sectionName = AppSettings.Section;
					if (JobName != null)
                  AppSettings.Section = JobName;

               _jobParameters = new Parameters(AppSettings);

					// RunDate should always be present
               if (!_jobParameters.ContainsKey("RunDate"))
                  {
                  // We can always override it.
                  _jobParameters["RunDate"] = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                  }
					// Parse CommandLine for overrides
					CommandLineParser.Parse(_jobParameters, Environment.CommandLine, false);

               AppSettings.Section = sectionName;
					}
				return _jobParameters;
				}
			}
		#endregion

		#region "ResultCount"
		// Thread-safe operations on the number of items processed by ScheduledJob.
		private int _resultCount;
		public int ResultCount
			{
			get
				{
				int resultCount = -1;
				Interlocked.Exchange(ref resultCount, _resultCount);
				return resultCount;
				}
			}

		protected int SetResultCount(int value)
			{
			int resultCount = Interlocked.Exchange(ref _resultCount, value);
			return resultCount;
			}

		protected int IncrementResultCount()
			{
			return Interlocked.Increment(ref _resultCount);
			}

		protected int IncrementResultCount(int value)
			{
			return Interlocked.Add(ref _resultCount, value);
			}

		#endregion

		#region "DatabaseQueryOnly"
		bool? _databaseQueryOnly = null;
		/// <summary>
		/// If this setting is present and has a value of 'true', can be used by controllers
		/// to suppress database updates for development and testing purposes.
		/// <para>true : controller may suppress database updates for testing</para>
		/// <para>false(default) : normal processing</para>
		/// </summary>
		public bool DatabaseQueryOnly
			{
			get
				{
				if (!_databaseQueryOnly.HasValue)
					{
					bool result = false;
					AppSettings.get("DatabaseQueryOnly", ref result);
					_databaseQueryOnly = result;
					}
				return _databaseQueryOnly.Value;
				}
			}
		#endregion

		#endregion

		private void _initialize(IConfigSettings appSettings)
			{
         try
            {
            StringBuilder sb = new StringBuilder();

            //----------------------------------------------------------------
            // Initialize External Resource Providers
            //----------------------------------------------------------------
            _appSettings = appSettings;
#if _OBS
			//----------------------------------------------------------------
			// Parse CommandLine for overrides
			//----------------------------------------------------------------
			Parameters parameters = new Parameters(_appSettings);
			CommandLineParser.Parse(parameters, Environment.CommandLine, true);
			foreach (var item in parameters)
				{
				_appSettings[item.Key] = item.Value.ToString();
				}
#endif

            //----------------------------------------------------------------
            // Dump the query parameters to the trace log.
            //----------------------------------------------------------------
            sb.Length = 0;
            sb.AppendLine(String.Format("<{0}>", JobName));
            foreach (KeyValuePair<string, object> item in JobParameters)
               {
               XmlSerializeHelper.SerializeToString(ref sb, item);
               }
            sb.AppendLine(String.Format("</{0}>", JobName));
            Trace.WriteLine(sb.ToString());
            sb.Length = 0;
            }
         catch (Exception exp)
            {
            // Caught exception during initialization
            Stream stream = ExceptionReport(exp);
            NotifyLog(this, new StreamDoneEventArgs("Exception-" + JobName, stream));
            }
			}

		protected ScheduledJobsBase()
			{
			_initialize(new TConfigSettings());
			}

      protected ScheduledJobsBase(string configFileName)
         {
         IConfigSettings iconfig = ConfigurationAdapter.LoadConfiguration(configFileName)
            as IConfigSettings;
         _initialize(iconfig);
         }

		#region Database Transaction : CommitTransaction, RollbackTransaction

		protected bool CommitTransaction(ObjectContext context, IEnumerable transactionSet = null)
			{
			bool result = false;
			try
				{
				if (StopFlagSet)
					throw new StopSignalled();

				if (!DatabaseQueryOnly)
					{
					//-------------------------------------------------------------------
					// Save every change current in the context
					//-------------------------------------------------------------------
               context.SaveChanges();

					//-------------------------------------------------------------------
					// We need to refresh from the DataStore because all of our Entities
					// are referenced with synthetic keys generated by the database.
					//-------------------------------------------------------------------
               if (transactionSet != null)
					   context.Refresh(RefreshMode.StoreWins, transactionSet);
					}

				result = true;
				}
			catch (StopSignalled)
				{
				RollbackTransaction(context);
				throw;
				}
			catch (Exception exp)
				{
				ExceptionReport(exp);

				RollbackTransaction(context);
				}

			return result;
			}

		/// <summary>
		/// Rollback existing added objects
		/// </summary>
		/// <param name="context"></param>
		protected void RollbackTransaction(ObjectContext context)
			{
			Trace.WriteLine(string.Format("=== Aborting Save ==="));
			StringBuilder msg = new StringBuilder();
			msg.AppendLine();

			// Backout Added Objects and Relationships
			var addedObjects = context.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
			foreach (ObjectStateEntry ose in addedObjects)
				if (ose.State != EntityState.Detached)
					{
					msg.AppendFormat("   Entity: '{0}'", ose.Entity);
					Trace.WriteLine(msg.ToString());
					msg.Length = 0;
					ose.Delete();
					}
			Trace.WriteLine(string.Format("=== Aborted Save ==="));
			}

		#endregion

		#region Stream Events

		/// <summary>
		/// Stream is ready to begin processing
		/// </summary>
		public event StreamEventHandler StreamDone;

		/// <summary>
		/// Send notification to all registered subscribers to the 'StreamDone'
		/// event.
		/// </summary>
		protected void OnStreamDone(string subject,
												Stream stream)
			{
			if (StreamDone != null && stream != null)
				{
				Trace.WriteLine(string.Format("Report Dispatched ({0})", subject));
				StreamDone(this, new StreamDoneEventArgs(subject, stream));
				}
			}

		#endregion

      /// <summary>
      /// Extract file meta-data. This implementation uses filename decoration
      /// as the primary 
      /// </summary>
      /// <param name="filePath"></param>
      /// <returns></returns>
      protected  Parameters extractMetaData(string filePath)
         {
         Parameters metaData = new Parameters(filePath);
         string[] reportIDParts =  Path.GetFileNameWithoutExtension(filePath).Split(new char[] {'.'});

         // ReportID needs to always be the first "."-delimited 
         // portion of the path specification.
         metaData.Add("ReportId",reportIDParts[0]);

         // Compute the 'format' from the extension, unless we already
         // had a meta-data definition in the path specification.
         if (!metaData.ContainsKey("format"))
            metaData.Add("format", Path.GetExtension(filePath).TrimStart(new char[] {'.'}));

         foreach (var metaItem in metaData)
            Debug.WriteLine("'{0}' = '{1}'", metaItem.Key, metaItem.Value);
         return metaData;
         }

		#region Notifications : NotifyEmail, NotifyLog
		//
		//	Notifications are standard StreamEvent sinks for ScheduledJobs
		//
#if _LATER
		/// <summary>
		/// Format the output stream as an Email message and send to the
		/// Auditor's e-mail address.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="srea"></param>
		public void NotifyEmail(object sender, StreamDoneEventArgs srea)
			{
			string fromAddr = AppSettings["GeneratorEmailAddress"];
			string toAddr = AppSettings["AuditorEmailAddress"];
         try
            {
            SendEmail(fromAddr, toAddr, srea.Subject, srea.OutputStream);
            }
         catch (Exception exp)
            {
            ExceptionReport(exp, srea.OutputStream);
            }
			}
#endif
		/// <summary>
		/// Copy the Stream to a Log File.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="srea"></param>
		public void NotifyLog(object sender, StreamDoneEventArgs srea)
			{
         try
            {
            WriteStreamToLogFile(srea.Subject, srea.OutputStream);
            }
         catch (Exception exp)
            {
            ExceptionReport(exp, srea.OutputStream);
            }
			}
#if _LATER
		protected void SendEmail(string fromAddr,
												  string toAddr,
												  string subject,
												  Stream stream)
			{
			string routeEmailsTo = AppSettings["RouteEmailsTo"];
			if (routeEmailsTo != null)
				{
				subject = string.Format("*Testing* - {0}. Original 'To' : {1}",
							subject,
							toAddr);
				toAddr = routeEmailsTo;
				}
			//-----------------------------------------------------------------
			// Email Stream as an EmailMessage to the Auditor
			//-----------------------------------------------------------------
			string mailBody = null;
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] buffer = new byte[stream.Length];
			stream.Position = 0;
			stream.Read(buffer, 0, (int)stream.Length);
			mailBody = encoding.GetString(buffer).Trim(new char[] { ' ', '\0' });

			Trace.WriteLine(string.Format("ScheduledJobsBase::SendEmail (To : '{0}' From: '{1}', Subject : '{2}')", 
													toAddr,
													fromAddr,
													subject));

			MailClient.Send(fromAddr,
								 toAddr,
								 subject,
								 mailBody);
#if _TRACE_MAIL
			string outputName = string.Format(@"file://{0}{1}-{2:yyyyMMdd-hhmmss}.html",
											LogPath, JobName, DateTime.Now);
			Stream outputStream = StreamFactory.Create(outputName,
											FileMode.Create, FileAccess.Write);
			stream.Position = 0;
			stream.CopyTo(outputStream);
			outputStream.Close();
#endif
			}
#endif
		/// <summary>
		/// Copy the contents of the stream to a log file.
		/// </summary>
		/// <param name="subject">Main part of FileName</param>
		/// <param name="inputStream">Contents of payload.</param>
		protected void WriteStreamToLogFile(string subject, Stream inputStream)
			{
			long position = inputStream.Position;
			try
				{
				string outputName = string.Format("file://{0}{1}-{2:yyyyMMdd-HHmmss}-{3}.html",
												LogPath,
												subject.DelimittedToCamelCase(" ',:"),
												DateTime.Now,
												Identifier.ID);
				Trace.WriteLine(string.Format("ScheduledJobsBase::WriteStreamToLogFile( {0} )", outputName));

				using (Stream outputStream = StreamFactory.Create(outputName,
									FileMode.Create, FileAccess.Write))
					{
					inputStream.Position = 0;
					inputStream.CopyTo(outputStream);
					}
				}
			catch (Exception)
				{
            throw;
				}
			finally
				{
				inputStream.Position = position;
				}
			}

		#endregion

		#region Exception Handling

		public event ExceptionReportedDelegate ExceptionReported;

		int _isExceptionReportedCount = 0;

		/// <summary>
		/// true :	ExceptionReport has been generated and published to
		///			subscribers on 'OnStreamDone'.
		/// </summary>
		public bool IsExceptionReported
			{
			get
				{
				int exceptionReported = 0;
				Interlocked.Exchange(ref exceptionReported, _isExceptionReportedCount);
				return exceptionReported > 0;
				}
			}

      public void ClearExceptionReport()
         {
         Interlocked.Exchange(ref _isExceptionReportedCount, 0);
         }

		public Stream ExceptionReport(Exception exp, Stream stream = null)
			{
         Interlocked.Add(ref _isExceptionReportedCount, 1);
			Trace.WriteLine(exp.ToString());
			stream = ExceptionHandler.ExceptionPageToStream(exp, stream);
			if (!OnExceptionReported(this, exp))
				OnStreamDone(string.Format("Exception-{0}", JobName), stream);
			return stream;
			}

		/// <summary>
		/// Fire ExceptionReported Event if it has not already been fired.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="exp"></param>
		/// <returns>true : Fired; false : not fired.</returns>
		public bool OnExceptionReported(object sender, Exception exp)
			{
			bool fired = false;
			if (!IsExceptionReported
			&& ExceptionReported != null)
				{
				ExceptionReported(sender, exp);
				fired = true;
				}

			return fired;
			}

		#endregion


		/// <summary>
		/// Render a standard "No Results" report as an HTML document
		/// onto the Stream.
		/// </summary>
		/// <param name="text">Text to appear in Caption area of report.</param>
		/// <param name="stream">Output Stream</param>
		/// <returns></returns>
		protected Stream NoResultsReport(string text, Stream stream = null)
			{
			//-----------------------------------------------------------------
			// Bind the QueryParameters to a View; Render onto a Stream
			//-----------------------------------------------------------------
			HTMLReportDocumentView<HTMLDictionaryTableControl> pageView =
				new HTMLReportDocumentView<HTMLDictionaryTableControl>(stream);

			pageView.Text = text;
			pageView.ReportHeading = string.Format("{0} - Run Date : {1}",
														JobName.CamelCaseToDelimitted(),
														RunDateDisplay);
			stream = pageView.Render(JobParameters);
			return stream;
			}

		protected Stream NoResultsReport(Stream stream = null)
			{
			return NoResultsReport("Based on these parameters, no tasks are scheduled for processing.",
											stream);
			}

		#region IRunnable Members

		protected int StopFlag;
		protected bool StopFlagSet
			{
			get
				{
				int stopFlag = 0;
				Interlocked.Exchange(ref stopFlag, StopFlag);
				return (stopFlag > 0);
				}
			}

		public abstract void Run();

		/// <summary>
		/// Signal the STOP Event, as to whether anybody is listening,
		/// that is a different issue. 
		/// </summary>
		public void Stop()
			{
			Interlocked.Exchange(ref StopFlag, 1);
			}

		#endregion

		#region Disposable

		~ScheduledJobsBase()
			{
			Dispose(false);
			}

		public void Dispose(bool disposing)
			{
			if (disposing)
				{
#if _LATER
				if (_mailClient != null)
					{
					_mailClient.Dispose();
					_mailClient = null;
					}
#endif
				}
			}

		public virtual void Dispose()
			{
         GC.SuppressFinalize(this);
         Dispose(true);
         }

		#endregion
		}
	}
