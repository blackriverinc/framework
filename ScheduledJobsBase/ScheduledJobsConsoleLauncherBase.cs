﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using BlackRiverInc.Framework.Utility;
using BlackRiverInc.Framework.DataStore;

namespace BlackRiverInc.Framework.ScheduledJobs
	{
	public abstract class ScheduledJobsConsoleLauncherBase<ScheduledJobType, ConfigSettingsType>
		where ScheduledJobType : ScheduledJobsBase<ConfigSettingsType>, new()
		where ConfigSettingsType : IConfigSettings, new()
		{
      static int? _exitCode = null;
      static public int ExitCode
         {
         get
            {
            return (_exitCode.HasValue) ? _exitCode.Value : 1;
            }
         }
		protected static void LaunchJob()
			{
			IConfigSettings appSettings = new ConfigSettingsType();
         //----------------------------------------------------------------
         // Parse CommandLine for overrides
         //----------------------------------------------------------------
         Parameters parameters = new Parameters(appSettings);
         CommandLineParser.Parse(parameters, Environment.CommandLine, true);
         foreach (var item in parameters)
            {
            appSettings[item.Key] = item.Value.ToString();
            }

       
			AppTraceListener tracer = new AppTraceListener(Path.Combine((appSettings["LogPath"] != null)?appSettings["LogPath"]:"",
																							appSettings["JobName"]));
         Trace.Listeners.Add(new ConsoleTraceListener());

			StringBuilder sb = new StringBuilder();

			try
				{
				//-----------------------------------------------------------------
				// Attach new tracer to the System.Diagnostics.Trace. After this
				// point, all Write/WriteLine commands to Trace and Debug.
				//-----------------------------------------------------------------
				tracer.DisplayTaskID = true;
				
				//-----------------------------------------------------------------
				// Create a Controller and Run it.
				//-----------------------------------------------------------------
				using (ScheduledJobType controller
					= new ScheduledJobType())
					{
               // Construction/Initialization tossed an exception...bye! bye!
               if (controller.IsExceptionReported)
                  return;

					//----------------------------------------------------------------
					// Setup Standard Callbacks to issue an Email and write a log file
					//	with any Reports issued by the Controller.
					//----------------------------------------------------------------
					//controller.StreamDone += controller.NotifyEmail;
					controller.StreamDone += controller.NotifyLog;

					//----------------------------------------------------------------
					// Parse CommandLine for overrides to original AppSettings
					//----------------------------------------------------------------
					appSettings = controller.AppSettings;
               appSettings["RunDate"] = controller.RunDateDisplay;

					//----------------------------------------------------------------
					// Dump the appSettings to the trace log.
					//----------------------------------------------------------------
					sb.Length = 0;
					sb.AppendLine(String.Format("<{0}>", appSettings.Section));
					foreach (KeyValuePair<string, object> item in appSettings)
						XmlSerializeHelper.SerializeToString(ref sb, item);
					sb.AppendLine(String.Format("</{0}>", appSettings.Section));
					Trace.WriteLine(sb.ToString());
					sb.Length = 0;

               // Break into the debugger
               bool debug = false;
               appSettings.get("Debug", ref debug);
               if (debug)
                  Debugger.Break();

					//----------------------------------------------------------------
					// Hook Ctrl-C to signal Controller to Stop running.
					//----------------------------------------------------------------
					Console.CancelKeyPress +=
						((sender, ea) =>
							{
								Trace.WriteLine("Operator aborted.");

								// Set the stop flag
								((IRunnable)controller).Stop();

								// Let application wind-down
								ea.Cancel = true;
                        _exitCode = 1;
							});

					Console.WriteLine("Press Ctrl-C to stop...");

					//----------------------------------------------------------------
					//	Synchronous "Go!"
					//----------------------------------------------------------------
               
					controller.Run();

               if (!_exitCode.HasValue)
                  _exitCode = (controller.IsExceptionReported)?-1:0;
					}
				}
			catch (Exception exp)
				{            
				Trace.WriteLine("*** Caught Exception at 'Run' ***");
				Trace.WriteLine(exp.ToString());
            _exitCode = -1;
				}
			finally
				{
				tracer.Dispose();
				}
			}
		}
	}

